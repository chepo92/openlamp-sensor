include <arduino.scad>
include  <params.scad>;

//Arduino boards
//You can create a boxed out version of a variety of boards by calling the arduino() module
//The default board for all functions is the Uno

dueDimensions = boardDimensions( DUE );
unoDimensions = boardDimensions( UNO );

//Board mockups


//translate( [0, 0, 8] )	arduino(MEGA);

caseLid();
module caseLid() {
translate([0, 0, 1.5 + 8 + 36 ]) {
    difference () {
        enclosureLid(MEGA, offset = 20);
        
        //cylinders substraction
        xMov = 10 ;   // x displacement cylinder
        yMov = 40 ;
        translate([xMov, 0, -2]) {cube ([dimX,dimY,10]);}
        translate([xMov, yMov, -2]) {cube ([dimX,dimY,10]);}     
        
        //Conectors substraction
        xMovLed = 40 + xMov;
        yMovLed = yMov + 10 ; 
        translate([xMovLed, 0, -2]) {cube([10,8,10]);}          
        translate([xMovLed, yMovLed, -2]) {cube([10,8,10]);}             
        
        xMovTh = -20 + xMov; 
        yMovTh =  yMov;     
        translate([xMovTh, 0, -2]) {cube([10,8,10]);} 
        translate([xMovTh, yMovTh, -2]) {cube([10,8,10]);}         
        
        Yoffset = 20 ; 
        translate([xMovTh, Yoffset, -2]) {cube([10,8,10]);} 
        translate([xMovTh, yMovTh + Yoffset, -2]) {cube([10,8,10]);}  
       
        //LCD conectors substraction       
        translate([0, 90, -2]) {cube([8,8,10]);}
       
        
    }
}
}