include <params.scad>;

module pcb(largo=false) {
    if (largo==false) {  
        cube([largoPcb,anchoPcb,altoPcb]);        
    }
    else {
        delta=20;
        translate ([0,0,-delta])cube([largoPcb,anchoPcb,altoPcb+delta]);      
    }    
    //conectores();


}
module pins(){
    cube([pinX,pinY,pinZ]);
}

module conectores(){
    translate ([0,anchoPcb/2-pinY/2,,-pinZ*0.7]) pins();
    translate ([largoPcb-pinX,anchoPcb/2-pinY/2,-pinZ*0.7]) pins();
}

module sensor(largoCuad=false, largoCil=false) {
   if (largoCuad==false && largoCil==false) {
    //Sensor real
       difference(){
        cube([ladoSensor,ladoSensor,altoSensor]);  
        translate ([ladoSensor/2,ladoSensor/2,altoSensor-altoMuesca]) cylinder (h=altoMuesca,d= Dmuesca); 
    }
        //exagerado
   }else if (largoCuad==true) {
        difference(){
            extra=3;
            cube([ladoSensor,ladoSensor,extra]);  
            translate ([ladoSensor/2,ladoSensor/2,extra-altoMuesca]) cylinder (h=altoMuesca,d= Dmuesca); 
        }
        //cildrindro sensor real
   }else if (largoCil==true) {
        //difference(){
                cube([ladoSensor,ladoSensor,altoSensor]);  
        translate ([ladoSensor/2,ladoSensor/2,altoSensor-altoMuesca]) cylinder (h=30,d= Dmuesca); 
        //}
   }
}

module fd() {
pcb();
translate ([largoPcb/2-ladoSensor/2,anchoPcb/2-ladoSensor/2,altoPcb]) sensor();
}

module fd_largo() {
    pcb(true);
    translate ([largoPcb/2-ladoSensor/2,anchoPcb/2-ladoSensor/2,altoPcb])  sensor();
}

module fd_largo2() {
    translate ([-largoPcb/2,-anchoPcb/2,-altoPcb]) {
    pcb(true);
    translate ([largoPcb/2-ladoSensor/2,anchoPcb/2-ladoSensor/2,altoPcb]) sensor(true,false);
    }
}

fd_largo2();