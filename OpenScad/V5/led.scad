//Params
include  <params.scad>;

module ledCir() {
union(){
    translate ([0,0,altoLED]) cylinder (h=altoMuesca,d= diametroMuescaLED); 
    cylinder (h=altoLED,d= diametroLED); 
    translate ([0,0,0]) sphere(d=diametroLED);
}
}

module ledSqr() {

cylinder (h=altoSqr,d= diametroLED); 
    

}

module ledHighPower() {

  translate ([0,0,-ledHighPowerHeight-5])cylinder (h=ledHighPowerHeight+5,d= ledHighPowerDiameter); 
  translate ([0,0,0])cube([3,8,2],center=true);    
  translate ([ledHighPowerDiameter/2,0,(-ledHighPowerHeight-3)/2])cube([8,3,ledHighPowerHeight+5],center=true);
  translate ([-(ledHighPowerDiameter/2),0,(-ledHighPowerHeight-3)/2])cube([8,3,ledHighPowerHeight+5],center=true);        

}
ledHighPower();
//ledSqr();