use <tubo.scad>;
use <led.scad>;
use <canales.scad>;
use <fotoDiodo.scad>;
include  <params.scad>;
use <1.Box.scad>;
use <2.Holder.scad>; 
use <3.Isolator.scad>;
use <4.Lids.scad>;
use <5.Case.scad>;
use <6.CaseLid.scad>;


//1
translate ([18,0,100])soporteSacado();
//2 
translate ([0,0,120])cilindroInt();

//3
translate ([0,0,120])halves();

//4 
translate ([-10,-10,150]) tapaSuperior();
translate ([30,-6,90])  tapaLateral (); 

//5 
case();

//6 
translate ([0,0,20])caseLid();