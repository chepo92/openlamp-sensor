use <led.scad>;
include <params.scad>;

module tapaSuperior () { 
   difference(){
       delta= (diametroTapaExterior - diametroExteriorCilindroExterior-tolerancia);
       translate ([0,0,0]) cube([diametroTapaExterior,diametroTapaExterior,altoTapa]);
       translate ([delta/2,delta/4,0]) cube([diametroExteriorCilindroExterior+tolerancia,diametroExteriorCilindroExterior+tolerancia,altoTapaInterior]);
       

            
   }
   
}

translate ([0,50,0])tapaSuperior();



module tapaLateral () { 
   rotate ([90,0,90])  difference(){
            translate ([0,0,0]) cube([largoPcb-5,largoPcb,5]);
   }
}

tapaLateral (); 