use <tubo.scad>;
use <led.scad>;
use <canales.scad>;
use <fotoDiodo.scad>;
include  <params.scad>;
use <1.Box.scad>;
use <2.Holder.scad>; 
use <3.Isolator.scad>;
use <4.Lids.scad>;
use <5.Case.scad>;
use <6.CaseLid.scad>;


//1
translate ([18,0,0])soporteSacado();

//2 
translate ([0,0,20])cilindroInt();

//3
translate ([0,0,20])halves();

//4 
translate ([-10,0,50]) tapaSuperior();
translate ([30,0,-10])  tapaLateral (); 

//5 
case();

//6 
caseLid();