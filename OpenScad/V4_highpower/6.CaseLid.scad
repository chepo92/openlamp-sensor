include <arduino.scad>
include  <params.scad>;

//Arduino boards
//You can create a boxed out version of a variety of boards by calling the arduino() module
//The default board for all functions is the Uno

dueDimensions = boardDimensions( DUE );
unoDimensions = boardDimensions( UNO );

//Board mockups


//translate( [0, 0, 8] )	arduino(MEGA);


module caseLid() {
translate([0, 0, 1.5 + 8 + 36 ]) {
    difference () {
        enclosureLid(MEGA);
        
        //cylinders substraction
        xMov = 15 ;   // x displacement cylinder
        translate([xMov, 30+6, -2]) {cylinder (h=23,d= diametroExteriorCilindroExterior+tolerancia*2);}
        translate([xMov, 64, -2]) {cylinder (h=23,d= diametroExteriorCilindroExterior+tolerancia*2);}
        
        //sd conectors substraction
        yMov = 92;     // y displacement 
        translate([40, yMov, -2]) {cube([8,6,10]);}
        translate([1, yMov, -2]) {cube([4,6,10]);}
        
        //LED conectors substraction
        yMovLed = 58 ; 
        translate([30, yMovLed, -2]) {cube([20,8,10]);}        
        //LCD conectors substraction
       
        translate([5, 80, -2]) {cube([6,6,10]);}
        translate([30-8, 10+6, -2]) {cube([6,6,10]);}
        
    }
}
}