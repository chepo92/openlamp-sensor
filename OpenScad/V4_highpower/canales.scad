include  <params.scad>;

module canalesInternos(){
    //restar canal luz     
    translate ([0,0,diametroCanalLuz/2])rotate([0,-90 , 0])cylinder (h=20,d= diametroCanalLuz);
    translate ([0,0,diametroCanalLuz/2])rotate([-90,0 , 0])cylinder (h=20,d= diametroCanalLuz);
    translate ([0,0,diametroCanalLuz/2])rotate([180,0 , 0])cylinder (h=20,d= diametroCanalLuz);
    //translate ([0,0,diametroCanalLuz/2])rotate([90,0 , 0])cylinder (h=20,d= diametroCanalLuz);
    translate ([0,0,diametroCanalLuz/2])rotate([0,90 , 0])cylinder (h=20,d= diametroCanalLuz);
    //restar termistor    
    
}

module canalTermistor(){
    translate ([0,0,0])rotate([90,0 , 0])cylinder (h=20,d= diametroTermistor);
    }

module canalesLED(){ 
    translate ([0,0,0])rotate([0,-90 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([-90,0 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([180,0 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([0,90 , 0])cylinder (h=20,d= diametroLED);

}

module canalSensor(){ 
    translate ([0,dimY/2,0]) { cube([ladoSensor,dimY,,ladoSensor],true);}
    translate ([0,0,0])rotate([0,-90 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([-90,0 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([180,0 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([0,90 , 0])cylinder (h=20,d= diametroLED);

}
canalSensor();
canalTermistor();
canalesInternos();

canalesLED();