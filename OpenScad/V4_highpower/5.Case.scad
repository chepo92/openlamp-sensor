include <arduino.scad>
include  <params.scad>;

//Arduino boards
//You can create a boxed out version of a variety of boards by calling the arduino() module
//The default board for all functions is the Uno

module case () {
    
translate([0, 0, 0]) {
	enclosure(MEGA,heightExtension= 25);
}

}
/*
translate([0, 0, 1.5 + 8 + 36 ]) {
    difference () {
        enclosureLid(MEGA);
        xMov = 16 ;
        yMov = 92;
        translate([xMov, 30, -2]) {cylinder (h=23,d= dExt+tolerancia);}
        translate([xMov, 64, -2]) {cylinder (h=23,d= dExt+tolerancia);}
        translate([10, yMov, -2]) {cube([8,6,10]);}
        translate([50, yMov, -2]) {cube([4,6,10]);}
    }
}
*/