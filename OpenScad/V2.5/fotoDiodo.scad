//Params
lado=5;
alto=2;

Dmuesca=2;
altoMuesca=0.1;

largoPcb=32;
anchoPcb=25;
altoPcb=1.5;

$fn=100; 

pinX=4;
pinY=11;
pinZ=12;

module pcb(largo=false) {
    if (largo==false) {
  
        cube([largoPcb,anchoPcb,altoPcb]);
        
    }
    else {
        delta=4;
        translate ([0,0,-delta])cube([largoPcb,anchoPcb,altoPcb+delta]);
      
    }
    
    conectores();


}
module pins(){
    cube([pinX,pinY,pinZ]);
    }

module conectores(){
    translate ([0,anchoPcb/2-pinY/2,,-pinZ*0.7]) pins();
    translate ([largoPcb-pinX,anchoPcb/2-pinY/2,-pinZ*0.7]) pins();
}

module sensor(largoCuad=false, largoCil=false) {
   if (largoCuad==false && largoCil==false) {
    difference(){
    cube([lado,lado,alto]);  
        translate ([lado/2,lado/2,alto-altoMuesca]) cylinder (h=altoMuesca,d= Dmuesca); 
    }   
   }else if (largoCuad==true) {
        difference(){
         cube([lado,lado,alto*5]);  
        translate ([lado/2,lado/2,5*alto-altoMuesca]) cylinder (h=altoMuesca,d= Dmuesca); 
        }
   }else if (largoCil==true) {
        //difference(){
                cube([lado,lado,alto]);  
        translate ([lado/2,lado/2,alto-altoMuesca]) cylinder (h=30,d= Dmuesca); 
        //}
   }
}

module fd() {
pcb();
translate ([largoPcb/2-lado/2,anchoPcb/2-lado/2,altoPcb]) sensor();
}

module fd_largo() {
    pcb(true);
    translate ([largoPcb/2-lado/2,anchoPcb/2-lado/2,altoPcb])  sensor();
}

module fd_largo2() {
    pcb(true);
    translate ([largoPcb/2-lado/2,anchoPcb/2-lado/2,altoPcb]) sensor(false,true);
}

fd_largo2();