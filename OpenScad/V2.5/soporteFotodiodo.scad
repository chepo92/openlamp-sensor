

use <fotoDiodo.scad>;
use <prism.scad>;
// millimeters
dimX=34;
dimY=27;
dimZ=8;

ancho=15;
largo=10;
alto=30;

muescaX=5;
muescaY=1;
muescaZ=1;

module soporte(){
 
    
  difference(){
     //cubo stock       
     translate ([-1,0,-1])rotate([acos(0),0 , 0])cube([dimX,dimY,dimZ]);
     //restar el pcb fotodiodo con los componentes (fd_largo) 
     translate ([0,-2,0])rotate([acos(0),0 , 0]) fd_largo();
     //restar encaje con el holder 
     translate ([dimX/2-ancho/2-1,-15,-1])cube([ancho,largo,alto]);
 }
 //muescas para afirmar el fd  
 translate ([muescaX/2+dimX/2-1,0,0])rotate([0,0,180])cube([muescaX,muescaY,muescaZ]);
  translate ([muescaX/2+dimX/2-1,-1,dimY-3])rotate([90,0,180])cube([muescaX,muescaY,muescaZ]);
}

soporte();
