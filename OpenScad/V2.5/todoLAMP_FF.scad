use <tubo.scad>;
use <led.scad>;
use <fotoDiodo.scad>;
use <soporteFotodiodo.scad>;
use <holderV2b.scad>;
use <threads.scad>;
// millimeters



translate ([8,0,0]) holderv2b();
translate ([0,30,0]) { 
    rotate([acos(0),0 , 0]) fd();
    translate ([0,0,0]) soporte();
}
tube();