use <tubo.scad>;
use <led.scad>;
use <threads.scad>;
use <fotoDiodo.scad>;
use <prism.scad>;
include  <params.scad>;

module cilindroInt (show=false) {
    //
    //translate ([dimX/2-1,-dimZ-5,1])
    z=21;
    
  difference(){
     //Cilindro stock
      metric_thread (diameter=9, pitch=1, length=19, square=true, thread_size=1, groove=true, rectangle=1);
     //metric_thread (diameter=9, pitch=2, length=19);
     //cylinder (h=19,d= 9);
     //restar canal luz
     cylinder (h=10,d= 3);
     //restar termistor
     translate ([0,0,6])rotate([90,0 , 0])cylinder (h=10,d= 2.5);
     // restar tubo
     scale([1,1,1])translate ([0,0,z]) tube(false);
  }
  if (show)  scale([1.15,1.15,1.15])translate ([0,0,z]) tube(false);  //show tube

}


translate ([0,0,0])cilindroInt(false);

