use <tubo.scad>;
use <led.scad>;

//Params
xDim=15;
yDim=20;
zDim=24; //28 

altoIndentacion=2; 
espesor=2;
altoTapa=5;
altoCuerpo= zDim-altoTapa ; 

diamTermistor=2;
altoTermistor=2; 


$fn=100;

overlapIndent =0.5;
toleranciaEncaje= 0.2;
altoEncaje=altoIndentacion*4;
explotar=0;

module infBox()
{
      
            difference(){
                difference(){
                    translate ([0,0,0])cube ([xDim,yDim,altoCuerpo]);
                    translate ([espesor,espesor,espesor])cube ([xDim-2*espesor,yDim-2*espesor,zDim-altoTapa]);
                    //scale([1.15,1.15,1.15])tube(cutout=false);    
                }  
          translate ([xDim/2,yDim/2,0])cylinder (h=altoTermistor,d=diamTermistor ); 
            //translate ([0,0,0])rotate([acos(0),0 , 0]) cylinder (h=11,d= 5*sqrt(2)); 
            }
            
            
            
        
}

module tapa()
{
   translate ([0,0,zDim+explotar*2]) difference(){
        translate ([0,0,0])cube ([xDim,yDim,altoTapa]);
        translate ([espesor,espesor,0])cube ([xDim-2*espesor,yDim-2*espesor,altoTapa-espesor]);
        //scale([1.15,1.15,1.15])tube(cutout=false);
        
        
    }  
}


module indentacion()
{
   translate ([-espesor-toleranciaEncaje,-espesor-toleranciaEncaje,altoCuerpo+explotar]) union (){
        
    difference(){
        
    cube ([(xDim+espesor*2+toleranciaEncaje*2),(yDim+espesor*2+toleranciaEncaje*2),altoEncaje]);
        
    translate ([espesor,espesor,0])cube ([xDim+toleranciaEncaje*2,yDim+toleranciaEncaje*2,altoIndentacion*5.1]);
    }
    
    difference(){
        translate ([espesor-overlapIndent,espesor-overlapIndent,altoEncaje/3])cube ([xDim+overlapIndent*2,yDim+2*overlapIndent,altoIndentacion]);

        translate ([espesor+ xDim/2,espesor+yDim/2,altoEncaje/2]) rotate ([0,0,acos(0)]) tube(cutout=false);
        
    }
    
}
 
}

 
  
module body(){
difference () {
union    () {
         infBox();
    translate ([xDim/2,yDim,altoCuerpo/3])cube ([6,2,8],center=true);   
}
translate ([xDim/2,5.7,altoCuerpo/3]) rotate([acos(0),0 , 0]) led();   
translate ([xDim/2,yDim,altoCuerpo/3])cube ([5,10,7],center=true);
}
}




 //translate ([xDim/2,yDim,altoCuerpo/3])cube ([5,2.1,7],center=true);
tapa();
indentacion();
body();
//translate ([xDim/2,yDim/2,altoCuerpo]) rotate ([0,0,acos(0)]) tube(cutout=false);