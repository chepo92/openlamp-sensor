use <tubo.scad>;
use <led.scad>;
use <threads.scad>;
use <fotoDiodo.scad>;
use <prism.scad>;
include  <params.scad>;


module soporte(){
  translate ([1-dimX/2,1-dimY/2,0])
  rotate([-90,0 , 0])
  difference(){
      union(){
            //cubo stock       
            translate ([-1,0,-1])rotate([acos(0),0 , 0])cube([dimX,dimY,dimZ]);
            //muescas para afirmar el fd   
   
     }
     //restar el pcb fotodiodo con los componentes (fd_largo) 
     translate ([0,-2,0])rotate([acos(0),0 , 0]) fd_largo2();

 }
     translate ([1-dimX/2,1-dimY/2,0])
  rotate([-90,0 , 0])          translate ([muescaX/2+dimX/2-1,0,0])rotate([0,0,180])cube([muescaX,muescaY,muescaZ]);
       translate ([1-dimX/2,1-dimY/2,0])
  rotate([-90,0 , 0])       translate ([muescaX/2+dimX/2-1,-1,dimY-3])rotate([90,0,180])cube([muescaX,muescaY,muescaZ]);
}


module soporteSacado(){
    difference(){
        soporte();
             //restar el cilindro grande
        translate ([0,0,5])cylinder (h=5,d= dExt+tolerancia);
    }
    
}

soporteSacado();
