use <tubo.scad>;
use <led.scad>;
use <threads.scad>;
use <fotoDiodo.scad>;
use <prism.scad>;
include  <params.scad>;

module cilindroExt (show=false) {
    //
    //translate ([dimX/2-1,-dimZ-5,1])
  z=23;
    
  difference(){
     //Cilindro stock
     cylinder (h=23,d= dExt-tolerancia);
     //restar interno
     cylinder (h=23,d= 10);
     //restar termistor
     translate ([0,0,6])rotate([0,90 , 0])cylinder (h=10,d= 3);
     // restar tubo
     translate ([0,0,z])scale([1.2,1.2,6]) rotate([180,0 , 0]) upperTube(false);
  }
  if (show)  translate ([0,0,z]) tube(false);

}

module half1() {
      difference(){
          cilindroExt();
          translate ([-15,0,0])cube ([30,30,30]);
      } 
}
module half2() {
      difference(){
          cilindroExt();
          translate ([-15,-30,0])cube ([30,30,30]);
      } 
}

module halves()
{
    half1();
    half2();
    
}

//translate ([0,0,13])halves();
translate ([0,-10,0])half1();
translate ([0,10,0])half2();
//translate ([0,0,0])cilindroExt();

