use <tubo.scad>;
use <led.scad>;
use <threads.scad>;
use <fotoDiodo.scad>;
use <prism.scad>;
include <params.scad>;


module tapa () {
   
   diametroTapa=20;
   altoInterior=8; //5mm inserto + 3mm acrilico 
   diametroInterior= 15+tolerancia;
   altoTapa=altoInterior+7;
   difference(){
            cylinder (h=altoTapa,d= diametroTapa);
            cylinder (h=altoInterior,d= diametroInterior);
            translate ([0,0,altoInterior]) ledSqr();
            translate ([6,-0.5,0]) cube([6,1,8]);
   }
   //translate ([0,0,3])cylinder (h=3,d= 15);
    
}



//soporteSacado();
//translate ([0,0,13])halves();
//translate ([0,-10,0])half1();
//translate ([0,10,0])half2();
//translate ([0,0,0])cilindroInt(false);
//translate ([0,0,0])cilindroExt();
translate ([0,0,70])tapa();






