use <tubo.scad>;


//Params
xDim=15;
yDim=10;
zDim=27;
difference(){
    translate ([0,0,-zDim*0.47])cube ([xDim,yDim,zDim], center=true);
    scale([1.2,1.2,1.2])tube(cutout=false);
    $fn=100;
    translate ([0,0,-17])rotate([acos(0),0 , 0]) cylinder (h=11,d= 5*sqrt(2), center=true); 
}

tube(cutout=false);