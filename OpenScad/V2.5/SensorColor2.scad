z=1.7;
$fn=100;
difference (){
    union (){
    translate ([0,0,z])  {
    
        cube ([5,5,3], center=true);
    }
    cube ([26,28,z], center=true);

}
    translate ([-1.4,-1.4,0]) translate ([13-1.25,14-1.25,-1.5])  {cylinder (h=z*1.5,d= 2.5); }
    translate ([1.4,-1.4,0]) translate ([-13+1.25,14-1.25,-1.5])  {cylinder (h=z*1.5,d= 2.5); }
    translate ([1.4,1.4,0]) translate ([-13+1.25,-14+1.25,-1.5])  {cylinder (h=z*1.5,d= 2.5); }
    translate ([-1.4,1.4,0]) translate ([13-1.25,-14+1.25,-1.5])  {cylinder (h=z*1.5,d= 2.5); }
    }

