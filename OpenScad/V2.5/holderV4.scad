use <tubo.scad>;
use <led.scad>;
use <threads.scad>;
use <fotoDiodo.scad>;
use <prism.scad>;
use <params.scad>;


module soporte(){
  translate ([1-dimX/2,1-dimY/2,0])
  rotate([-90,0 , 0])
  difference(){
      union(){
            //cubo stock       
            translate ([-1,0,-1])rotate([acos(0),0 , 0])cube([dimX,dimY,dimZ]);
            //muescas para afirmar el fd   
   
     }
     //restar el pcb fotodiodo con los componentes (fd_largo) 
     translate ([0,-2,0])rotate([acos(0),0 , 0]) fd_largo2();

 }
     translate ([1-dimX/2,1-dimY/2,0])
  rotate([-90,0 , 0])          translate ([muescaX/2+dimX/2-1,0,0])rotate([0,0,180])cube([muescaX,muescaY,muescaZ]);
       translate ([1-dimX/2,1-dimY/2,0])
  rotate([-90,0 , 0])       translate ([muescaX/2+dimX/2-1,-1,dimY-3])rotate([90,0,180])cube([muescaX,muescaY,muescaZ]);
}


module soporteSacado(){
    difference(){
        soporte();
             //restar el cilindro grande
        translate ([0,0,5])cylinder (h=5,d= dExt+tolerancia);
    }
    
}

module cilindroInt (show=false) {
    //
    //translate ([dimX/2-1,-dimZ-5,1])
    z=21;
    
  difference(){
     //Cilindro stock
      metric_thread (diameter=9, pitch=2, length=19, square=true, thread_size=1.5,
              groove=true, rectangle=0.5);
     //metric_thread (diameter=9, pitch=2, length=19);
     //cylinder (h=19,d= 9);
     //restar canal luz
     cylinder (h=10,d= 3);
     //restar termistor
     translate ([0,0,6])rotate([90,0 , 0])cylinder (h=10,d= 2.5);
     // restar tubo
     scale([1.15,1.15,1.15])translate ([0,0,z]) tube(false);
  }
  if (show)  scale([1.15,1.15,1.15])translate ([0,0,z]) tube(false);  //show tube

}




module cilindroExt (show=false) {
    //
    //translate ([dimX/2-1,-dimZ-5,1])
  z=23;
    
  difference(){
     //Cilindro stock
     cylinder (h=23,d= dExt-tolerancia);
     //restar interno
     cylinder (h=23,d= 10);
     //restar termistor
     translate ([0,0,6])rotate([0,90 , 0])cylinder (h=10,d= 3);
     // restar tubo
     translate ([0,0,z])scale([1.2,1.2,6]) rotate([180,0 , 0]) upperTube(false);
  }
  if (show)  translate ([0,0,z]) tube(false);

}

module half1() {
      difference(){
          cilindroExt();
          translate ([-15,0,0])cube ([30,30,30]);
      } 
}
module half2() {
      difference(){
          cilindroExt();
          translate ([-15,-30,0])cube ([30,30,30]);
      } 
}

module halves()
{
    half1();
    half2();
    
}

module tapa () {
   
   diametroTapa=20;
   altoInterior=8; //5mm inserto + 3mm acrilico 
   diametroInterior= 15+tolerancia;
   altoTapa=altoInterior+7;
   difference(){
            cylinder (h=altoTapa,d= diametroTapa);
            cylinder (h=altoInterior,d= diametroInterior);
            translate ([0,0,altoInterior]) ledSqr();
            translate ([6,-0.5,0]) cube([6,1,8]);
   }
   //translate ([0,0,3])cylinder (h=3,d= 15);
    
}



//soporteSacado();
//translate ([0,0,13])halves();
//translate ([0,-10,0])half1();
//translate ([0,10,0])half2();
//translate ([0,0,0])cilindroInt(false);
//translate ([0,0,0])cilindroExt();
//translate ([0,0,70])tapa();






