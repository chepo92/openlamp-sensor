use <tubo.scad>;
use <led.scad>;
use <threads.scad>;

//Params
xDim=15;
yDim=20;
zDim=26;
corr=0.3;

altoIndentacion=2; 
espesor=2;
altoTapa=5;
altoCuerpo= zDim-altoTapa ; 

diamTermistor=2.2;
altoTermistor=5; 

altoMuestra=7;
altoFondo=10;


$fn=100;

overlapIndent =0.5;
toleranciaEncaje= 0.2;
altoEncaje=altoIndentacion*4;
explotar=10;

module infBox(){   
    difference(){
        translate ([0,0,0])cube ([xDim,yDim,altoCuerpo]);
        translate ([espesor,espesor,espesor+altoFondo])cube ([xDim-2*espesor,yDim-2*espesor,zDim-altoTapa]);
                    //scale([1.15,1.15,1.15])tube(cutout=false);    
                }     
}



module body(ventana){
    
    difference () {
       union    () {
          infBox();
           x=8;
           y=2;
           z=8;
        //   ventana=true;
          if (ventana==true) {
              //ventana lateral del fotodiodo
              translate ([xDim/2,yDim+y/2,altoCuerpo/3])cube ([x,y,z], center=true); 
              //Soportes para la ventana 
              translate ([xDim/2,yDim+y,1.5])cube ([1,1,3], center=true); 
              translate ([xDim/2+x/2,yDim+y,1.5])cube ([1,1,3], center=true); 
              translate ([xDim/2-x/2,yDim+y,1.5])cube ([1,1,3], center=true); 
          }
        }
        translate ([xDim/2,4.5,altoCuerpo/3]) rotate([acos(0),0 , 0]) ledSqr();   //restamos el led
           x=6;
           y=2;
           z=6;
        translate ([xDim/2,yDim+y/2,altoCuerpo/3])cube ([x,y,z],center=true); // restamos el cubo de la ventana
        translate ([xDim/2,yDim/2,0])cylinder (h=altoTermistor,d=diamTermistor ); // restar termistor por abajo
translate ([xDim/2,yDim,altoCuerpo/3]) rotate([acos(0),0 , 0])cylinder (h=yDim,d= 2); // restar canal de luz   
        translate ([xDim/2,yDim/2,zDim-2]) rotate ([0,0,acos(0)]) tube(cutout=false); //restar el tubo
        }

        
}

 

module tapa()
{
   translate ([0,0,zDim+explotar*2]) difference(){
        translate ([0,0,0])cube ([xDim,yDim,altoTapa]);
        translate ([espesor,espesor,0])cube ([xDim-2*espesor,yDim-2*espesor,altoTapa-espesor]);
        //scale([1.15,1.15,1.15])tube(cutout=false);
        
        
    }  
}


module indentacion()
{
   translate ([-espesor-toleranciaEncaje,-espesor-toleranciaEncaje,altoCuerpo+explotar]) union (){
        
    difference(){
        
    cube ([(xDim+espesor*2+toleranciaEncaje*2),(yDim+espesor*2+toleranciaEncaje*2),altoEncaje]);
        
    translate ([espesor,espesor,0])cube ([xDim+toleranciaEncaje*2,yDim+toleranciaEncaje*2,altoIndentacion*5.1]);
    }
    
    difference(){
        translate ([espesor-overlapIndent,espesor-overlapIndent,altoEncaje/3])cube ([xDim+overlapIndent*2,yDim+2*overlapIndent,altoIndentacion]);

        translate ([espesor+ xDim/2,espesor+yDim/2,altoEncaje/2]) rotate ([0,0,acos(0)]) tube(cutout=false);
        
    }
    
    }
}
xsprbox=xDim;
ysprbox=xDim;
zsprbox=xDim;

module box_spr(){   
   
        union(){
        difference(){
            translate ([0,0,0])cube ([xDim,yDim,zDim/2]);
            translate ([xDim/2,yDim/2,espesor])cylinder (h=zDim,d=spring_d); 
            translate ([xDim/2,yDim/2,-corr])cylinder (h=zDim,d=diamTermistor ); // restar termistor por abajo// restar termistor por abajo
//       translate ([espesor,espesor,espesor])cube ([xDim-2*espesor,yDim-2*espesor,zDim-altoTapa]);
                    //scale([1.15,1.15,1.15])tube(cutout=false); 
            }
        }
        difference(){
            translate ([xDim/2,yDim/2,espesor])cylinder (h=zDim/8,d=diamTermistor*3);  
            translate ([xDim/2,yDim/2,-corr])cylinder (h=zDim,d=diamTermistor); 
}}

spring_d=10;
module spring(){   
     difference(){
         union(){
            translate ([xDim/2,yDim/2,0])cylinder (h=zDim/8,d=diamTermistor*3);   
            translate ([xDim/2,yDim/2,espesor]) cylinder (h=espesor,d=spring_d-corr);  
         }
         translate ([xDim/2,yDim/2,-corr])cylinder (h=zDim,d=diamTermistor );}     
}
 
module holderv2b() {
    //tapa();
    //indentacion();
    body();
}
spring();
translate([ 0.00, 0.00, -15]) box_spr();
//holderv2b();
