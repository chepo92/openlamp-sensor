
// millimeters

$fn = 100;
nothing = 0.05;

material_thickness = 0.8;

// lid
lid_length = 8.4; //8.4
lid_center_diameter = 7;
lid_appendix_diameter = 2;
lid_height = 0.8;
lid_center_radius = lid_center_diameter/2;
lid_appendix_radius = lid_appendix_diameter/2;
lid_appendix_excentricity = lid_length - lid_center_diameter/2 - lid_appendix_diameter/2;

//lid joint
joint_x = 3.5;
joint_y = 2.68;
joint_z = 1.55;


body_top_outer_diameter = 7;
body_top_inner_diameter = 5.7;
body_top_height = 0.77;
body_top_outer_radius = body_top_outer_diameter/2;
body_top_inner_radius = body_top_inner_diameter/2;

body_center_diameter = 6.2;
body_center_height =8.6;
body_center_radius = body_center_diameter/2;

body_cone_upper_diameter = body_center_diameter;
body_cone_lower_diameter = 4;
body_cone_height = 9.4;
body_cone_upper_radius = body_cone_upper_diameter/2;
body_cone_lower_radius = body_cone_lower_diameter/2;

// half sphere
body_bottom_radius = 2;

//Total height
total_height = body_bottom_radius+ body_cone_height +body_center_height +body_top_height;