use <led.scad>;
include <params.scad>;

module tapa () { 
   difference(){
            cylinder (h=altoTapa,d= diametroTapaExterior);
            cylinder (h=altoTapaInterior,d= diametroTapaInterior);
            translate ([0,0,altoTapaInterior]) ledSqr();
            translate ([6,-0.5,0]) cube([6,1,8]);
   }
}

translate ([0,0,70])tapa();






