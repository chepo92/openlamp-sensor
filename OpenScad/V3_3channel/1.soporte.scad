use <tubo.scad>;
use <led.scad>;
use <3.Ext.scad>;
use <canales.scad>;
use <fotoDiodo.scad>;
include  <params.scad>;

module soporte(){
  translate ([-dimX/2,-dimY/2,0])
  difference(){
        //cubo stock       
        translate ([0,0,0])cube([dimX,dimY,dimZ]);
         //restar el pcb fotodiodo con los componentes (fd_largo) 
        translate ([grosorPared,grosorPared,10]) fd_largo2();

 }
  
 muescas();   
}

module muescas(){
    translate ([dimX/2-grosorPared,-muescaX/2,0])rotate([0,0 , 90])cube([muescaX,muescaY,muescaZ]);
    translate ([-dimX/2+grosorPared+muescaY,-muescaX/2,0])rotate([0,0 , 90]) cube([muescaX,muescaY,muescaZ]);
}



module soporteSacado(){
    
    difference(){
        soporte();
        //restar el ext
        h=30;
        translate ([-ladoSensor/2,0,dimZ/2+5]) {  rotate([0,90,0])siluetteExt(); }
        translate ([0,0,dimZ/2+5]) {  rotate([-90,0,-90])canalesLED(); }
        translate ([-dimX-3-7,-dimY/2-1,dimZ/2]) {
            cube([dimX,dimY+2,dimZ]);
        }
        //canal de cable    
        y=1.5;
        translate ([-dimX/2,-y/2,dimZ-4])cube([dimX+1,y,5]);
        }
    
        //translate ([dimY/2+1,0,dimZ/2+3]) rotate([0,90,0])tube();
        
}
//cube([5,5,10]);
soporteSacado();
