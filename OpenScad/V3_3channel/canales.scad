include  <params.scad>;

module canalesInternos(){
    //restar canal luz     
    translate ([0,0,diametroCanalLuz/2])rotate([0,-90 , 0])cylinder (h=20,d= diametroCanalLuz);
    translate ([0,0,diametroCanalLuz/2])rotate([-90,0 , 0])cylinder (h=20,d= diametroCanalLuz);
    translate ([0,0,diametroCanalLuz/2])rotate([180,0 , 0])cylinder (h=20,d= diametroCanalLuz);
    //translate ([0,0,diametroCanalLuz/2])rotate([90,0 , 0])cylinder (h=20,d= diametroCanalLuz);
    translate ([0,0,diametroCanalLuz/2])rotate([0,90 , 0])cylinder (h=20,d= diametroCanalLuz);
    //restar termistor    
    
}

module canalTermistor(){
    translate ([0,0,posVerticalTermistor])rotate([90,0 , 0])cylinder (h=20,d= 2.2);
    }

module canalesLED(){ 
    translate ([0,0,0])rotate([0,-90 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([-90,0 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([180,0 , 0])cylinder (h=20,d= diametroLED);
    translate ([0,0,0])rotate([0,90 , 0])cylinder (h=20,d= diametroLED);

}

canalTermistor();
canalesInternos();

canalesLED();