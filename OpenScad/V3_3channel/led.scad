//Params
include  <params.scad>;

module ledCir() {
union(){
    translate ([0,0,altoLED]) cylinder (h=altoMuesca,d= diametroMuescaLED); 
    cylinder (h=altoLED,d= diametroLED); 
    translate ([0,0,0]) sphere(d=diametroLED);
}
}

module ledSqr() {

cylinder (h=altoSqr,d= diametroLED); 
    

}
ledSqr();