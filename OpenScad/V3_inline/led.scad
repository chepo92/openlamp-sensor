//use <tubo.scad>;


//Params
diam=5.1;
Dmuesca=6;
alto=4.5;
altoSqr=7;
altoMuesca=1.3;
altoTotal= diam/2 + alto +altoMuesca;
$fn=100; 
module ledCir() {
union(){
    translate ([0,0,alto]) cylinder (h=altoMuesca,d= Dmuesca); 
    cylinder (h=alto,d= diam); 
    translate ([0,0,0]) sphere(d=diam);
}
}

module ledSqr() {

cylinder (h=altoSqr,d= diam); 
    

}
ledSqr();