include <params.scad>;

module pcb(largo=false) {
    if (largo==false) {  
        cube([largoPcb,anchoPcb,altoPcb]);        
    }
    else {
        delta=20;
        translate ([0,0,-delta])cube([largoPcb,anchoPcb,altoPcb+delta]);      
    }    
    //conectores();


}
module pins(){
    cube([pinX,pinY,pinZ]);
}

module conectores(){
    translate ([0,anchoPcb/2-pinY/2,,-pinZ*0.7]) pins();
    translate ([largoPcb-pinX,anchoPcb/2-pinY/2,-pinZ*0.7]) pins();
}

module sensor(largoCuad=false, largoCil=false) {
   if (largoCuad==false && largoCil==false) {
    difference(){
        cube([ladoSensor,ladoSensor,altoSensor]);  
        translate ([ladoSensor/2,ladoSensor/2,altoSensor-altoMuesca]) cylinder (h=altoMuesca,d= Dmuesca); 
    }   
   }else if (largoCuad==true) {
        difference(){
            cube([ladoSensor,ladoSensor,altoSensor*5]);  
            translate ([ladoSensor/2,ladoSensor/2,5*altoSensor-altoMuesca]) cylinder (h=altoMuesca,d= Dmuesca); 
        }
   }else if (largoCil==true) {
        //difference(){
                cube([ladoSensor,ladoSensor,altoSensor]);  
        translate ([ladoSensor/2,ladoSensor/2,altoSensor-altoMuesca]) cylinder (h=30,d= Dmuesca); 
        //}
   }
}

module fd() {
pcb();
translate ([largoPcb/2-ladoSensor/2,anchoPcb/2-ladoSensor/2,altoPcb]) sensor();
}

module fd_largo() {
    pcb(true);
    translate ([largoPcb/2-ladoSensor/2,anchoPcb/2-ladoSensor/2,altoPcb])  sensor();
}

module fd_largo2() {
    translate ([0,0,-altoPcb]) {
    pcb(true);
    translate ([largoPcb/2-ladoSensor/2,anchoPcb/2-ladoSensor/2,altoPcb]) sensor(true,false);
    }
}

fd_largo2();