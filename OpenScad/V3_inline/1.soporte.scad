use <tubo.scad>;
use <led.scad>;
use <fotoDiodo.scad>;
include  <params.scad>;

module soporte(){
  translate ([-dimX/2,-dimY/2,0])
  difference(){
      union(){
            //cubo stock       
            translate ([0,0,0])cube([dimX,dimY,dimZ]);
 
   
     }
     //restar el pcb fotodiodo con los componentes (fd_largo) 
     translate ([grosorPared,grosorPared,dimZ/3+1]) fd_largo2();

 }
  
 muescas();   
}

module muescas(){
    translate ([dimX/2-grosorPared,-muescaX/2,0])rotate([0,0 , 90])cube([muescaX,muescaY,muescaZ]);
    translate ([-dimX/2+grosorPared+muescaY,-muescaX/2,0])rotate([0,0 , 90]) cube([muescaX,muescaY,muescaZ]);
}



module soporteSacado(){
    difference(){
        soporte();
             //restar el cilindro grande
        h=4+3;
        translate ([0,0,dimZ-h])cylinder (h+1,d= diametroExteriorCilindroExterior+tolerancia);
    }
        
}

soporteSacado();
