// millimeters
$fn=100;
tolerancia=0.5;

//Canal de luz
 diametroCanalLuz=3;

//Termistor
posVerticalTermistor=4.6;

//Cilindro interior
diametroExteriorCilindroInterior= 10;
alturaCilindroInterior= 19;

//Cilindro exterior
alturaCilindroExterior=23; 
diametroExteriorCilindroExterior=15;

// Tapa con led
diametroTapaExterior=20;
altoTapaInterior=8; //5mm inserto + 3mm acrilico 
diametroTapaInterior= 15+tolerancia;
altoTapa=altoTapaInterior+7;

//Sensor
ladoSensor=5.5;
altoSensor=2;

Dmuesca=2;
altoMuesca=0.1;

largoPcb=29;
anchoPcb=27;
altoPcb=1.5;

pinX=4;
pinY=11;
pinZ=12;

//Soporte Sensor
grosorPared=1;
dimX=largoPcb+grosorPared*2;
dimY=anchoPcb+grosorPared*2;
dimZ=27;

muescaX=5;
muescaY=1;
muescaZ=1;