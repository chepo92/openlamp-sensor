use <tubo.scad>;
use <threads.scad>;
use <canales.scad>;
include  <params.scad>;


module cilindroInt (show=false) {
    //
    //translate ([dimX/2-1,-dimZ-5,1])
    z=21;
    
  difference(){
     //Cilindro stock
     rotate([0,0 , 180]) metric_thread (diameter=diametroExteriorCilindroInterior-tolerancia, pitch=2.7, length=alturaCilindroInterior, square=true, thread_size=1, groove=true, rectangle=1);
    canalesInternos();
     // restar tubo
     scale([1,1,1])translate ([0,0,z]) tube(false);
  }
  if (show) translate ([0,0,z]) tube(false);  //show tube

}


translate ([0,0,0])cilindroInt(false);

