use <tubo.scad>;
use <canales.scad>;
include  <params.scad>;

module ext (show=false) {
  z=23;
    
  difference(){
     //Cubo stock
     translate ([-diametroExteriorCilindroExterior/2,-diametroExteriorCilindroExterior/2,0])cube ([diametroExteriorCilindroExterior-tolerancia,diametroExteriorCilindroExterior-tolerancia,alturaCilindroExterior]);
     //restar cilindro interno
     cylinder (h=alturaCilindroExterior,d= diametroExteriorCilindroInterior+ tolerancia);
    //Restar canales
     canalesInternos();
     // restar forma superior del tubo
     translate ([0,0,z])scale([1.3,1.3,6]) rotate([180,0 , 0]) upperTube(false);
  }
  if (show)  translate ([0,0,z]) tube(false);

}

module half1() {
      difference(){
          ext();
          translate ([0,-15,0])cube ([30,30,30]);
      } 
      
}
module half2() {
      difference(){
          ext();
          translate ([-30,-15,0])cube ([30,30,30]);
      } 
       
}

module halves()
{
    half1();
    half2();
    
}

module siluetteExt(){
 translate ([-(diametroExteriorCilindroExterior+tolerancia)/2,-(diametroExteriorCilindroExterior+tolerancia)/2,0])cube ([diametroExteriorCilindroExterior+tolerancia,diametroExteriorCilindroExterior+tolerancia,alturaCilindroExterior]);

}

siluetteExt();
//ext();
//halves();
//translate ([-5,0,0])half1();
//translate ([5,0,0])half2();
//translate ([0,0,0])cilindroExt();

