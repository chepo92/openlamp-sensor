include <arduino.scad>

//Arduino boards
//You can create a boxed out version of a variety of boards by calling the arduino() module
//The default board for all functions is the Uno

dueDimensions = boardDimensions( DUE );
unoDimensions = boardDimensions( UNO );

//Board mockups


translate( [0, 0, 8] )
	arduino(MEGA);


translate([0, 0, 0]) {
	enclosure(MEGA,heightExtension= 25);
}

translate([0, 0, 1.5 + 8 + 36 ]) {
	enclosureLid(MEGA);
}
