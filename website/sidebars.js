module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Open LAMP Documentation',
      items: [
        '1.intro',
        '2.design',
        '3.bom',
        '4.pcb',
        '5.wiring',
        '6.programming',
        '7.operation',
        '8.data',
        '9.todo',      
      //  'start',
      //  'create-a-page',
      //  'create-a-document',
      //  'create-a-blog-post',
      //  'thank-you',
      //  'markdown-features',
      ],
    },
  ],
};



