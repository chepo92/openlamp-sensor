---
title: Introduction
slug: /
---

## Open LAMP

System to perform isothermal amplification reactions (LAMP) in Eppendorf tubes, with digital control, measurement and data logging. 

## LAMP Reactions

Loop-mediated isothermal amplification (LAMP) amplifies DNA with high specificity, efficiency and rapidity under isothermal conditions (Notomi, 2000). 


Loop-mediated isothermal amplification (LAMP) is a single-tube technique for the amplification of DNA and a low-cost alternative to detect certain diseases. Reverse transcription loop-mediated isothermal amplification (RT-LAMP) combines LAMP with a reverse transcription step to allow the detection of RNA.
LAMP is an isothermal nucleic acid amplification technique. In contrast to the polymerase chain reaction (PCR) technology, in which the reaction is carried out with a series of alternating temperature steps or cycles, isothermal amplification is carried out at a constant temperature, and does not require a thermal cycler.
(Source: https://en.wikipedia.org/wiki/Loop-mediated_isothermal_amplification)

## About this device

This LAMP device allows to make LAMP isothermal reactions using a thermistor and a NiCrom wire heater with a PID temperature control loop, it also can record the reaction evolution through time with a color sensor (RGB photodiode spectrometer) and UV illumination, measuring the light retransmited by each tube. The data collected can be directly captured by serial communicaion or stored in a SD card, then it can be analyzed and used to compare the reactions (detection vs/no detection), speed reaction, among others. The device is inspired in work done by (Velders, Schoen & Saggiomo, 2018)

## Current version

Current version is v5 for software and hardware which are documented here

## Showcase

![Assembled](../static/img/assembled.jpg)

![Modules](../static/img/modules.jpg)


## References 

Notomi, T. (2000). Loop-mediated isothermal amplification of DNA. Nucleic Acids Research, 28(12), 63e-63. doi: 10.1093/nar/28.12.e63

Velders, A., Schoen, C., & Saggiomo, V. (2018). Loop-mediated isothermal amplification (LAMP) shield for Arduino DNA detection. BMC Research Notes, 11(1). doi: 10.1186/s13104-018-3197-9