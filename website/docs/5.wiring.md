---
title: Assembly
---

## Assembly 
- Wire Heater Winding with Int.Cyl
 
- External Cyl and thermistor
 
- Cap and Led

- PhotodiodeHolder

## Wiring

-	Connect Tube holder, Heater, Led, and photodiode to PCB Sensor Module

-	Connect PCB sensor modules to Control PCB

-	Connect SD card module to control PCB

-	Connect Control PCB to Arduino

-	Connect Power
