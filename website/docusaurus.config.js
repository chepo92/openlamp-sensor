/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Open LAMP',
  tagline: 'An open source device to perform LAMP reactions',
  url: 'https://chepo92.gitlab.io/',
  baseUrl: '/openlamp-sensor/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: '/img/favicon.png',
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'OpenLAMP', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'OpenLAMP',
      logo: {
        alt: 'Site Logo',
        src: '/img/logo_a.png',
        srcDark: '/img/logo_dark_a.png', // Default to `logo.src`.
        //href: './', // Default to `siteConfig.baseUrl`.
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
      // {
      //   to: 'page1',
      //   label: 'Page 1',
      //   position: 'left',
      // },        
      // {
      //     to: 'blog', 
      //     label: 'Blog', 
      //     position: 'left'
      // },
        {
          href: 'https://gitlab.com/chepo92/openlamp-sensor',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Documentation',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Contact',
          items: [
            {
              label: 'GitLab Issues',
              href: 'https://gitlab.com/chepo92/openlamp-sensor/-/issues',
            },
           // {
           //   label: 'Discord',
           //   href: 'https://discordapp.com/invite/docusaurus',
           // },
           // {
           //   label: 'Twitter',
           //   href: 'https://twitter.com/docusaurus',
           // },
          ],
        },
        {
          title: 'More',
          items: [
           // {
           //   label: 'Blog',
           //   to: 'blog',
           // },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/chepo92/openlamp-sensor/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} OpenLAMP Sensor. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/chepo92/openlamp-sensor/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
