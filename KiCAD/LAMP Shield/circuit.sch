EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino
LIBS:circuit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 5A9F02C7
P 5050 6300
F 0 "#PWR01" H 5050 6050 50  0001 C CNN
F 1 "GND" H 5050 6150 50  0000 C CNN
F 2 "" H 5050 6300 50  0001 C CNN
F 3 "" H 5050 6300 50  0001 C CNN
	1    5050 6300
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5A9F09F3
P 4400 5750
F 0 "R1" V 4480 5750 50  0000 C CNN
F 1 "R" V 4400 5750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4330 5750 50  0001 C CNN
F 3 "" H 4400 5750 50  0001 C CNN
	1    4400 5750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5050 5950 5050 6200
Wire Wire Line
	5050 6200 5050 6300
Wire Wire Line
	4550 5750 4700 5750
Wire Wire Line
	4700 5750 4750 5750
Connection ~ 4700 5750
Connection ~ 5050 6200
$Comp
L Conn_01x02 J3
U 1 1 5AA03BB2
P 4650 4900
F 0 "J3" H 4650 5000 50  0000 C CNN
F 1 "Conn_01x02" H 4650 4700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 4650 4900 50  0001 C CNN
F 3 "" H 4650 4900 50  0001 C CNN
	1    4650 4900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4850 5000 5050 5000
$Comp
L GND #PWR02
U 1 1 5AB0939F
P 2550 6200
F 0 "#PWR02" H 2550 5950 50  0001 C CNN
F 1 "GND" H 2550 6050 50  0000 C CNN
F 2 "" H 2550 6200 50  0001 C CNN
F 3 "" H 2550 6200 50  0001 C CNN
	1    2550 6200
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5AB093A5
P 1900 5650
F 0 "R2" V 1980 5650 50  0000 C CNN
F 1 "R" V 1900 5650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1830 5650 50  0001 C CNN
F 3 "" H 1900 5650 50  0001 C CNN
	1    1900 5650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2550 5850 2550 6100
Wire Wire Line
	2550 6100 2550 6200
Wire Wire Line
	2050 5650 2200 5650
Wire Wire Line
	2200 5650 2250 5650
$Comp
L Jack-DC J7
U 1 1 5AB093AD
P 3450 4900
F 0 "J7" H 3450 5110 50  0000 C CNN
F 1 "Jack-DC" H 3450 4725 50  0000 C CNN
F 2 "Connectors:JACK_ALIM" H 3500 4860 50  0001 C CNN
F 3 "" H 3500 4860 50  0001 C CNN
	1    3450 4900
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 5AB093B3
P 3050 5150
F 0 "#PWR03" H 3050 4900 50  0001 C CNN
F 1 "GND" H 3050 5000 50  0000 C CNN
F 2 "" H 3050 5150 50  0001 C CNN
F 3 "" H 3050 5150 50  0001 C CNN
	1    3050 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4900 3050 4900
Wire Wire Line
	3050 4900 3050 5000
Wire Wire Line
	3050 5000 3050 5150
Wire Wire Line
	3150 5000 3050 5000
Connection ~ 3050 5000
Connection ~ 2200 5650
Connection ~ 2550 5100
Connection ~ 2550 6100
Wire Wire Line
	1350 5650 1750 5650
$Comp
L PWR_FLAG #FLG04
U 1 1 5AB093CF
P 2650 4600
F 0 "#FLG04" H 2650 4675 50  0001 C CNN
F 1 "PWR_FLAG" H 2650 4750 50  0000 C CNN
F 2 "" H 2650 4600 50  0001 C CNN
F 3 "" H 2650 4600 50  0001 C CNN
	1    2650 4600
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J6
U 1 1 5AB093DD
P 2150 4800
F 0 "J6" H 2150 4900 50  0000 C CNN
F 1 "Conn_01x02" H 2150 4600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2150 4800 50  0001 C CNN
F 3 "" H 2150 4800 50  0001 C CNN
	1    2150 4800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2350 4800 2650 4800
Wire Wire Line
	2650 4800 2850 4800
Wire Wire Line
	2850 4800 3150 4800
Wire Wire Line
	2650 4600 2650 4800
Connection ~ 2650 4800
Wire Wire Line
	2350 4900 2550 4900
Wire Wire Line
	2550 4900 2550 5100
Wire Wire Line
	2550 5100 2550 5450
$Comp
L GND #PWR05
U 1 1 5AB097DB
P 2550 3600
F 0 "#PWR05" H 2550 3350 50  0001 C CNN
F 1 "GND" H 2550 3450 50  0000 C CNN
F 2 "" H 2550 3600 50  0001 C CNN
F 3 "" H 2550 3600 50  0001 C CNN
	1    2550 3600
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5AB097E1
P 2550 2300
F 0 "R3" V 2750 2300 50  0000 C CNN
F 1 "R" V 2550 2300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2480 2300 50  0001 C CNN
F 3 "" H 2550 2300 50  0001 C CNN
	1    2550 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 3050 2550 3500
Wire Wire Line
	2550 3500 2550 3600
Connection ~ 2550 2500
Connection ~ 2550 3500
$Comp
L Conn_01x02 J5
U 1 1 5AB09819
P 2200 2950
F 0 "J5" H 2200 3050 50  0000 C CNN
F 1 "Conn_01x02" H 2200 2750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2200 2950 50  0001 C CNN
F 3 "" H 2200 2950 50  0001 C CNN
	1    2200 2950
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2550 2450 2550 2500
Wire Wire Line
	2550 2500 2550 2650
Wire Wire Line
	2550 2650 2550 2950
Wire Wire Line
	2550 1750 2550 1900
Wire Wire Line
	2550 1900 2550 2150
Connection ~ 2550 2650
$Comp
L GND #PWR06
U 1 1 5AB0A6E1
P 5700 3650
F 0 "#PWR06" H 5700 3400 50  0001 C CNN
F 1 "GND" H 5700 3500 50  0000 C CNN
F 2 "" H 5700 3650 50  0001 C CNN
F 3 "" H 5700 3650 50  0001 C CNN
	1    5700 3650
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5AB0A6E7
P 5700 2250
F 0 "R4" V 5750 2200 50  0000 C CNN
F 1 "R" V 5700 2250 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5630 2250 50  0001 C CNN
F 3 "" H 5700 2250 50  0001 C CNN
	1    5700 2250
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J9
U 1 1 5AB0A6F1
P 5500 3000
F 0 "J9" H 5500 3100 50  0000 C CNN
F 1 "Conn_01x02" H 5500 2800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5500 3000 50  0001 C CNN
F 3 "" H 5500 3000 50  0001 C CNN
	1    5500 3000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5700 2400 5700 2800
Wire Wire Line
	5700 2800 5700 3000
Wire Wire Line
	5700 1700 5700 1850
Wire Wire Line
	5700 1850 5700 2100
$Comp
L IRF540N Q1
U 1 1 5AB0A84D
P 2450 5650
F 0 "Q1" H 2700 5725 50  0000 L CNN
F 1 "IRF540N" H 2700 5650 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 2700 5575 50  0001 L CIN
F 3 "" H 2450 5650 50  0001 L CNN
	1    2450 5650
	1    0    0    -1  
$EndComp
$Comp
L IRF540N Q2
U 1 1 5AB0A8A2
P 4950 5750
F 0 "Q2" H 5200 5825 50  0000 L CNN
F 1 "IRF540N" H 5200 5750 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 5200 5675 50  0001 L CIN
F 3 "" H 4950 5750 50  0001 L CNN
	1    4950 5750
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR07
U 1 1 5AB12647
P 2550 1750
F 0 "#PWR07" H 2550 1600 50  0001 C CNN
F 1 "+5V" H 2550 1890 50  0000 C CNN
F 2 "" H 2550 1750 50  0001 C CNN
F 3 "" H 2550 1750 50  0001 C CNN
	1    2550 1750
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR08
U 1 1 5AB12679
P 5700 1700
F 0 "#PWR08" H 5700 1550 50  0001 C CNN
F 1 "+5V" H 5700 1840 50  0000 C CNN
F 2 "" H 5700 1700 50  0001 C CNN
F 3 "" H 5700 1700 50  0001 C CNN
	1    5700 1700
	1    0    0    -1  
$EndComp
$Comp
L Arduino_Uno_Shield XA1
U 1 1 5AB12800
P 8200 2650
F 0 "XA1" V 8300 2650 60  0000 C CNN
F 1 "Arduino_Uno_Shield" V 8100 2650 60  0000 C CNN
F 2 "arduino:Arduino_Uno_Shield" H 10000 6400 60  0001 C CNN
F 3 "" H 10000 6400 60  0001 C CNN
	1    8200 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2650 6750 2700
Wire Wire Line
	6750 2700 6900 2700
Wire Wire Line
	6750 2650 2550 2650
Text GLabel 1350 5650 0    60   Input ~ 0
Pwm1
Text GLabel 10300 2400 2    60   Input ~ 0
Pwm1
Text GLabel 4150 5750 0    60   Input ~ 0
Pwm2
Text GLabel 10650 2500 2    60   Input ~ 0
Pwm2
Wire Wire Line
	9500 2400 10300 2400
Wire Wire Line
	9500 2500 10650 2500
$Comp
L +5V #PWR09
U 1 1 5AB13434
P 6500 3550
F 0 "#PWR09" H 6500 3400 50  0001 C CNN
F 1 "+5V" H 6500 3690 50  0000 C CNN
F 2 "" H 6500 3550 50  0001 C CNN
F 3 "" H 6500 3550 50  0001 C CNN
	1    6500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3600 6500 3600
Wire Wire Line
	6500 3600 6900 3600
Wire Wire Line
	6500 3600 6500 3550
Wire Wire Line
	2850 4800 2850 4550
Wire Wire Line
	2850 4550 4000 4550
Wire Wire Line
	4000 4550 4950 4550
Wire Wire Line
	4950 4200 4950 4550
Wire Wire Line
	4950 4550 4950 4900
Connection ~ 4950 4900
Connection ~ 2850 4800
Wire Wire Line
	4950 4900 4850 4900
Text GLabel 2450 1900 0    60   Input ~ 0
+5
Wire Wire Line
	2550 1900 2450 1900
Connection ~ 2550 1900
Text GLabel 5600 1850 0    60   Input ~ 0
+5
Text GLabel 6350 3600 0    60   Input ~ 0
+5
Connection ~ 6500 3600
Wire Wire Line
	5700 1850 5600 1850
Connection ~ 5700 1850
Wire Wire Line
	4150 5750 4250 5750
Wire Wire Line
	5050 5000 5050 5550
$Comp
L GND #PWR010
U 1 1 5AB1463D
P 6250 3250
F 0 "#PWR010" H 6250 3000 50  0001 C CNN
F 1 "GND" H 6250 3100 50  0000 C CNN
F 2 "" H 6250 3250 50  0001 C CNN
F 3 "" H 6250 3250 50  0001 C CNN
	1    6250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3200 6250 3250
Wire Wire Line
	6650 3300 6900 3300
Wire Wire Line
	6650 3200 6650 3300
Wire Wire Line
	6650 3300 6650 3400
Wire Wire Line
	6650 3400 6900 3400
Connection ~ 6650 3300
Wire Wire Line
	6250 3200 6650 3200
Wire Wire Line
	6650 3200 6900 3200
$Comp
L R R5
U 1 1 5AB14A3F
P 8950 4600
F 0 "R5" V 9030 4600 50  0000 C CNN
F 1 "R" V 8950 4600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8880 4600 50  0001 C CNN
F 3 "" H 8950 4600 50  0001 C CNN
	1    8950 4600
	0    -1   -1   0   
$EndComp
$Comp
L R R6
U 1 1 5AB14B06
P 8950 5250
F 0 "R6" V 9030 5250 50  0000 C CNN
F 1 "R" V 8950 5250 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8880 5250 50  0001 C CNN
F 3 "" H 8950 5250 50  0001 C CNN
	1    8950 5250
	0    -1   -1   0   
$EndComp
$Comp
L LED D1
U 1 1 5AB14C0E
P 9400 4600
F 0 "D1" H 9400 4700 50  0000 C CNN
F 1 "LED" H 9400 4500 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 9400 4600 50  0001 C CNN
F 3 "" H 9400 4600 50  0001 C CNN
	1    9400 4600
	-1   0    0    1   
$EndComp
$Comp
L LED D2
U 1 1 5AB14C7C
P 9400 5250
F 0 "D2" H 9400 5350 50  0000 C CNN
F 1 "LED" H 9400 5150 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 9400 5250 50  0001 C CNN
F 3 "" H 9400 5250 50  0001 C CNN
	1    9400 5250
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR011
U 1 1 5AB14D2A
P 9800 4850
F 0 "#PWR011" H 9800 4600 50  0001 C CNN
F 1 "GND" H 9800 4700 50  0000 C CNN
F 2 "" H 9800 4850 50  0001 C CNN
F 3 "" H 9800 4850 50  0001 C CNN
	1    9800 4850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 5AB14DA5
P 9800 5450
F 0 "#PWR012" H 9800 5200 50  0001 C CNN
F 1 "GND" H 9800 5300 50  0000 C CNN
F 2 "" H 9800 5450 50  0001 C CNN
F 3 "" H 9800 5450 50  0001 C CNN
	1    9800 5450
	1    0    0    -1  
$EndComp
Text GLabel 8500 4600 0    60   Input ~ 0
ledR
Text GLabel 8500 5250 0    60   Input ~ 0
ledG
Wire Wire Line
	8500 4600 8800 4600
Wire Wire Line
	9100 4600 9250 4600
Wire Wire Line
	9550 4600 9800 4600
Wire Wire Line
	9800 4600 9800 4850
Wire Wire Line
	8500 5250 8800 5250
Wire Wire Line
	9100 5250 9250 5250
Wire Wire Line
	9550 5250 9800 5250
Wire Wire Line
	9800 5250 9800 5450
Text GLabel 10450 2200 2    60   Input ~ 0
ledR
Text GLabel 10700 2300 2    60   Input ~ 0
ledG
Wire Wire Line
	9500 2300 10700 2300
Wire Wire Line
	9500 2200 9700 2200
Wire Wire Line
	9700 2200 10450 2200
Wire Wire Line
	4950 4200 6700 4200
Wire Wire Line
	6700 4200 6750 4200
Wire Wire Line
	6750 4200 6750 3700
Wire Wire Line
	6750 3700 6900 3700
Connection ~ 4950 4550
Wire Wire Line
	6900 2800 5700 2800
Connection ~ 5700 2800
Wire Wire Line
	5700 3100 5700 3650
$Comp
L CP C1
U 1 1 5ACB7568
P 4000 4800
F 0 "C1" H 4025 4900 50  0000 L CNN
F 1 "CP" H 4025 4700 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P5.00mm" H 4038 4650 50  0001 C CNN
F 3 "" H 4000 4800 50  0001 C CNN
	1    4000 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4650 4000 4550
Connection ~ 4000 4550
$Comp
L GND #PWR013
U 1 1 5ACB7643
P 4000 5100
F 0 "#PWR013" H 4000 4850 50  0001 C CNN
F 1 "GND" H 4000 4950 50  0000 C CNN
F 2 "" H 4000 5100 50  0001 C CNN
F 3 "" H 4000 5100 50  0001 C CNN
	1    4000 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4950 4000 5100
$Comp
L CP C2
U 1 1 5ACB797D
P 6700 4550
F 0 "C2" H 6725 4650 50  0000 L CNN
F 1 "CP" H 6725 4450 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P5.00mm" H 6738 4400 50  0001 C CNN
F 3 "" H 6700 4550 50  0001 C CNN
	1    6700 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4200 6700 4400
$Comp
L GND #PWR014
U 1 1 5ACB7984
P 6700 4850
F 0 "#PWR014" H 6700 4600 50  0001 C CNN
F 1 "GND" H 6700 4700 50  0000 C CNN
F 2 "" H 6700 4850 50  0001 C CNN
F 3 "" H 6700 4850 50  0001 C CNN
	1    6700 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4700 6700 4850
Connection ~ 6700 4200
Connection ~ 9700 2200
Wire Wire Line
	9500 2100 9950 2100
Wire Wire Line
	9950 2000 9500 2000
Wire Wire Line
	9500 1900 9950 1900
Wire Wire Line
	9950 1800 9500 1800
Wire Wire Line
	9500 1700 9950 1700
Wire Wire Line
	9950 1600 9500 1600
$Comp
L GND #PWR015
U 1 1 5ACEDAD6
P 10200 900
F 0 "#PWR015" H 10200 650 50  0001 C CNN
F 1 "GND" H 10200 750 50  0000 C CNN
F 2 "" H 10200 900 50  0001 C CNN
F 3 "" H 10200 900 50  0001 C CNN
	1    10200 900 
	1    0    0    -1  
$EndComp
Text GLabel 9500 1100 0    60   Input ~ 0
+5
Wire Wire Line
	9500 1100 9650 1100
Wire Wire Line
	9850 800  10200 800 
Wire Wire Line
	10200 800  10200 900 
$Comp
L Conn_01x06 J1
U 1 1 5ACEE0FD
P 10150 1800
F 0 "J1" H 10150 2100 50  0000 C CNN
F 1 "Conn_01x06" H 10150 1400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 10150 1800 50  0001 C CNN
F 3 "" H 10150 1800 50  0001 C CNN
	1    10150 1800
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J2
U 1 1 5ACEE239
P 10300 1350
F 0 "J2" H 10300 1450 50  0000 C CNN
F 1 "Conn_01x02" H 10300 1150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10300 1350 50  0001 C CNN
F 3 "" H 10300 1350 50  0001 C CNN
	1    10300 1350
	1    0    0    1   
$EndComp
Wire Wire Line
	9650 1100 9650 1350
Wire Wire Line
	9650 1350 10100 1350
Wire Wire Line
	9850 1250 10100 1250
Wire Wire Line
	9850 1250 9850 800 
Wire Wire Line
	2550 2950 2400 2950
Wire Wire Line
	2400 3050 2550 3050
Connection ~ 6650 3200
$EndSCHEMATC
