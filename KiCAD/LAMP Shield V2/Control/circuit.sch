EESchema Schematic File Version 4
LIBS:circuit-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5AB093CF
P 6300 4000
F 0 "#FLG01" H 6300 4075 50  0001 C CNN
F 1 "PWR_FLAG" H 6300 4150 50  0000 C CNN
F 2 "" H 6300 4000 50  0001 C CNN
F 3 "" H 6300 4000 50  0001 C CNN
	1    6300 4000
	1    0    0    -1  
$EndComp
Text GLabel 10300 2400 2    39   Input ~ 0
pwm0
Text GLabel 10700 2300 2    39   Input ~ 0
pwm1
Wire Wire Line
	9500 2400 10300 2400
Wire Wire Line
	9500 2500 10650 2500
$Comp
L power:+5V #PWR06
U 1 1 5AB13434
P 6500 3550
F 0 "#PWR06" H 6500 3400 50  0001 C CNN
F 1 "+5V" H 6500 3690 50  0000 C CNN
F 2 "" H 6500 3550 50  0001 C CNN
F 3 "" H 6500 3550 50  0001 C CNN
	1    6500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3600 6500 3600
Wire Wire Line
	6500 3600 6500 3550
Text GLabel 4250 1400 2    39   Input ~ 0
+5
Text GLabel 6350 3600 0    60   Input ~ 0
+5
Connection ~ 6500 3600
$Comp
L power:GND #PWR05
U 1 1 5AB1463D
P 6250 3250
F 0 "#PWR05" H 6250 3000 50  0001 C CNN
F 1 "GND" H 6250 3100 50  0000 C CNN
F 2 "" H 6250 3250 50  0001 C CNN
F 3 "" H 6250 3250 50  0001 C CNN
	1    6250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3200 6250 3250
Wire Wire Line
	6650 3300 6900 3300
Wire Wire Line
	6650 3200 6650 3300
Wire Wire Line
	6650 3400 6900 3400
Connection ~ 6650 3300
Wire Wire Line
	6250 3200 6650 3200
Text GLabel 6550 2700 0    39   Input ~ 0
led0
Wire Wire Line
	9500 2300 10700 2300
Wire Wire Line
	9500 2200 10450 2200
Wire Wire Line
	6550 3700 6750 3700
$Comp
L Device:CP C2
U 1 1 5ACB797D
P 6750 4200
F 0 "C2" H 6775 4300 50  0000 L CNN
F 1 "CP" H 6775 4100 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P5.00mm" H 6788 4050 50  0001 C CNN
F 3 "" H 6750 4200 50  0001 C CNN
	1    6750 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3700 6750 3850
$Comp
L power:GND #PWR07
U 1 1 5ACB7984
P 6750 4500
F 0 "#PWR07" H 6750 4250 50  0001 C CNN
F 1 "GND" H 6750 4350 50  0000 C CNN
F 2 "" H 6750 4500 50  0001 C CNN
F 3 "" H 6750 4500 50  0001 C CNN
	1    6750 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4350 6750 4500
Connection ~ 6750 3850
Wire Wire Line
	9500 2100 9950 2100
Wire Wire Line
	9950 2000 9500 2000
Wire Wire Line
	9500 1900 9950 1900
Wire Wire Line
	9950 1800 9500 1800
Wire Wire Line
	9500 1700 9950 1700
Wire Wire Line
	9950 1600 9500 1600
$Comp
L conn:Conn_01x06 J1
U 1 1 5ACEE0FD
P 10150 1800
F 0 "J1" H 10150 2100 50  0000 C CNN
F 1 "Sensors" H 10150 2200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 10150 1800 50  0001 C CNN
F 3 "" H 10150 1800 50  0001 C CNN
	1    10150 1800
	1    0    0    -1  
$EndComp
Text GLabel 6550 2500 0    39   Input ~ 0
led1
Text GLabel 10650 2500 2    60   Input ~ 0
mosi
Wire Wire Line
	9500 2600 9800 2600
Wire Wire Line
	9500 2700 10250 2700
Text GLabel 9800 2600 2    39   Input ~ 0
miso
Text GLabel 10250 2700 2    39   Input ~ 0
sck
Text GLabel 10450 2200 2    39   Input ~ 0
cs
$Comp
L conn:Conn_01x06 J4
U 1 1 5B438DD9
P 3900 1300
F 0 "J4" H 3900 1600 50  0000 C CNN
F 1 "SPI_SD" H 3900 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 3900 1300 50  0001 C CNN
F 3 "" H 3900 1300 50  0001 C CNN
	1    3900 1300
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5B439427
P 4250 1550
F 0 "#PWR04" H 4250 1300 50  0001 C CNN
F 1 "GND" H 4250 1400 50  0000 C CNN
F 2 "" H 4250 1550 50  0001 C CNN
F 3 "" H 4250 1550 50  0001 C CNN
	1    4250 1550
	1    0    0    -1  
$EndComp
Text GLabel 4250 1200 2    39   Input ~ 0
mosi
Text GLabel 4250 1300 2    39   Input ~ 0
miso
Text GLabel 5150 1100 2    39   Input ~ 0
sck
Text GLabel 4800 1000 2    39   Input ~ 0
cs
Wire Wire Line
	4100 1000 4450 1000
Wire Wire Line
	4250 1500 4250 1550
$Comp
L switches:SW_Push Start1
U 1 1 5B51726D
P 7200 5650
F 0 "Start1" H 7250 5750 50  0000 L CNN
F 1 "SW_Push" H 7200 5590 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7200 5850 50  0001 C CNN
F 3 "" H 7200 5850 50  0001 C CNN
	1    7200 5650
	1    0    0    -1  
$EndComp
Text GLabel 6750 2400 0    39   Input ~ 0
Start
Wire Wire Line
	6750 2400 6900 2400
Text GLabel 6850 5650 0    39   Input ~ 0
Start
Wire Wire Line
	6850 5650 7000 5650
$Comp
L conn:Conn_01x03 J2
U 1 1 5BE4F239
P 2700 2350
F 0 "J2" H 2700 2550 50  0000 C CNN
F 1 "Control0" H 2700 2150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2700 2350 50  0001 C CNN
F 3 "" H 2700 2350 50  0001 C CNN
	1    2700 2350
	-1   0    0    1   
$EndComp
Text GLabel 6550 2800 0    39   Input ~ 0
T0
Text GLabel 6550 2600 0    39   Input ~ 0
T1
Wire Wire Line
	6550 2800 6900 2800
Text GLabel 3100 2250 2    39   Input ~ 0
T0
Text GLabel 3100 2350 2    39   Input ~ 0
led0
Text GLabel 3100 2450 2    39   Input ~ 0
pwm0
Wire Wire Line
	2900 2250 3100 2250
Wire Wire Line
	2900 2350 3100 2350
Wire Wire Line
	2900 2450 3100 2450
$Comp
L conn:Conn_01x03 J3
U 1 1 5BE50329
P 2700 3200
F 0 "J3" H 2700 3400 50  0000 C CNN
F 1 "Alim0" H 2700 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2700 3200 50  0001 C CNN
F 3 "" H 2700 3200 50  0001 C CNN
	1    2700 3200
	-1   0    0    1   
$EndComp
Text GLabel 6100 3700 0    39   Input ~ 0
Vin
Connection ~ 6750 3700
Text GLabel 6800 2300 0    39   Input ~ 0
ledW
Wire Wire Line
	6800 2300 6900 2300
Text GLabel 3050 3100 2    39   Input ~ 0
Vin
Text GLabel 3250 3200 2    60   Input ~ 0
+5
$Comp
L power:GND #PWR01
U 1 1 5BE51437
P 3100 3350
F 0 "#PWR01" H 3100 3100 50  0001 C CNN
F 1 "GND" H 3100 3200 50  0000 C CNN
F 2 "" H 3100 3350 50  0001 C CNN
F 3 "" H 3100 3350 50  0001 C CNN
	1    3100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3200 3250 3200
Wire Wire Line
	2900 3300 3100 3300
Wire Wire Line
	3100 3300 3100 3350
Wire Wire Line
	6300 4000 6550 4000
Wire Wire Line
	6550 4000 6550 3850
Wire Wire Line
	6550 3850 6750 3850
$Comp
L conn:Conn_01x03 J5
U 1 1 5BE51D34
P 4200 2350
F 0 "J5" H 4200 2550 50  0000 C CNN
F 1 "Control1" H 4200 2150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 4200 2350 50  0001 C CNN
F 3 "" H 4200 2350 50  0001 C CNN
	1    4200 2350
	-1   0    0    1   
$EndComp
Text GLabel 4600 2250 2    39   Input ~ 0
T1
Text GLabel 4600 2350 2    39   Input ~ 0
led1
Text GLabel 4600 2450 2    39   Input ~ 0
pwm1
Wire Wire Line
	4400 2250 4600 2250
Wire Wire Line
	4400 2350 4600 2350
Wire Wire Line
	4400 2450 4600 2450
$Comp
L conn:Conn_01x03 J6
U 1 1 5BE51D40
P 4200 3200
F 0 "J6" H 4200 3400 50  0000 C CNN
F 1 "Alim1" H 4200 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 4200 3200 50  0001 C CNN
F 3 "" H 4200 3200 50  0001 C CNN
	1    4200 3200
	-1   0    0    1   
$EndComp
Text GLabel 4550 3100 2    39   Input ~ 0
Vin
Text GLabel 4750 3200 2    60   Input ~ 0
+5
$Comp
L power:GND #PWR03
U 1 1 5BE51D48
P 4600 3350
F 0 "#PWR03" H 4600 3100 50  0001 C CNN
F 1 "GND" H 4600 3200 50  0000 C CNN
F 2 "" H 4600 3350 50  0001 C CNN
F 3 "" H 4600 3350 50  0001 C CNN
	1    4600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3200 4750 3200
Wire Wire Line
	4400 3300 4600 3300
Wire Wire Line
	4600 3300 4600 3350
Wire Wire Line
	6550 2500 6900 2500
Wire Wire Line
	6900 2600 6550 2600
Wire Wire Line
	6550 2700 6900 2700
$Comp
L power:GND #PWR09
U 1 1 5BE52205
P 8300 5950
F 0 "#PWR09" H 8300 5700 50  0001 C CNN
F 1 "GND" H 8300 5800 50  0000 C CNN
F 2 "" H 8300 5950 50  0001 C CNN
F 3 "" H 8300 5950 50  0001 C CNN
	1    8300 5950
	1    0    0    -1  
$EndComp
Text GLabel 9950 5250 2    60   Input ~ 0
ledW
Wire Wire Line
	9750 5250 9950 5250
$Comp
L Device:LED D1
U 1 1 5BE52303
P 9200 5250
F 0 "D1" H 9200 5350 50  0000 C CNN
F 1 "LED" H 9200 5150 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 9200 5250 50  0001 C CNN
F 3 "" H 9200 5250 50  0001 C CNN
	1    9200 5250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BE52344
P 9600 5250
F 0 "R1" V 9680 5250 50  0000 C CNN
F 1 "R" V 9600 5250 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 9530 5250 50  0001 C CNN
F 3 "" H 9600 5250 50  0001 C CNN
	1    9600 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 5250 9450 5250
$Comp
L Device:R jump1
U 1 1 5BE5265C
P 4600 1000
F 0 "jump1" V 4680 1000 50  0000 C CNN
F 1 "0R" V 4600 1000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4530 1000 50  0001 C CNN
F 3 "" H 4600 1000 50  0001 C CNN
	1    4600 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 1000 4750 1000
Text GLabel 6700 1600 0    39   Input ~ 0
Rx
Text GLabel 6700 1700 0    39   Input ~ 0
Tx
Wire Wire Line
	6700 1600 6900 1600
Wire Wire Line
	6700 1700 6900 1700
$Comp
L power:GND #PWR02
U 1 1 5BEB940F
P 3350 1300
F 0 "#PWR02" H 3350 1050 50  0001 C CNN
F 1 "GND" H 3350 1150 50  0000 C CNN
F 2 "" H 3350 1300 50  0001 C CNN
F 3 "" H 3350 1300 50  0001 C CNN
	1    3350 1300
	1    0    0    -1  
$EndComp
Text GLabel 3050 750  2    39   Input ~ 0
Rx
Text GLabel 3050 850  2    39   Input ~ 0
Tx
Wire Wire Line
	2800 1150 3050 1150
Wire Wire Line
	2800 1250 3350 1250
Wire Wire Line
	3350 1250 3350 1300
Wire Wire Line
	2800 750  3050 750 
Wire Wire Line
	2800 850  3050 850 
Text GLabel 6750 3500 0    39   Input ~ 0
3v3
Wire Wire Line
	6750 3500 6900 3500
Text GLabel 3050 1150 2    39   Input ~ 0
+5
Wire Wire Line
	6500 3600 6900 3600
Wire Wire Line
	6650 3300 6650 3400
Wire Wire Line
	6750 3850 6750 4050
Wire Wire Line
	6750 3700 6900 3700
$Comp
L conn:Conn_01x02 J7
U 1 1 5C00C98F
P 2600 850
F 0 "J7" H 2520 525 50  0000 C CNN
F 1 "Serial" H 2520 616 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2600 850 50  0001 C CNN
F 3 "~" H 2600 850 50  0001 C CNN
	1    2600 850 
	-1   0    0    1   
$EndComp
$Comp
L conn:Conn_01x02 J8
U 1 1 5C00EC48
P 2600 1250
F 0 "J8" H 2520 925 50  0000 C CNN
F 1 "5Vgnd" H 2520 1016 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 2600 1250 50  0001 C CNN
F 3 "~" H 2600 1250 50  0001 C CNN
	1    2600 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 1500 4250 1500
Wire Wire Line
	4100 1400 4250 1400
Wire Wire Line
	4250 1200 4100 1200
Wire Wire Line
	4100 1300 4250 1300
$Comp
L arduino:Arduino_Uno_Shield XA1
U 1 1 5C0A9769
P 8200 2650
F 0 "XA1" H 8200 4037 60  0000 C CNN
F 1 "Arduino_Uno_Shield" H 8200 3931 60  0000 C CNN
F 2 "Modules:Arduino_UNO_R3" H 10000 6400 60  0001 C CNN
F 3 "https://store.arduino.cc/arduino-uno-rev3" H 10000 6400 60  0001 C CNN
	1    8200 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 5650 8050 5350
Wire Wire Line
	8050 5350 8300 5350
Wire Wire Line
	8300 5350 8300 5550
Wire Wire Line
	7400 5650 8050 5650
Wire Wire Line
	8300 5350 8300 5250
Wire Wire Line
	8300 5250 9050 5250
Connection ~ 8300 5350
Wire Wire Line
	8300 5850 8300 5900
Wire Wire Line
	8300 5350 8700 5350
Wire Wire Line
	8700 5350 8700 5900
Wire Wire Line
	8700 5900 8300 5900
Connection ~ 8300 5900
Wire Wire Line
	8300 5900 8300 5950
$Comp
L Device:R R2
U 1 1 5C0B3612
P 4900 1100
F 0 "R2" V 4980 1100 50  0000 C CNN
F 1 "0R" V 4900 1100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4830 1100 50  0001 C CNN
F 3 "" H 4900 1100 50  0001 C CNN
	1    4900 1100
	0    1    1    0   
$EndComp
Wire Wire Line
	5150 1100 5050 1100
Wire Wire Line
	4100 1100 4750 1100
$Comp
L Device:R R3
U 1 1 5C0EE240
P 6400 3700
F 0 "R3" V 6480 3700 50  0000 C CNN
F 1 "0R" V 6400 3700 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6330 3700 50  0001 C CNN
F 3 "" H 6400 3700 50  0001 C CNN
	1    6400 3700
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 3700 6250 3700
Wire Wire Line
	2900 3100 3050 3100
Wire Wire Line
	4400 3100 4550 3100
$EndSCHEMATC
