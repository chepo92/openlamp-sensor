EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:arduino
LIBS:circuit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PWR_FLAG #FLG1
U 1 1 5AB093CF
P 6300 4000
F 0 "#FLG1" H 6300 4075 50  0001 C CNN
F 1 "PWR_FLAG" H 6300 4150 50  0000 C CNN
F 2 "" H 6300 4000 50  0001 C CNN
F 3 "" H 6300 4000 50  0001 C CNN
	1    6300 4000
	1    0    0    -1  
$EndComp
$Comp
L Arduino_Uno_Shield XA1
U 1 1 5AB12800
P 8200 2650
F 0 "XA1" V 8300 2650 60  0000 C CNN
F 1 "Arduino_Uno_Shield" V 8100 2650 60  0000 C CNN
F 2 "arduino:Arduino_Uno_Shield" H 10000 6400 60  0001 C CNN
F 3 "" H 10000 6400 60  0001 C CNN
	1    8200 2650
	1    0    0    -1  
$EndComp
Text GLabel 10300 2400 2    39   Input ~ 0
pwm0
Text GLabel 10700 2300 2    39   Input ~ 0
pwm1
Wire Wire Line
	9500 2400 10300 2400
Wire Wire Line
	9500 2500 10650 2500
$Comp
L +5V #PWR6
U 1 1 5AB13434
P 6500 3550
F 0 "#PWR6" H 6500 3400 50  0001 C CNN
F 1 "+5V" H 6500 3690 50  0000 C CNN
F 2 "" H 6500 3550 50  0001 C CNN
F 3 "" H 6500 3550 50  0001 C CNN
	1    6500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 3600 6900 3600
Wire Wire Line
	6500 3600 6500 3550
Text GLabel 4400 1500 2    39   Input ~ 0
+5
Text GLabel 6350 3600 0    60   Input ~ 0
+5
Connection ~ 6500 3600
$Comp
L GND #PWR5
U 1 1 5AB1463D
P 6250 3250
F 0 "#PWR5" H 6250 3000 50  0001 C CNN
F 1 "GND" H 6250 3100 50  0000 C CNN
F 2 "" H 6250 3250 50  0001 C CNN
F 3 "" H 6250 3250 50  0001 C CNN
	1    6250 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3200 6250 3250
Wire Wire Line
	6650 3300 6900 3300
Wire Wire Line
	6650 3200 6650 3400
Wire Wire Line
	6650 3400 6900 3400
Connection ~ 6650 3300
Wire Wire Line
	6250 3200 6900 3200
Text GLabel 6550 2700 0    39   Input ~ 0
led0
Wire Wire Line
	9500 2300 10700 2300
Wire Wire Line
	9500 2200 10450 2200
Wire Wire Line
	6550 3700 6900 3700
$Comp
L CP C2
U 1 1 5ACB797D
P 6750 4200
F 0 "C2" H 6775 4300 50  0000 L CNN
F 1 "CP" H 6775 4100 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P5.00mm" H 6788 4050 50  0001 C CNN
F 3 "" H 6750 4200 50  0001 C CNN
	1    6750 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3700 6750 4050
$Comp
L GND #PWR7
U 1 1 5ACB7984
P 6750 4500
F 0 "#PWR7" H 6750 4250 50  0001 C CNN
F 1 "GND" H 6750 4350 50  0000 C CNN
F 2 "" H 6750 4500 50  0001 C CNN
F 3 "" H 6750 4500 50  0001 C CNN
	1    6750 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4350 6750 4500
Connection ~ 6750 3850
Wire Wire Line
	9500 2100 9950 2100
Wire Wire Line
	9950 2000 9500 2000
Wire Wire Line
	9500 1900 9950 1900
Wire Wire Line
	9950 1800 9500 1800
Wire Wire Line
	9500 1700 9950 1700
Wire Wire Line
	9950 1600 9500 1600
$Comp
L Conn_01x06 J1
U 1 1 5ACEE0FD
P 10150 1800
F 0 "J1" H 10150 2100 50  0000 C CNN
F 1 "Conn_01x06" H 10150 1400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 10150 1800 50  0001 C CNN
F 3 "" H 10150 1800 50  0001 C CNN
	1    10150 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 1350 9650 1350
Connection ~ 6650 3200
Text GLabel 6550 2500 0    39   Input ~ 0
led1
Text GLabel 10650 2500 2    60   Input ~ 0
mosi
Wire Wire Line
	9500 2600 9800 2600
Wire Wire Line
	9500 2700 10250 2700
Text GLabel 9800 2600 2    39   Input ~ 0
miso
Text GLabel 10250 2700 2    39   Input ~ 0
sck
Text GLabel 10450 2200 2    39   Input ~ 0
cs
$Comp
L Conn_01x06 J4
U 1 1 5B438DD9
P 3900 1300
F 0 "J4" H 3900 1600 50  0000 C CNN
F 1 "Conn_01x06" H 3900 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 3900 1300 50  0001 C CNN
F 3 "" H 3900 1300 50  0001 C CNN
	1    3900 1300
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR4
U 1 1 5B439427
P 4750 1450
F 0 "#PWR4" H 4750 1200 50  0001 C CNN
F 1 "GND" H 4750 1300 50  0000 C CNN
F 2 "" H 4750 1450 50  0001 C CNN
F 3 "" H 4750 1450 50  0001 C CNN
	1    4750 1450
	1    0    0    -1  
$EndComp
Text GLabel 4350 1100 2    39   Input ~ 0
mosi
Text GLabel 4300 1200 2    39   Input ~ 0
miso
Text GLabel 4300 1300 2    39   Input ~ 0
sck
Text GLabel 4800 1000 2    39   Input ~ 0
cs
Wire Wire Line
	4100 1000 4450 1000
Wire Wire Line
	4750 1400 4750 1450
Wire Wire Line
	4100 1200 4300 1200
Wire Wire Line
	4300 1300 4100 1300
Wire Wire Line
	4100 1400 4750 1400
Wire Wire Line
	4100 1500 4400 1500
Wire Wire Line
	4350 1100 4100 1100
$Comp
L SW_Push Start1
U 1 1 5B51726D
P 7200 5650
F 0 "Start1" H 7250 5750 50  0000 L CNN
F 1 "SW_Push" H 7200 5590 50  0000 C CNN
F 2 "Buttons_Switches_THT:SW_PUSH_6mm" H 7200 5850 50  0001 C CNN
F 3 "" H 7200 5850 50  0001 C CNN
	1    7200 5650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR8
U 1 1 5B517F94
P 7600 5850
F 0 "#PWR8" H 7600 5600 50  0001 C CNN
F 1 "GND" H 7600 5700 50  0000 C CNN
F 2 "" H 7600 5850 50  0001 C CNN
F 3 "" H 7600 5850 50  0001 C CNN
	1    7600 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 5650 7600 5650
Wire Wire Line
	7600 5650 7600 5850
Text GLabel 6750 2400 0    39   Input ~ 0
Start
Wire Wire Line
	6750 2400 6900 2400
Text GLabel 6850 5650 0    39   Input ~ 0
Start
Wire Wire Line
	6850 5650 7000 5650
$Comp
L Conn_01x03 J2
U 1 1 5BE4F239
P 2700 2350
F 0 "J2" H 2700 2550 50  0000 C CNN
F 1 "Conn_01x03" H 2700 2150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2700 2350 50  0001 C CNN
F 3 "" H 2700 2350 50  0001 C CNN
	1    2700 2350
	-1   0    0    1   
$EndComp
Text GLabel 6550 2800 0    39   Input ~ 0
T0
Text GLabel 6550 2600 0    39   Input ~ 0
T1
Wire Wire Line
	6550 2800 6900 2800
Text GLabel 3100 2250 2    39   Input ~ 0
T0
Text GLabel 3100 2350 2    39   Input ~ 0
led0
Text GLabel 3100 2450 2    39   Input ~ 0
pwm0
Wire Wire Line
	2900 2250 3100 2250
Wire Wire Line
	2900 2350 3100 2350
Wire Wire Line
	2900 2450 3100 2450
$Comp
L Conn_01x03 J3
U 1 1 5BE50329
P 2700 3200
F 0 "J3" H 2700 3400 50  0000 C CNN
F 1 "Conn_01x03" H 2700 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 2700 3200 50  0001 C CNN
F 3 "" H 2700 3200 50  0001 C CNN
	1    2700 3200
	-1   0    0    1   
$EndComp
Text GLabel 6550 3700 0    39   Input ~ 0
Vin
Connection ~ 6750 3700
Text GLabel 6800 2300 0    39   Input ~ 0
ledW
Wire Wire Line
	6800 2300 6900 2300
Text GLabel 3050 3100 2    39   Input ~ 0
Vin
Text GLabel 3250 3200 2    60   Input ~ 0
+5
$Comp
L GND #PWR1
U 1 1 5BE51437
P 3100 3350
F 0 "#PWR1" H 3100 3100 50  0001 C CNN
F 1 "GND" H 3100 3200 50  0000 C CNN
F 2 "" H 3100 3350 50  0001 C CNN
F 3 "" H 3100 3350 50  0001 C CNN
	1    3100 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3100 3050 3100
Wire Wire Line
	2900 3200 3250 3200
Wire Wire Line
	2900 3300 3100 3300
Wire Wire Line
	3100 3300 3100 3350
Wire Wire Line
	6300 4000 6550 4000
Wire Wire Line
	6550 4000 6550 3850
Wire Wire Line
	6550 3850 6750 3850
$Comp
L Conn_01x03 J5
U 1 1 5BE51D34
P 4200 2350
F 0 "J5" H 4200 2550 50  0000 C CNN
F 1 "Conn_01x03" H 4200 2150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 4200 2350 50  0001 C CNN
F 3 "" H 4200 2350 50  0001 C CNN
	1    4200 2350
	-1   0    0    1   
$EndComp
Text GLabel 4600 2250 2    39   Input ~ 0
T1
Text GLabel 4600 2350 2    39   Input ~ 0
led1
Text GLabel 4600 2450 2    39   Input ~ 0
pwm1
Wire Wire Line
	4400 2250 4600 2250
Wire Wire Line
	4400 2350 4600 2350
Wire Wire Line
	4400 2450 4600 2450
$Comp
L Conn_01x03 J6
U 1 1 5BE51D40
P 4200 3200
F 0 "J6" H 4200 3400 50  0000 C CNN
F 1 "Conn_01x03" H 4200 3000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 4200 3200 50  0001 C CNN
F 3 "" H 4200 3200 50  0001 C CNN
	1    4200 3200
	-1   0    0    1   
$EndComp
Text GLabel 4550 3100 2    39   Input ~ 0
Vin
Text GLabel 4750 3200 2    60   Input ~ 0
+5
$Comp
L GND #PWR3
U 1 1 5BE51D48
P 4600 3350
F 0 "#PWR3" H 4600 3100 50  0001 C CNN
F 1 "GND" H 4600 3200 50  0000 C CNN
F 2 "" H 4600 3350 50  0001 C CNN
F 3 "" H 4600 3350 50  0001 C CNN
	1    4600 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3100 4550 3100
Wire Wire Line
	4400 3200 4750 3200
Wire Wire Line
	4400 3300 4600 3300
Wire Wire Line
	4600 3300 4600 3350
Wire Wire Line
	6550 2500 6900 2500
Wire Wire Line
	6900 2600 6550 2600
Wire Wire Line
	6550 2700 6900 2700
$Comp
L GND #PWR9
U 1 1 5BE52205
P 8800 5400
F 0 "#PWR9" H 8800 5150 50  0001 C CNN
F 1 "GND" H 8800 5250 50  0000 C CNN
F 2 "" H 8800 5400 50  0001 C CNN
F 3 "" H 8800 5400 50  0001 C CNN
	1    8800 5400
	1    0    0    -1  
$EndComp
Text GLabel 9950 5250 2    60   Input ~ 0
ledW
Wire Wire Line
	8800 5250 8800 5400
Wire Wire Line
	9750 5250 9950 5250
$Comp
L LED D1
U 1 1 5BE52303
P 9200 5250
F 0 "D1" H 9200 5350 50  0000 C CNN
F 1 "LED" H 9200 5150 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 9200 5250 50  0001 C CNN
F 3 "" H 9200 5250 50  0001 C CNN
	1    9200 5250
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5BE52344
P 9600 5250
F 0 "R1" V 9680 5250 50  0000 C CNN
F 1 "R" V 9600 5250 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9530 5250 50  0001 C CNN
F 3 "" H 9600 5250 50  0001 C CNN
	1    9600 5250
	0    1    1    0   
$EndComp
Wire Wire Line
	9350 5250 9450 5250
Wire Wire Line
	9050 5250 8800 5250
$Comp
L R R2
U 1 1 5BE5265C
P 4600 1000
F 0 "R2" V 4680 1000 50  0000 C CNN
F 1 "R" V 4600 1000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4530 1000 50  0001 C CNN
F 3 "" H 4600 1000 50  0001 C CNN
	1    4600 1000
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 1000 4750 1000
Text GLabel 6700 1600 0    39   Input ~ 0
Rx
Text GLabel 6700 1700 0    39   Input ~ 0
Tx
Wire Wire Line
	6700 1600 6900 1600
Wire Wire Line
	6700 1700 6900 1700
$Comp
L Conn_01x04 J7
U 1 1 5BEB918E
P 2600 1350
F 0 "J7" H 2600 1550 50  0000 C CNN
F 1 "Conn_01x04" H 2600 1050 50  0000 C CNN
F 2 "" H 2600 1350 50  0001 C CNN
F 3 "" H 2600 1350 50  0001 C CNN
	1    2600 1350
	-1   0    0    1   
$EndComp
Text GLabel 2000 1200 2    60   Input ~ 0
+5
$Comp
L GND #PWR2
U 1 1 5BEB940F
P 3350 1300
F 0 "#PWR2" H 3350 1050 50  0001 C CNN
F 1 "GND" H 3350 1150 50  0000 C CNN
F 2 "" H 3350 1300 50  0001 C CNN
F 3 "" H 3350 1300 50  0001 C CNN
	1    3350 1300
	1    0    0    -1  
$EndComp
Text GLabel 3050 1350 2    39   Input ~ 0
Rx
Text GLabel 3050 1450 2    39   Input ~ 0
Tx
Wire Wire Line
	2800 1150 3050 1150
Wire Wire Line
	2800 1250 3350 1250
Wire Wire Line
	3350 1250 3350 1300
Wire Wire Line
	2800 1350 3050 1350
Wire Wire Line
	2800 1450 3050 1450
$Comp
L Conn_01x03 J?
U 1 1 5BEB999C
P 1500 1300
F 0 "J?" H 1500 1500 50  0000 C CNN
F 1 "Conn_01x03" H 1500 1100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 1500 1300 50  0001 C CNN
F 3 "" H 1500 1300 50  0001 C CNN
	1    1500 1300
	-1   0    0    1   
$EndComp
Text GLabel 6750 3500 0    39   Input ~ 0
3v3
Wire Wire Line
	6750 3500 6900 3500
Wire Wire Line
	1700 1200 2000 1200
Text GLabel 2000 1400 2    39   Input ~ 0
3v3
Text GLabel 2000 1300 2    39   Input ~ 0
Vo
Wire Wire Line
	1700 1300 2000 1300
Wire Wire Line
	2000 1400 1700 1400
Text GLabel 3050 1150 2    39   Input ~ 0
Vo
$EndSCHEMATC
