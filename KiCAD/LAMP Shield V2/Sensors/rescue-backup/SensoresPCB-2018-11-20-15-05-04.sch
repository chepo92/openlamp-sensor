EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:SensoresPCB-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x04 J3
U 1 1 5BE49349
P 5250 1000
F 0 "J3" H 5250 1200 50  0000 C CNN
F 1 "Conn_01x04" H 5250 700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 5250 1000 50  0001 C CNN
F 3 "" H 5250 1000 50  0001 C CNN
	1    5250 1000
	1    0    0    1   
$EndComp
$Comp
L Conn_01x04 J7
U 1 1 5BE49501
P 6350 1000
F 0 "J7" H 6350 1200 50  0000 C CNN
F 1 "Conn_01x04" H 6350 700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 6350 1000 50  0001 C CNN
F 3 "" H 6350 1000 50  0001 C CNN
	1    6350 1000
	1    0    0    1   
$EndComp
$Comp
L Conn_01x02 J4
U 1 1 5BE4967B
P 5250 1750
F 0 "J4" H 5250 1850 50  0000 C CNN
F 1 "Conn_01x02" H 5250 1550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5250 1750 50  0001 C CNN
F 3 "" H 5250 1750 50  0001 C CNN
	1    5250 1750
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J5
U 1 1 5BE49721
P 5250 2200
F 0 "J5" H 5250 2300 50  0000 C CNN
F 1 "Conn_01x02" H 5250 2000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5250 2200 50  0001 C CNN
F 3 "" H 5250 2200 50  0001 C CNN
	1    5250 2200
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x02 J6
U 1 1 5BE498DC
P 5250 2750
F 0 "J6" H 5250 2850 50  0000 C CNN
F 1 "Conn_01x02" H 5250 2550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5250 2750 50  0001 C CNN
F 3 "" H 5250 2750 50  0001 C CNN
	1    5250 2750
	1    0    0    -1  
$EndComp
$Comp
L Jack-DC J1
U 1 1 5BE49FC7
P 2550 1150
F 0 "J1" H 2550 1360 50  0000 C CNN
F 1 "Jack-DC" H 2550 975 50  0000 C CNN
F 2 "Connectors:JACK_ALIM" H 2600 1110 50  0001 C CNN
F 3 "" H 2600 1110 50  0001 C CNN
	1    2550 1150
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR01
U 1 1 5BE4A25D
P 5900 1250
F 0 "#PWR01" H 5900 1000 50  0001 C CNN
F 1 "GND" H 5900 1100 50  0000 C CNN
F 2 "" H 5900 1250 50  0001 C CNN
F 3 "" H 5900 1250 50  0001 C CNN
	1    5900 1250
	1    0    0    -1  
$EndComp
Text GLabel 4550 800  0    60   Input ~ 0
5v
$Comp
L Conn_01x03 J8
U 1 1 5BE4A475
P 8300 900
F 0 "J8" H 8300 1100 50  0000 C CNN
F 1 "Conn_01x03" H 8300 700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 8300 900 50  0001 C CNN
F 3 "" H 8300 900 50  0001 C CNN
	1    8300 900 
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR02
U 1 1 5BE4A827
P 3000 1450
F 0 "#PWR02" H 3000 1200 50  0001 C CNN
F 1 "GND" H 3000 1300 50  0000 C CNN
F 2 "" H 3000 1450 50  0001 C CNN
F 3 "" H 3000 1450 50  0001 C CNN
	1    3000 1450
	1    0    0    -1  
$EndComp
$Comp
L IRF540N Q1
U 1 1 5BE4A98D
P 5450 3950
F 0 "Q1" H 5700 4025 50  0000 L CNN
F 1 "IRF540N" H 5700 3950 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 5700 3875 50  0001 L CIN
F 3 "" H 5450 3950 50  0001 L CNN
	1    5450 3950
	1    0    0    -1  
$EndComp
Text GLabel 5350 3400 0    60   Input ~ 0
12v
$Comp
L R R1
U 1 1 5BE4AB8C
P 4900 3950
F 0 "R1" V 4980 3950 50  0000 C CNN
F 1 "R" V 4900 3950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4830 3950 50  0001 C CNN
F 3 "" H 4900 3950 50  0001 C CNN
	1    4900 3950
	0    1    1    0   
$EndComp
$Comp
L Conn_01x03 J2
U 1 1 5BE4ACE6
P 8300 3100
F 0 "J2" H 8300 3300 50  0000 C CNN
F 1 "Conn_01x03" H 8300 2900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 8300 3100 50  0001 C CNN
F 3 "" H 8300 3100 50  0001 C CNN
	1    8300 3100
	-1   0    0    1   
$EndComp
Text GLabel 8950 3100 2    60   Input ~ 0
5v
Text GLabel 3250 1050 2    60   Input ~ 0
12v
Text GLabel 4600 3950 0    60   Input ~ 0
pwm0
$Comp
L GND #PWR03
U 1 1 5BE4B11D
P 5550 4350
F 0 "#PWR03" H 5550 4100 50  0001 C CNN
F 1 "GND" H 5550 4200 50  0000 C CNN
F 2 "" H 5550 4350 50  0001 C CNN
F 3 "" H 5550 4350 50  0001 C CNN
	1    5550 4350
	1    0    0    -1  
$EndComp
Text GLabel 8650 3000 2    60   Input ~ 0
12v
$Comp
L GND #PWR04
U 1 1 5BE4B344
P 8700 3300
F 0 "#PWR04" H 8700 3050 50  0001 C CNN
F 1 "GND" H 8700 3150 50  0000 C CNN
F 2 "" H 8700 3300 50  0001 C CNN
F 3 "" H 8700 3300 50  0001 C CNN
	1    8700 3300
	1    0    0    -1  
$EndComp
Text GLabel 4550 900  0    39   Input ~ 0
out
Text GLabel 4550 1000 0    39   Input ~ 0
S2
Text GLabel 4550 1100 0    39   Input ~ 0
S3
Text GLabel 5900 1000 0    39   Input ~ 0
OE
Text GLabel 5900 900  0    39   Input ~ 0
S1
Text GLabel 5900 800  0    39   Input ~ 0
S0
Text GLabel 4850 1750 0    39   Input ~ 0
T0a
Text GLabel 4850 2200 0    39   Input ~ 0
Ha
Text GLabel 4850 2300 0    39   Input ~ 0
Hb
Text GLabel 4900 2750 0    39   Input ~ 0
UvLedA
Text GLabel 4900 2850 0    39   Input ~ 0
UvLedB
Text GLabel 4850 1850 0    39   Input ~ 0
T0b
Text GLabel 9150 800  2    39   Input ~ 0
out
Text GLabel 9150 900  2    39   Input ~ 0
S2
Text GLabel 9150 1000 2    39   Input ~ 0
S3
Text GLabel 5750 2200 0    39   Input ~ 0
OE
Text GLabel 5750 2400 0    39   Input ~ 0
S1
Text GLabel 6500 2300 2    39   Input ~ 0
S0
Text GLabel 10400 1850 2    39   Input ~ 0
T0a
Text GLabel 10400 1950 2    39   Input ~ 0
T0b
Text GLabel 6000 3450 2    39   Input ~ 0
Ha
Text GLabel 6000 3550 2    39   Input ~ 0
Hb
Text GLabel 9600 2600 2    39   Input ~ 0
UvLedA
Text GLabel 9600 2700 2    39   Input ~ 0
UvLedB
$Comp
L GND #PWR05
U 1 1 5BE4C685
P 10150 2200
F 0 "#PWR05" H 10150 1950 50  0001 C CNN
F 1 "GND" H 10150 2050 50  0000 C CNN
F 2 "" H 10150 2200 50  0001 C CNN
F 3 "" H 10150 2200 50  0001 C CNN
	1    10150 2200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5BE4C6B5
P 9350 2850
F 0 "#PWR06" H 9350 2600 50  0001 C CNN
F 1 "GND" H 9350 2700 50  0000 C CNN
F 2 "" H 9350 2850 50  0001 C CNN
F 3 "" H 9350 2850 50  0001 C CNN
	1    9350 2850
	1    0    0    -1  
$EndComp
Text GLabel 8850 2150 2    60   Input ~ 0
pwm0
Text GLabel 6100 1900 0    60   Input ~ 0
5v
Text GLabel 9950 1300 0    60   Input ~ 0
5v
$Comp
L R R3
U 1 1 5BE4D45D
P 10200 1550
F 0 "R3" V 10280 1550 50  0000 C CNN
F 1 "R" V 10200 1550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10130 1550 50  0001 C CNN
F 3 "" H 10200 1550 50  0001 C CNN
	1    10200 1550
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5BE4D7B2
P 9350 2300
F 0 "R2" V 9430 2300 50  0000 C CNN
F 1 "R" V 9350 2300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9280 2300 50  0001 C CNN
F 3 "" H 9350 2300 50  0001 C CNN
	1    9350 2300
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J10
U 1 1 5BE4D8BD
P 8300 1950
F 0 "J10" H 8300 2150 50  0000 C CNN
F 1 "Conn_01x03" H 8300 1750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 8300 1950 50  0001 C CNN
F 3 "" H 8300 1950 50  0001 C CNN
	1    8300 1950
	-1   0    0    1   
$EndComp
$Comp
L Conn_02x03_Top_Bottom J9
U 1 1 5BE4BFC8
P 6050 2300
F 0 "J9" H 6100 2500 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 6100 2100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 6050 2300 50  0001 C CNN
F 3 "" H 6050 2300 50  0001 C CNN
	1    6050 2300
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5BE4C348
P 7100 3500
F 0 "#PWR07" H 7100 3250 50  0001 C CNN
F 1 "GND" H 7100 3350 50  0000 C CNN
F 2 "" H 7100 3500 50  0001 C CNN
F 3 "" H 7100 3500 50  0001 C CNN
	1    7100 3500
	1    0    0    -1  
$EndComp
Text GLabel 7200 3050 2    39   Input ~ 0
OE
Text GLabel 6400 3150 0    39   Input ~ 0
S1
Text GLabel 6400 2950 0    39   Input ~ 0
S0
$Comp
L Conn_02x03_Top_Bottom J11
U 1 1 5BE4EE47
P 6700 3050
F 0 "J11" H 6750 3250 50  0000 C CNN
F 1 "Conn_02x03_Top_Bottom" H 6750 2850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 6700 3050 50  0001 C CNN
F 3 "" H 6700 3050 50  0001 C CNN
	1    6700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 800  5050 800 
Wire Wire Line
	4550 900  5050 900 
Wire Wire Line
	4550 1000 5050 1000
Wire Wire Line
	4550 1100 5050 1100
Wire Wire Line
	5900 800  6150 800 
Wire Wire Line
	5900 900  6150 900 
Wire Wire Line
	5900 1000 6150 1000
Wire Wire Line
	5900 1250 5900 1100
Wire Wire Line
	2850 1050 3250 1050
Wire Wire Line
	2850 1150 3000 1150
Wire Wire Line
	3000 1150 3000 1450
Wire Wire Line
	2850 1250 3000 1250
Connection ~ 3000 1250
Wire Wire Line
	5350 3400 5800 3400
Wire Wire Line
	5050 3950 5250 3950
Wire Wire Line
	4600 3950 4750 3950
Wire Wire Line
	5550 4150 5550 4350
Wire Wire Line
	8500 3000 8650 3000
Wire Wire Line
	8500 3100 8950 3100
Wire Wire Line
	8500 3200 8700 3200
Wire Wire Line
	8700 3200 8700 3300
Wire Wire Line
	5900 1100 6150 1100
Wire Wire Line
	4850 1750 5050 1750
Wire Wire Line
	4850 1850 5050 1850
Wire Wire Line
	4850 2200 5050 2200
Wire Wire Line
	4850 2300 5050 2300
Wire Wire Line
	4900 2750 5050 2750
Wire Wire Line
	4900 2850 5050 2850
Wire Wire Line
	8500 800  9150 800 
Wire Wire Line
	8500 900  9150 900 
Wire Wire Line
	8500 1000 9150 1000
Wire Wire Line
	10150 1950 10400 1950
Wire Wire Line
	6000 3450 5800 3450
Wire Wire Line
	5550 3550 6000 3550
Wire Wire Line
	9350 2600 9600 2600
Wire Wire Line
	9350 2700 9600 2700
Wire Wire Line
	5800 3450 5800 3400
Wire Wire Line
	5550 3550 5550 3750
Wire Wire Line
	10150 1950 10150 2200
Wire Wire Line
	9350 2850 9350 2700
Wire Wire Line
	6100 1900 6250 1900
Wire Wire Line
	9950 1300 10200 1300
Wire Wire Line
	10200 1300 10200 1400
Wire Wire Line
	10200 1850 10200 1700
Connection ~ 10200 1850
Wire Wire Line
	8500 1850 10400 1850
Wire Wire Line
	9350 2450 9350 2600
Wire Wire Line
	8500 1950 9350 1950
Wire Wire Line
	9350 1950 9350 2150
Wire Wire Line
	8500 2050 8750 2050
Wire Wire Line
	8750 2050 8750 2150
Wire Wire Line
	8750 2150 8850 2150
Wire Wire Line
	7100 2950 7100 3500
Connection ~ 7100 3400
Wire Wire Line
	5750 2200 5850 2200
Wire Wire Line
	5750 2400 5850 2400
Wire Wire Line
	6350 2300 6500 2300
Wire Wire Line
	6350 2400 6450 2400
Wire Wire Line
	6450 2400 6450 2200
Wire Wire Line
	6450 2200 6350 2200
Wire Wire Line
	5850 2300 5800 2300
Wire Wire Line
	5800 2300 5800 2050
Wire Wire Line
	5800 2050 6400 2050
Wire Wire Line
	6250 1900 6250 2050
Wire Wire Line
	6400 2050 6400 2200
Connection ~ 6400 2200
Connection ~ 6250 2050
Wire Wire Line
	6400 3150 6500 3150
Wire Wire Line
	7000 3150 7100 3150
Wire Wire Line
	7100 2950 7000 2950
Wire Wire Line
	6500 3050 6450 3050
Wire Wire Line
	6450 3050 6450 2800
Wire Wire Line
	6450 2800 7050 2800
Wire Wire Line
	7050 2800 7050 2950
Connection ~ 7050 2950
Connection ~ 6900 2800
Connection ~ 7100 3150
Wire Wire Line
	6400 2950 6500 2950
Wire Wire Line
	7000 3050 7200 3050
$EndSCHEMATC
