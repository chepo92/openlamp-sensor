EESchema Schematic File Version 4
LIBS:SensoresPCB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L conn:Conn_01x05 J3
U 1 1 5BE49349
P 3350 850
F 0 "J3" H 3350 1050 50  0000 C CNN
F 1 "Conn_01x05" H 3350 550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 3350 850 50  0001 C CNN
F 3 "" H 3350 850 50  0001 C CNN
	1    3350 850 
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Therm0
U 1 1 5BE4967B
P 3350 1500
F 0 "Therm0" H 3350 1600 50  0000 C CNN
F 1 "Conn_01x02" H 3350 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3350 1500 50  0001 C CNN
F 3 "" H 3350 1500 50  0001 C CNN
	1    3350 1500
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Heater0
U 1 1 5BE49721
P 3350 1950
F 0 "Heater0" H 3350 2050 50  0000 C CNN
F 1 "Conn_01x02" H 3350 1750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3350 1950 50  0001 C CNN
F 3 "" H 3350 1950 50  0001 C CNN
	1    3350 1950
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Led0
U 1 1 5BE498DC
P 3350 2400
F 0 "Led0" H 3350 2500 50  0000 C CNN
F 1 "Conn_01x02" H 3350 2200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 3350 2400 50  0001 C CNN
F 3 "" H 3350 2400 50  0001 C CNN
	1    3350 2400
	1    0    0    -1  
$EndComp
$Comp
L transistors:IRF540N Q1
U 1 1 5BE4A98D
P 7950 5650
F 0 "Q1" H 8200 5725 50  0000 L CNN
F 1 "IRF540N" H 8200 5650 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 8200 5575 50  0001 L CIN
F 3 "" H 7950 5650 50  0001 L CNN
	1    7950 5650
	1    0    0    -1  
$EndComp
Text GLabel 7850 5150 0    60   Input ~ 0
12v
$Comp
L Device:R R1
U 1 1 5BE4AB8C
P 7400 5650
F 0 "R1" V 7480 5650 50  0000 C CNN
F 1 "10" V 7400 5650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7330 5650 50  0001 C CNN
F 3 "" H 7400 5650 50  0001 C CNN
	1    7400 5650
	0    1    1    0   
$EndComp
Text GLabel 1350 900  2    39   Input ~ 0
5vCap
Text GLabel 7100 5650 0    39   Input ~ 0
pinPWM0
$Comp
L power:GND #PWR03
U 1 1 5BE4B11D
P 8050 6050
F 0 "#PWR03" H 8050 5800 50  0001 C CNN
F 1 "GND" H 8050 5900 50  0000 C CNN
F 2 "" H 8050 6050 50  0001 C CNN
F 3 "" H 8050 6050 50  0001 C CNN
	1    8050 6050
	1    0    0    -1  
$EndComp
Text GLabel 1100 1400 2    39   Input ~ 0
12v
Text GLabel 2900 950  0    39   Input ~ 0
sda0
Text GLabel 2900 1050 0    39   Input ~ 0
scl0
Text GLabel 2950 1500 0    39   Input ~ 0
T0a
Text GLabel 2950 1950 0    39   Input ~ 0
H0a
Text GLabel 2950 2050 0    39   Input ~ 0
H0b
Text GLabel 3000 2400 0    39   Input ~ 0
UvLed0a
Text GLabel 3000 2500 0    39   Input ~ 0
UvLed0b
Text GLabel 2950 1600 0    39   Input ~ 0
T0b
Text GLabel 8150 4650 2    39   Input ~ 0
T0a
Text GLabel 8100 5150 2    39   Input ~ 0
H0a
Text GLabel 8100 5250 2    39   Input ~ 0
H0b
Text GLabel 8200 4850 2    39   Input ~ 0
UvLed0a
Text GLabel 7700 4100 0    60   Input ~ 0
5va
$Comp
L Device:R R3
U 1 1 5BE4D45D
P 7950 4350
F 0 "R3" V 8030 4350 50  0000 C CNN
F 1 "100k" V 7950 4350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7880 4350 50  0001 C CNN
F 3 "" H 7950 4350 50  0001 C CNN
	1    7950 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BE4D7B2
P 7950 4850
F 0 "R2" V 8030 4850 50  0000 C CNN
F 1 "560" V 7950 4850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7880 4850 50  0001 C CNN
F 3 "" H 7950 4850 50  0001 C CNN
	1    7950 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	7550 5650 7650 5650
Wire Wire Line
	7100 5650 7250 5650
Wire Wire Line
	8050 5850 8050 5900
Wire Wire Line
	950  1400 1100 1400
Wire Wire Line
	1150 900  1350 900 
Wire Wire Line
	2950 1500 3150 1500
Wire Wire Line
	2950 1600 3150 1600
Wire Wire Line
	2950 1950 3150 1950
Wire Wire Line
	2950 2050 3150 2050
Wire Wire Line
	3000 2400 3150 2400
Wire Wire Line
	3000 2500 3150 2500
Wire Wire Line
	8050 5250 8100 5250
Wire Wire Line
	8050 5250 8050 5450
Wire Wire Line
	7700 4100 7950 4100
Wire Wire Line
	7950 4100 7950 4200
Wire Wire Line
	7950 4650 7950 4500
Wire Wire Line
	7950 4650 8150 4650
$Comp
L Device:R R4
U 1 1 5C30FE8A
P 7800 5900
F 0 "R4" V 7880 5900 50  0000 C CNN
F 1 "10k" V 7800 5900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7730 5900 50  0001 C CNN
F 3 "" H 7800 5900 50  0001 C CNN
	1    7800 5900
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 5900 8050 5900
Connection ~ 8050 5900
Wire Wire Line
	8050 5900 8050 6050
Wire Wire Line
	7650 5900 7650 5650
Connection ~ 7650 5650
Wire Wire Line
	7650 5650 7750 5650
Wire Wire Line
	7800 4650 7950 4650
Connection ~ 7950 4650
$Comp
L Device:CP C1
U 1 1 5C3740C4
P 1400 2350
F 0 "C1" H 1518 2396 50  0000 L CNN
F 1 "CP" H 1518 2305 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P3.80mm" H 1438 2200 50  0001 C CNN
F 3 "~" H 1400 2350 50  0001 C CNN
	1    1400 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5C37439A
P 1800 2350
F 0 "C2" H 1918 2396 50  0000 L CNN
F 1 "CP" H 1918 2305 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P3.80mm" H 1838 2200 50  0001 C CNN
F 3 "~" H 1800 2350 50  0001 C CNN
	1    1800 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C37446B
P 1400 2650
F 0 "#PWR05" H 1400 2400 50  0001 C CNN
F 1 "GND" H 1400 2500 50  0000 C CNN
F 2 "" H 1400 2650 50  0001 C CNN
F 3 "" H 1400 2650 50  0001 C CNN
	1    1400 2650
	1    0    0    -1  
$EndComp
Text GLabel 1100 2100 0    60   Input ~ 0
12v
Text GLabel 2000 2050 2    39   Input ~ 0
5va
Wire Wire Line
	1100 2100 1400 2100
Wire Wire Line
	1400 2100 1400 2200
Wire Wire Line
	1800 2050 1800 2200
Wire Wire Line
	1800 2500 1800 2550
Wire Wire Line
	1400 2500 1400 2550
$Comp
L conn:Conn_01x02 J6
U 1 1 5C37D39D
P 950 1000
F 0 "J6" H 950 1100 50  0000 C CNN
F 1 "pow5V" H 950 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 950 1000 50  0001 C CNN
F 3 "" H 950 1000 50  0001 C CNN
	1    950  1000
	-1   0    0    1   
$EndComp
$Comp
L conn:Conn_01x02 J2
U 1 1 5C383C5C
P 750 1500
F 0 "J2" H 750 1600 50  0000 C CNN
F 1 "pow12V" H 750 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 750 1500 50  0001 C CNN
F 3 "" H 750 1500 50  0001 C CNN
	1    750  1500
	-1   0    0    1   
$EndComp
Text GLabel 8000 1400 2    39   Input ~ 0
T0b
Wire Wire Line
	7700 4850 7800 4850
Wire Wire Line
	8100 4850 8200 4850
Text GLabel 1300 1000 2    39   Input ~ 0
uCgnd
Text GLabel 8000 1250 2    39   Input ~ 0
UvLed0b
Wire Wire Line
	1150 1000 1300 1000
Text GLabel 1900 2600 2    39   Input ~ 0
uCgnd
Wire Wire Line
	1800 2600 1900 2600
Wire Wire Line
	950  1500 1150 1500
Wire Wire Line
	1150 1500 1150 1650
Text GLabel 2900 850  0    39   Input ~ 0
5vCap
Text GLabel 2900 750  0    39   Input ~ 0
uCgnd
Wire Wire Line
	3150 1050 2900 1050
Wire Wire Line
	2900 950  3150 950 
Wire Wire Line
	2900 850  3150 850 
Wire Wire Line
	2900 750  3150 750 
$Comp
L w_connectors:Arduino_Mega_Header J1
U 1 1 5C5E43E2
P 2550 5100
F 0 "J1" H 2550 6593 60  0000 C CNN
F 1 "Arduino_Mega_Header" H 2550 6487 60  0000 C CNN
F 2 "w_conn_misc:arduino_mega_header_lampCustom" H 2550 6381 60  0000 C CNN
F 3 "" H 2550 5100 60  0000 C CNN
	1    2550 5100
	1    0    0    -1  
$EndComp
$Comp
L transistors:IRF540N Q2
U 1 1 5C5E5059
P 10100 5600
F 0 "Q2" H 10350 5675 50  0000 L CNN
F 1 "IRF540N" H 10350 5600 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 10350 5525 50  0001 L CIN
F 3 "" H 10100 5600 50  0001 L CNN
	1    10100 5600
	1    0    0    -1  
$EndComp
Text GLabel 10000 5100 0    60   Input ~ 0
12v
$Comp
L Device:R R6
U 1 1 5C5E5061
P 9550 5600
F 0 "R6" V 9630 5600 50  0000 C CNN
F 1 "10" V 9550 5600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9480 5600 50  0001 C CNN
F 3 "" H 9550 5600 50  0001 C CNN
	1    9550 5600
	0    1    1    0   
$EndComp
Text GLabel 9250 5600 0    39   Input ~ 0
pinPWM1
$Comp
L power:GND #PWR0101
U 1 1 5C5E5069
P 10200 6000
F 0 "#PWR0101" H 10200 5750 50  0001 C CNN
F 1 "GND" H 10200 5850 50  0000 C CNN
F 2 "" H 10200 6000 50  0001 C CNN
F 3 "" H 10200 6000 50  0001 C CNN
	1    10200 6000
	1    0    0    -1  
$EndComp
Text GLabel 10250 5100 2    39   Input ~ 0
H1a
Text GLabel 10250 5200 2    39   Input ~ 0
H1b
Wire Wire Line
	9700 5600 9800 5600
Wire Wire Line
	9250 5600 9400 5600
Wire Wire Line
	10200 5800 10200 5850
Wire Wire Line
	10200 5200 10250 5200
Wire Wire Line
	10200 5200 10200 5400
$Comp
L Device:R R7
U 1 1 5C5E5079
P 9950 5850
F 0 "R7" V 10030 5850 50  0000 C CNN
F 1 "10k" V 9950 5850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9880 5850 50  0001 C CNN
F 3 "" H 9950 5850 50  0001 C CNN
	1    9950 5850
	0    1    1    0   
$EndComp
Wire Wire Line
	10100 5850 10200 5850
Connection ~ 10200 5850
Wire Wire Line
	10200 5850 10200 6000
Wire Wire Line
	9800 5850 9800 5600
Connection ~ 9800 5600
Wire Wire Line
	9800 5600 9900 5600
Wire Wire Line
	10000 5100 10250 5100
Wire Wire Line
	7850 5150 8100 5150
Wire Notes Line
	4750 550  4750 3000
Wire Notes Line
	4750 3000 2450 3000
Wire Notes Line
	2450 3000 2450 550 
Wire Notes Line
	550  550  550  2900
Wire Notes Line
	550  2900 2300 2900
Wire Notes Line
	2300 2900 2300 550 
Text GLabel 7700 4850 0    39   Input ~ 0
PinLed0
Text GLabel 7800 4650 0    39   Input ~ 0
PinTh0
Text GLabel 8000 1700 2    39   Input ~ 0
T1b
Text GLabel 8000 1550 2    39   Input ~ 0
UvLed1b
Text GLabel 10300 4650 2    39   Input ~ 0
T1a
Text GLabel 10350 4850 2    39   Input ~ 0
UvLed1a
Text GLabel 9850 4100 0    60   Input ~ 0
5va
$Comp
L Device:R R8
U 1 1 5C63880A
P 10100 4350
F 0 "R8" V 10180 4350 50  0000 C CNN
F 1 "100k" V 10100 4350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10030 4350 50  0001 C CNN
F 3 "" H 10100 4350 50  0001 C CNN
	1    10100 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5C638811
P 10100 4850
F 0 "R9" V 10180 4850 50  0000 C CNN
F 1 "560" V 10100 4850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10030 4850 50  0001 C CNN
F 3 "" H 10100 4850 50  0001 C CNN
	1    10100 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	9850 4100 10100 4100
Wire Wire Line
	10100 4100 10100 4200
Wire Wire Line
	10100 4650 10100 4500
Wire Wire Line
	10100 4650 10300 4650
Wire Wire Line
	9950 4650 10100 4650
Connection ~ 10100 4650
Wire Wire Line
	9850 4850 9950 4850
Wire Wire Line
	10250 4850 10350 4850
Text GLabel 9850 4850 0    39   Input ~ 0
PinLed1
Text GLabel 9950 4650 0    39   Input ~ 0
PinTh1
$Comp
L conn:Conn_01x05 J4
U 1 1 5C63A61F
P 5650 950
F 0 "J4" H 5650 1150 50  0000 C CNN
F 1 "Conn_01x05" H 5650 650 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 5650 950 50  0001 C CNN
F 3 "" H 5650 950 50  0001 C CNN
	1    5650 950 
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Therm1
U 1 1 5C63A626
P 5650 1600
F 0 "Therm1" H 5650 1700 50  0000 C CNN
F 1 "Conn_01x02" H 5650 1400 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5650 1600 50  0001 C CNN
F 3 "" H 5650 1600 50  0001 C CNN
	1    5650 1600
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Heater1
U 1 1 5C63A62D
P 5650 2050
F 0 "Heater1" H 5650 2150 50  0000 C CNN
F 1 "Conn_01x02" H 5650 1850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5650 2050 50  0001 C CNN
F 3 "" H 5650 2050 50  0001 C CNN
	1    5650 2050
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Led1
U 1 1 5C63A634
P 5650 2500
F 0 "Led1" H 5650 2600 50  0000 C CNN
F 1 "Conn_01x02" H 5650 2300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5650 2500 50  0001 C CNN
F 3 "" H 5650 2500 50  0001 C CNN
	1    5650 2500
	1    0    0    -1  
$EndComp
Text GLabel 5200 1050 0    39   Input ~ 0
sda1
Text GLabel 5200 1150 0    39   Input ~ 0
scl1
Text GLabel 5250 1600 0    39   Input ~ 0
T1a
Text GLabel 5250 2050 0    39   Input ~ 0
H1a
Text GLabel 5250 2150 0    39   Input ~ 0
H1b
Text GLabel 5300 2500 0    39   Input ~ 0
UvLed1a
Text GLabel 5300 2600 0    39   Input ~ 0
UvLed1b
Text GLabel 5250 1700 0    39   Input ~ 0
T1b
Wire Wire Line
	5250 1600 5450 1600
Wire Wire Line
	5250 1700 5450 1700
Wire Wire Line
	5250 2050 5450 2050
Wire Wire Line
	5250 2150 5450 2150
Wire Wire Line
	5300 2600 5450 2600
Wire Wire Line
	5450 1150 5200 1150
Wire Wire Line
	5200 950  5450 950 
Wire Wire Line
	5200 850  5450 850 
Wire Notes Line
	4850 2850 4850 600 
Wire Notes Line
	4850 600  7000 600 
Wire Notes Line
	7000 600  7000 2850
Wire Notes Line
	7000 2850 4850 2850
$Comp
L conn:Conn_01x02 Serial2
U 1 1 5C641089
P 10550 2000
F 0 "Serial2" H 10550 2100 50  0000 C CNN
F 1 "Conn_01x02" H 10550 1800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10550 2000 50  0001 C CNN
F 3 "" H 10550 2000 50  0001 C CNN
	1    10550 2000
	1    0    0    -1  
$EndComp
Text GLabel 1850 4800 0    39   Input ~ 0
PinTh0
Text GLabel 1850 4600 0    39   Input ~ 0
PinLed0
Text GLabel 3300 4350 2    39   Input ~ 0
pinPWM0
Text GLabel 1850 4900 0    39   Input ~ 0
PinTh1
Text GLabel 1850 4700 0    39   Input ~ 0
PinLed1
Text GLabel 3300 4500 2    39   Input ~ 0
pinPWM1
Text GLabel 3500 4250 2    39   Input ~ 0
sda0
Text GLabel 3250 4200 2    39   Input ~ 0
scl0
Text GLabel 3450 5000 2    39   Input ~ 0
sda1
Text GLabel 3250 4950 2    39   Input ~ 0
scl1
$Comp
L Switch:SW_Push SW1
U 1 1 5C64DA10
P 7750 2250
F 0 "SW1" H 7750 2535 50  0000 C CNN
F 1 "SW_Push" H 7750 2444 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 7750 2450 50  0001 C CNN
F 3 "" H 7750 2450 50  0001 C CNN
	1    7750 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5C64DBEE
P 8100 2600
F 0 "D1" H 8092 2345 50  0000 C CNN
F 1 "LED" H 8092 2436 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8100 2600 50  0001 C CNN
F 3 "~" H 8100 2600 50  0001 C CNN
	1    8100 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5C64DCBA
P 7700 2600
F 0 "R5" V 7780 2600 50  0000 C CNN
F 1 "560" V 7700 2600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7630 2600 50  0001 C CNN
F 3 "" H 7700 2600 50  0001 C CNN
	1    7700 2600
	0    1    1    0   
$EndComp
Text GLabel 8100 2250 2    39   Input ~ 0
uCgnd
Text GLabel 8400 2600 2    39   Input ~ 0
uCgnd
Text GLabel 7450 2250 0    39   Input ~ 0
sw
Text GLabel 7450 2600 0    39   Input ~ 0
ledStat
Wire Wire Line
	7450 2250 7550 2250
Wire Wire Line
	7950 2250 8100 2250
Wire Wire Line
	7450 2600 7550 2600
Wire Wire Line
	7850 2600 7950 2600
Wire Wire Line
	8250 2600 8400 2600
Text GLabel 10150 2000 0    39   Input ~ 0
Tx
Text GLabel 10150 2100 0    39   Input ~ 0
Rx
Wire Wire Line
	10150 2000 10350 2000
Wire Wire Line
	10350 2100 10150 2100
Text GLabel 1900 5450 0    39   Input ~ 0
sw
Text GLabel 1650 5450 0    39   Input ~ 0
ledStat
Wire Notes Line
	7100 2750 7100 1900
Wire Notes Line
	7100 1900 8700 1900
Wire Notes Line
	8700 1900 8700 2750
Wire Notes Line
	8700 2750 7100 2750
Wire Notes Line
	6650 6250 6650 3900
Wire Notes Line
	6650 3900 8550 3900
Wire Notes Line
	8550 3900 8550 6250
Wire Notes Line
	8550 6250 6650 6250
Wire Notes Line
	8900 6200 8900 3950
Wire Notes Line
	8900 3950 10700 3950
Wire Notes Line
	10700 3950 10700 6200
Wire Notes Line
	10700 6200 8900 6200
Wire Wire Line
	3050 4200 3250 4200
Wire Wire Line
	3100 4250 3500 4250
Wire Wire Line
	1850 4900 2000 4900
Text GLabel 3350 5150 2    39   Input ~ 0
Tx
Text GLabel 3500 5200 2    39   Input ~ 0
Rx
Wire Wire Line
	3100 5150 3350 5150
Wire Wire Line
	3050 5200 3500 5200
Wire Wire Line
	5300 2500 5450 2500
$Comp
L power:GND #PWR01
U 1 1 5C4C0CC6
P 1150 1650
F 0 "#PWR01" H 1150 1400 50  0001 C CNN
F 1 "GND" H 1150 1500 50  0000 C CNN
F 2 "" H 1150 1650 50  0001 C CNN
F 3 "" H 1150 1650 50  0001 C CNN
	1    1150 1650
	1    0    0    -1  
$EndComp
Text GLabel 1950 4250 0    39   Input ~ 0
12vin
Wire Wire Line
	2050 4250 1950 4250
$Comp
L power:GND #PWR0103
U 1 1 5C7EDB58
P 1750 4200
F 0 "#PWR0103" H 1750 3950 50  0001 C CNN
F 1 "GND" H 1750 4050 50  0000 C CNN
F 2 "" H 1750 4200 50  0001 C CNN
F 3 "" H 1750 4200 50  0001 C CNN
	1    1750 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	1750 4200 1900 4200
Wire Wire Line
	3100 4950 3250 4950
Wire Wire Line
	3050 5000 3450 5000
Wire Notes Line
	2300 550  550  550 
Wire Notes Line
	2450 550  4750 550 
Text GLabel 1750 4000 0    39   Input ~ 0
5v
Wire Wire Line
	1750 4000 1850 4000
Wire Wire Line
	1850 4000 1850 4100
Wire Wire Line
	1850 4100 2000 4100
Text GLabel 3200 5400 2    39   Input ~ 0
5va
Wire Wire Line
	3100 5400 3200 5400
Text GLabel 1900 5350 0    39   Input ~ 0
5va
Wire Wire Line
	1900 5350 2050 5350
Wire Wire Line
	3300 4350 3100 4350
Text GLabel 1950 1400 2    39   Input ~ 0
12vin
Wire Wire Line
	1800 1400 1950 1400
$Comp
L conn:Conn_01x02 J8
U 1 1 5C62AF0E
P 1600 1500
F 0 "J8" H 1600 1600 50  0000 C CNN
F 1 "pow12V" H 1600 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1600 1500 50  0001 C CNN
F 3 "" H 1600 1500 50  0001 C CNN
	1    1600 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 1500 2000 1500
Wire Wire Line
	2000 1500 2000 1650
$Comp
L power:GND #PWR0104
U 1 1 5C62AF17
P 2000 1650
F 0 "#PWR0104" H 2000 1400 50  0001 C CNN
F 1 "GND" H 2000 1500 50  0000 C CNN
F 2 "" H 2000 1650 50  0001 C CNN
F 3 "" H 2000 1650 50  0001 C CNN
	1    2000 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 1100 8000 1150
Wire Wire Line
	1850 4800 2000 4800
Wire Wire Line
	1850 4600 2050 4600
Wire Wire Line
	1850 4700 2050 4700
$Comp
L conn:Conn_01x06 J9
U 1 1 5C63A0EA
P 10300 1150
F 0 "J9" H 10300 1350 50  0000 C CNN
F 1 "sd_conn" H 10300 750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 10300 1150 50  0001 C CNN
F 3 "" H 10300 1150 50  0001 C CNN
	1    10300 1150
	1    0    0    -1  
$EndComp
Text GLabel 1900 6100 0    39   Input ~ 0
MISO
Text GLabel 3200 6150 2    39   Input ~ 0
MOSI
Text GLabel 3500 6200 2    39   Input ~ 0
SS
Text GLabel 1600 6150 0    39   Input ~ 0
SCK
Text GLabel 10000 950  0    39   Input ~ 0
uCgnd
Text GLabel 10000 1150 0    39   Input ~ 0
MISO
Text GLabel 10000 1250 0    39   Input ~ 0
MOSI
Text GLabel 10000 1450 0    39   Input ~ 0
SS
Text GLabel 10000 1350 0    39   Input ~ 0
SCK
Text GLabel 10000 1050 0    39   Input ~ 0
5va
Wire Wire Line
	10000 950  10100 950 
Wire Wire Line
	10000 1050 10100 1050
Wire Wire Line
	10100 1150 10000 1150
Wire Wire Line
	10000 1250 10100 1250
Wire Wire Line
	10100 1350 10000 1350
Wire Wire Line
	10000 1450 10100 1450
Wire Wire Line
	3050 6150 3200 6150
Wire Wire Line
	1600 6150 2050 6150
Wire Wire Line
	1900 6100 2000 6100
Wire Wire Line
	3100 6200 3500 6200
Wire Wire Line
	3050 4400 3250 4400
Wire Wire Line
	3250 4400 3250 4500
Wire Wire Line
	3250 4500 3300 4500
Wire Wire Line
	1650 5450 1700 5450
Wire Wire Line
	1700 5450 1700 5400
Wire Wire Line
	1700 5400 2000 5400
Wire Wire Line
	1900 5450 2050 5450
Text GLabel 3250 5250 2    39   Input ~ 0
sda3
Text GLabel 3450 5300 2    39   Input ~ 0
scl3
Wire Wire Line
	3100 5250 3250 5250
Wire Wire Line
	3050 5300 3450 5300
Text GLabel 10200 2600 0    39   Input ~ 0
sda3
Text GLabel 10200 2500 0    39   Input ~ 0
scl3
$Comp
L conn:Conn_01x02 I2C3
U 1 1 5C7C6622
P 10550 2500
F 0 "I2C3" H 10550 2600 50  0000 C CNN
F 1 "Conn_01x02" H 10550 2300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10550 2500 50  0001 C CNN
F 3 "" H 10550 2500 50  0001 C CNN
	1    10550 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 2500 10350 2500
Wire Wire Line
	10350 2600 10200 2600
Wire Notes Line
	9550 700  10600 700 
Wire Notes Line
	10600 700  10600 1650
Wire Notes Line
	10600 1650 9550 1650
Wire Notes Line
	9550 1650 9550 700 
Wire Notes Line
	9850 1750 10900 1750
Wire Notes Line
	10900 1750 10900 2850
Wire Notes Line
	10900 2850 9850 2850
Wire Notes Line
	9850 2850 9850 1750
Wire Notes Line
	7450 900  8400 900 
Wire Notes Line
	8400 900  8400 1800
Wire Notes Line
	8400 1800 7450 1800
Wire Notes Line
	7450 900  7450 1800
Wire Wire Line
	5200 1050 5450 1050
Text GLabel 7850 1150 0    39   Input ~ 0
uCgnd
Wire Wire Line
	7850 1150 8000 1150
Connection ~ 8000 1150
Text GLabel 5200 850  0    39   Input ~ 0
uCgnd
Text GLabel 5200 950  0    39   Input ~ 0
5vCap
Wire Wire Line
	8000 1150 8000 1700
NoConn ~ 2050 3950
NoConn ~ 2000 4000
NoConn ~ 2050 4050
NoConn ~ 2000 4350
NoConn ~ 2050 4400
NoConn ~ 2000 4450
NoConn ~ 2050 4500
NoConn ~ 2000 4550
NoConn ~ 2000 4650
NoConn ~ 2050 4850
NoConn ~ 2050 4950
NoConn ~ 2000 5000
NoConn ~ 2050 5050
NoConn ~ 2000 5100
NoConn ~ 2050 5150
NoConn ~ 2000 5500
NoConn ~ 2050 5550
NoConn ~ 2000 5600
NoConn ~ 2000 5700
NoConn ~ 2050 5650
NoConn ~ 2000 5800
NoConn ~ 2000 5900
NoConn ~ 2000 6000
NoConn ~ 2050 6050
NoConn ~ 2050 5950
NoConn ~ 2050 5850
NoConn ~ 2050 5750
NoConn ~ 3050 6050
NoConn ~ 3050 5950
NoConn ~ 3050 5850
NoConn ~ 3100 5900
NoConn ~ 3100 6000
NoConn ~ 3100 6100
NoConn ~ 3050 5450
NoConn ~ 3050 5550
NoConn ~ 3050 5650
NoConn ~ 3050 5750
NoConn ~ 3100 5800
NoConn ~ 3100 5700
NoConn ~ 3100 5600
NoConn ~ 3100 5500
NoConn ~ 3100 4500
NoConn ~ 3100 4600
NoConn ~ 3100 4700
NoConn ~ 3100 4800
NoConn ~ 3050 4550
NoConn ~ 3050 4650
NoConn ~ 3050 4750
NoConn ~ 3050 4850
NoConn ~ 3100 5050
NoConn ~ 3050 5100
NoConn ~ 3050 4000
NoConn ~ 3100 3950
NoConn ~ 3100 4050
NoConn ~ 3100 4150
NoConn ~ 3050 4300
Wire Wire Line
	2050 4150 1900 4150
Wire Wire Line
	1900 4150 1900 4200
Connection ~ 1900 4200
Wire Wire Line
	1900 4200 2000 4200
Wire Wire Line
	1800 2050 1900 2050
Text GLabel 1750 1900 0    39   Input ~ 0
5vCap
Wire Wire Line
	1750 1900 1900 1900
Wire Wire Line
	1900 1900 1900 2050
Connection ~ 1900 2050
Wire Wire Line
	1900 2050 2000 2050
NoConn ~ 3050 4100
NoConn ~ 3050 6250
NoConn ~ 2000 6200
Wire Wire Line
	1400 2550 1800 2550
Connection ~ 1400 2550
Wire Wire Line
	1400 2550 1400 2650
Connection ~ 1800 2550
Wire Wire Line
	1800 2550 1800 2600
Text GLabel 3150 650  0    39   Input ~ 0
uCgnd
Text GLabel 5450 750  0    39   Input ~ 0
uCgnd
$EndSCHEMATC
