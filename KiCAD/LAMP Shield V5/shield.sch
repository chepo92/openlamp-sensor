EESchema Schematic File Version 4
LIBS:shield-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L shield-rescue:Conn_01x05-conn J3
U 1 1 5BE49349
P 6200 900
F 0 "J3" H 6200 1100 50  0000 C CNN
F 1 "Conn_01x05" H 6200 600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 6200 900 50  0001 C CNN
F 3 "" H 6200 900 50  0001 C CNN
	1    6200 900 
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:Conn_01x02-conn Therm0
U 1 1 5BE4967B
P 6200 1550
F 0 "Therm0" H 6200 1650 50  0000 C CNN
F 1 "Conn_01x02" H 6200 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6200 1550 50  0001 C CNN
F 3 "" H 6200 1550 50  0001 C CNN
	1    6200 1550
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:Conn_01x02-conn Heater0
U 1 1 5BE49721
P 6200 1950
F 0 "Heater0" H 6200 2050 50  0000 C CNN
F 1 "Conn_01x02" H 6200 1750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6200 1950 50  0001 C CNN
F 3 "" H 6200 1950 50  0001 C CNN
	1    6200 1950
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:Conn_01x02-conn Led0
U 1 1 5BE498DC
P 6200 2400
F 0 "Led0" H 6200 2500 50  0000 C CNN
F 1 "Conn_01x02" H 6200 2200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 6200 2400 50  0001 C CNN
F 3 "" H 6200 2400 50  0001 C CNN
	1    6200 2400
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:IRF540N-transistors Q1
U 1 1 5BE4A98D
P 10550 5500
F 0 "Q1" H 10800 5575 50  0000 L CNN
F 1 "IRF540N" H 10800 5500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 10800 5425 50  0001 L CIN
F 3 "" H 10550 5500 50  0001 L CNN
	1    10550 5500
	1    0    0    -1  
$EndComp
Text GLabel 10450 5000 0    60   Input ~ 0
12v
$Comp
L Device:R R1
U 1 1 5BE4AB8C
P 10000 5500
F 0 "R1" V 10080 5500 50  0000 C CNN
F 1 "10" V 10000 5500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9930 5500 50  0001 C CNN
F 3 "" H 10000 5500 50  0001 C CNN
	1    10000 5500
	0    1    1    0   
$EndComp
Text GLabel 1050 850  2    39   Input ~ 0
5vCap
Text GLabel 7800 5500 0    39   Input ~ 0
pinPWM0
$Comp
L power:GND #PWR03
U 1 1 5BE4B11D
P 10650 5900
F 0 "#PWR03" H 10650 5650 50  0001 C CNN
F 1 "GND" H 10650 5750 50  0000 C CNN
F 2 "" H 10650 5900 50  0001 C CNN
F 3 "" H 10650 5900 50  0001 C CNN
	1    10650 5900
	1    0    0    -1  
$EndComp
Text GLabel 1050 1400 2    39   Input ~ 0
12vin
Text GLabel 5750 1000 0    39   Input ~ 0
sda0
Text GLabel 5750 1100 0    39   Input ~ 0
scl0
Text GLabel 5800 1550 0    39   Input ~ 0
T0a
Text GLabel 7100 1950 0    39   Input ~ 0
H0a
Text GLabel 7100 2050 0    39   Input ~ 0
H0b
Text GLabel 7150 2400 0    39   Input ~ 0
UvLed0a
Text GLabel 7150 2500 0    39   Input ~ 0
UvLed0b
Text GLabel 5800 1650 0    39   Input ~ 0
T0b
Text GLabel 8800 4600 2    39   Input ~ 0
T0a
Text GLabel 8800 5000 2    39   Input ~ 0
H0a
Text GLabel 8800 5100 2    39   Input ~ 0
H0b
Text GLabel 8350 4050 0    60   Input ~ 0
5va
$Comp
L Device:R R3
U 1 1 5BE4D45D
P 8600 4300
F 0 "R3" V 8680 4300 50  0000 C CNN
F 1 "100k" V 8600 4300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8530 4300 50  0001 C CNN
F 3 "" H 8600 4300 50  0001 C CNN
	1    8600 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10150 5500 10250 5500
Wire Wire Line
	9700 5500 9850 5500
Wire Wire Line
	10650 5700 10650 5750
Wire Wire Line
	950  1400 1050 1400
Wire Wire Line
	950  850  1050 850 
Wire Wire Line
	5800 1550 6000 1550
Wire Wire Line
	5800 1650 6000 1650
Wire Wire Line
	7100 1950 7300 1950
Wire Wire Line
	7100 2050 7300 2050
Wire Wire Line
	7150 2400 7300 2400
Wire Wire Line
	7150 2500 7300 2500
Wire Wire Line
	10650 5100 10700 5100
Wire Wire Line
	10650 5100 10650 5300
Wire Wire Line
	8350 4050 8600 4050
Wire Wire Line
	8600 4050 8600 4150
Wire Wire Line
	8600 4600 8600 4450
Wire Wire Line
	8600 4600 8800 4600
$Comp
L Device:R R4
U 1 1 5C30FE8A
P 10400 5750
F 0 "R4" V 10480 5750 50  0000 C CNN
F 1 "10k" V 10400 5750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10330 5750 50  0001 C CNN
F 3 "" H 10400 5750 50  0001 C CNN
	1    10400 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	10550 5750 10650 5750
Connection ~ 10650 5750
Wire Wire Line
	10650 5750 10650 5900
Wire Wire Line
	10250 5750 10250 5500
Connection ~ 10250 5500
Wire Wire Line
	10250 5500 10350 5500
Wire Wire Line
	8450 4600 8600 4600
Connection ~ 8600 4600
$Comp
L Device:CP C1
U 1 1 5C3740C4
P 1400 2350
F 0 "C1" H 1518 2396 50  0000 L CNN
F 1 "CP" H 1518 2305 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P3.80mm" H 1438 2200 50  0001 C CNN
F 3 "~" H 1400 2350 50  0001 C CNN
	1    1400 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5C37439A
P 1800 2350
F 0 "C2" H 1918 2396 50  0000 L CNN
F 1 "CP" H 1918 2305 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P3.80mm" H 1838 2200 50  0001 C CNN
F 3 "~" H 1800 2350 50  0001 C CNN
	1    1800 2350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C37446B
P 1400 2650
F 0 "#PWR05" H 1400 2400 50  0001 C CNN
F 1 "GND" H 1400 2500 50  0000 C CNN
F 2 "" H 1400 2650 50  0001 C CNN
F 3 "" H 1400 2650 50  0001 C CNN
	1    1400 2650
	1    0    0    -1  
$EndComp
Text GLabel 1100 2100 0    60   Input ~ 0
12v
Text GLabel 2000 2050 2    39   Input ~ 0
5va
Wire Wire Line
	1100 2100 1400 2100
Wire Wire Line
	1400 2100 1400 2200
Wire Wire Line
	1800 2050 1800 2200
$Comp
L shield-rescue:Conn_01x02-conn J6
U 1 1 5C37D39D
P 750 950
F 0 "J6" H 750 1050 50  0000 C CNN
F 1 "pow5V" H 750 750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 750 950 50  0001 C CNN
F 3 "" H 750 950 50  0001 C CNN
	1    750  950 
	-1   0    0    1   
$EndComp
$Comp
L shield-rescue:Conn_01x02-conn J2
U 1 1 5C383C5C
P 750 1500
F 0 "J2" H 750 1600 50  0000 C CNN
F 1 "pow12V" H 750 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 750 1500 50  0001 C CNN
F 3 "" H 750 1500 50  0001 C CNN
	1    750  1500
	-1   0    0    1   
$EndComp
Text GLabel 8950 1350 2    39   Input ~ 0
T0b
Text GLabel 1050 950  2    39   Input ~ 0
uCgnd
Text GLabel 4950 5100 2    39   Input ~ 0
UvLed0b
Wire Wire Line
	950  950  1050 950 
Wire Wire Line
	950  1500 1050 1500
Wire Wire Line
	1050 1500 1050 1650
Text GLabel 5750 900  0    39   Input ~ 0
5vCap
Text GLabel 5750 800  0    39   Input ~ 0
uCgnd
Wire Wire Line
	6000 1100 5750 1100
Wire Wire Line
	5750 1000 6000 1000
Wire Wire Line
	5750 900  6000 900 
Wire Wire Line
	5750 800  6000 800 
$Comp
L shield-rescue:Arduino_Mega_Header-w_connectors-SensoresPCB-rescue J1
U 1 1 5C5E43E2
P 1650 5100
F 0 "J1" H 1650 6593 60  0000 C CNN
F 1 "Arduino_Mega_Header" H 1650 6487 60  0000 C CNN
F 2 "w_conn_misc:arduino_mega_header_lampCustom" H 1650 6381 60  0000 C CNN
F 3 "" H 1650 5100 60  0000 C CNN
	1    1650 5100
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:IRF540N-transistors Q2
U 1 1 5C5E5059
P 8650 5500
F 0 "Q2" H 8900 5575 50  0000 L CNN
F 1 "IRF540N" H 8900 5500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 8900 5425 50  0001 L CIN
F 3 "" H 8650 5500 50  0001 L CNN
	1    8650 5500
	1    0    0    -1  
$EndComp
Text GLabel 8550 5000 0    60   Input ~ 0
12v
$Comp
L Device:R R6
U 1 1 5C5E5061
P 8100 5500
F 0 "R6" V 8180 5500 50  0000 C CNN
F 1 "10" V 8100 5500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8030 5500 50  0001 C CNN
F 3 "" H 8100 5500 50  0001 C CNN
	1    8100 5500
	0    1    1    0   
$EndComp
Text GLabel 9700 5500 0    39   Input ~ 0
pinPWM1
$Comp
L power:GND #PWR0101
U 1 1 5C5E5069
P 8750 5900
F 0 "#PWR0101" H 8750 5650 50  0001 C CNN
F 1 "GND" H 8750 5750 50  0000 C CNN
F 2 "" H 8750 5900 50  0001 C CNN
F 3 "" H 8750 5900 50  0001 C CNN
	1    8750 5900
	1    0    0    -1  
$EndComp
Text GLabel 10700 5000 2    39   Input ~ 0
H1a
Text GLabel 10700 5100 2    39   Input ~ 0
H1b
Wire Wire Line
	8250 5500 8350 5500
Wire Wire Line
	7800 5500 7950 5500
Wire Wire Line
	8750 5700 8750 5750
Wire Wire Line
	8750 5100 8800 5100
Wire Wire Line
	8750 5100 8750 5300
$Comp
L Device:R R7
U 1 1 5C5E5079
P 8500 5750
F 0 "R7" V 8580 5750 50  0000 C CNN
F 1 "10k" V 8500 5750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8430 5750 50  0001 C CNN
F 3 "" H 8500 5750 50  0001 C CNN
	1    8500 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	8650 5750 8750 5750
Connection ~ 8750 5750
Wire Wire Line
	8750 5750 8750 5900
Wire Wire Line
	8350 5750 8350 5500
Connection ~ 8350 5500
Wire Wire Line
	8350 5500 8450 5500
Wire Wire Line
	8550 5000 8800 5000
Wire Wire Line
	10450 5000 10700 5000
Wire Notes Line
	6550 600  6550 3050
Wire Notes Line
	5300 3050 5300 600 
Wire Notes Line
	550  550  550  2900
Wire Notes Line
	550  2900 2300 2900
Wire Notes Line
	2300 2900 2300 550 
Text GLabel 8450 4600 0    39   Input ~ 0
PinTh0
Text GLabel 8950 1650 2    39   Input ~ 0
T1b
Text GLabel 6750 5100 2    39   Input ~ 0
UvLed1b
Text GLabel 10750 4650 2    39   Input ~ 0
T1a
Text GLabel 10300 4100 0    60   Input ~ 0
5va
$Comp
L Device:R R8
U 1 1 5C63880A
P 10550 4350
F 0 "R8" V 10630 4350 50  0000 C CNN
F 1 "100k" V 10550 4350 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 10480 4350 50  0001 C CNN
F 3 "" H 10550 4350 50  0001 C CNN
	1    10550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	10300 4100 10550 4100
Wire Wire Line
	10550 4100 10550 4200
Wire Wire Line
	10550 4650 10550 4500
Wire Wire Line
	10550 4650 10750 4650
Wire Wire Line
	10400 4650 10550 4650
Connection ~ 10550 4650
Text GLabel 10400 4650 0    39   Input ~ 0
PinTh1
$Comp
L shield-rescue:Conn_01x05-conn J4
U 1 1 5C63A61F
P 7500 900
F 0 "J4" H 7500 1100 50  0000 C CNN
F 1 "Conn_01x05" H 7500 600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 7500 900 50  0001 C CNN
F 3 "" H 7500 900 50  0001 C CNN
	1    7500 900 
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:Conn_01x02-conn Therm1
U 1 1 5C63A626
P 7500 1550
F 0 "Therm1" H 7500 1650 50  0000 C CNN
F 1 "Conn_01x02" H 7500 1350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 7500 1550 50  0001 C CNN
F 3 "" H 7500 1550 50  0001 C CNN
	1    7500 1550
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:Conn_01x02-conn Heater1
U 1 1 5C63A62D
P 7500 1950
F 0 "Heater1" H 7500 2050 50  0000 C CNN
F 1 "Conn_01x02" H 7500 1750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 7500 1950 50  0001 C CNN
F 3 "" H 7500 1950 50  0001 C CNN
	1    7500 1950
	1    0    0    -1  
$EndComp
$Comp
L shield-rescue:Conn_01x02-conn Led1
U 1 1 5C63A634
P 7500 2400
F 0 "Led1" H 7500 2500 50  0000 C CNN
F 1 "Conn_01x02" H 7500 2200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 7500 2400 50  0001 C CNN
F 3 "" H 7500 2400 50  0001 C CNN
	1    7500 2400
	1    0    0    -1  
$EndComp
Text GLabel 7050 1000 0    39   Input ~ 0
sda1
Text GLabel 7050 1100 0    39   Input ~ 0
scl1
Text GLabel 7100 1550 0    39   Input ~ 0
T1a
Text GLabel 5800 1950 0    39   Input ~ 0
H1a
Text GLabel 5800 2050 0    39   Input ~ 0
H1b
Text GLabel 5850 2400 0    39   Input ~ 0
UvLed1a
Text GLabel 5850 2500 0    39   Input ~ 0
UvLed1b
Text GLabel 7100 1650 0    39   Input ~ 0
T1b
Wire Wire Line
	7100 1550 7300 1550
Wire Wire Line
	7100 1650 7300 1650
Wire Wire Line
	5800 1950 6000 1950
Wire Wire Line
	5800 2050 6000 2050
Wire Wire Line
	5850 2500 6000 2500
Wire Wire Line
	7300 1100 7050 1100
Wire Wire Line
	7050 900  7300 900 
Wire Wire Line
	7050 800  7300 800 
Wire Notes Line
	6700 2800 6700 550 
Wire Notes Line
	7900 550  7900 2800
$Comp
L shield-rescue:Conn_01x02-conn Serial2
U 1 1 5C641089
P 10550 2000
F 0 "Serial2" H 10550 2100 50  0000 C CNN
F 1 "Conn_01x02" H 10550 1800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10550 2000 50  0001 C CNN
F 3 "" H 10550 2000 50  0001 C CNN
	1    10550 2000
	1    0    0    -1  
$EndComp
Text GLabel 950  4800 0    39   Input ~ 0
PinTh0
Text GLabel 2350 4700 2    39   Input ~ 0
PinLed0
Text GLabel 2400 4250 2    39   Input ~ 0
pinPWM0
Text GLabel 950  4900 0    39   Input ~ 0
PinTh1
Text GLabel 2350 4800 2    39   Input ~ 0
PinLed1
Text GLabel 2400 4350 2    39   Input ~ 0
pinPWM1
Text GLabel 2550 5000 2    39   Input ~ 0
sda0
Text GLabel 2350 4950 2    39   Input ~ 0
scl0
Text GLabel 2550 5100 2    39   Input ~ 0
sda1
Text GLabel 2350 5050 2    39   Input ~ 0
scl1
$Comp
L Switch:SW_Push SW1
U 1 1 5C64DA10
P 8700 2200
F 0 "SW1" H 8700 2485 50  0000 C CNN
F 1 "SW_Push" H 8700 2394 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8700 2400 50  0001 C CNN
F 3 "" H 8700 2400 50  0001 C CNN
	1    8700 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 5C64DBEE
P 9050 2550
F 0 "D1" H 9042 2295 50  0000 C CNN
F 1 "LED" H 9042 2386 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 9050 2550 50  0001 C CNN
F 3 "~" H 9050 2550 50  0001 C CNN
	1    9050 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5C64DCBA
P 8650 2550
F 0 "R5" V 8730 2550 50  0000 C CNN
F 1 "560" V 8650 2550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 8580 2550 50  0001 C CNN
F 3 "" H 8650 2550 50  0001 C CNN
	1    8650 2550
	0    1    1    0   
$EndComp
Text GLabel 9050 2200 2    39   Input ~ 0
uCgnd
Text GLabel 9350 2550 2    39   Input ~ 0
uCgnd
Text GLabel 8400 2200 0    39   Input ~ 0
btn
Text GLabel 8400 2550 0    39   Input ~ 0
ledStat
Wire Wire Line
	8400 2200 8500 2200
Wire Wire Line
	8900 2200 9050 2200
Wire Wire Line
	8400 2550 8500 2550
Wire Wire Line
	8800 2550 8900 2550
Wire Wire Line
	9200 2550 9350 2550
Text GLabel 10150 2000 0    39   Input ~ 0
Tx
Text GLabel 10150 2100 0    39   Input ~ 0
Rx
Wire Wire Line
	10150 2000 10350 2000
Wire Wire Line
	10350 2100 10150 2100
Text GLabel 1000 5450 0    39   Input ~ 0
btn
Text GLabel 750  5450 0    39   Input ~ 0
ledStat
Wire Notes Line
	8050 2700 8050 1850
Wire Notes Line
	8050 1850 9650 1850
Wire Notes Line
	9650 1850 9650 2700
Wire Notes Line
	9650 2700 8050 2700
Wire Notes Line
	7300 6200 7300 3850
Wire Notes Line
	7300 3850 9200 3850
Wire Notes Line
	9200 3850 9200 6200
Wire Notes Line
	9200 6200 7300 6200
Wire Notes Line
	9350 6200 9350 3950
Wire Notes Line
	9350 3950 11150 3950
Wire Notes Line
	11150 6200 9350 6200
Wire Wire Line
	2200 4950 2350 4950
Wire Wire Line
	950  4900 1100 4900
Text GLabel 2450 5150 2    39   Input ~ 0
Tx
Text GLabel 2600 5200 2    39   Input ~ 0
Rx
Wire Wire Line
	2200 5150 2450 5150
Wire Wire Line
	2150 5200 2600 5200
Wire Wire Line
	5850 2400 6000 2400
$Comp
L power:GND #PWR01
U 1 1 5C4C0CC6
P 1050 1650
F 0 "#PWR01" H 1050 1400 50  0001 C CNN
F 1 "GND" H 1050 1500 50  0000 C CNN
F 2 "" H 1050 1650 50  0001 C CNN
F 3 "" H 1050 1650 50  0001 C CNN
	1    1050 1650
	1    0    0    -1  
$EndComp
Text GLabel 1050 4250 0    39   Input ~ 0
12vin
Wire Wire Line
	1150 4250 1050 4250
$Comp
L power:GND #PWR0103
U 1 1 5C7EDB58
P 850 4200
F 0 "#PWR0103" H 850 3950 50  0001 C CNN
F 1 "GND" H 850 4050 50  0000 C CNN
F 2 "" H 850 4200 50  0001 C CNN
F 3 "" H 850 4200 50  0001 C CNN
	1    850  4200
	0    1    1    0   
$EndComp
Wire Wire Line
	850  4200 1000 4200
Wire Wire Line
	2200 5050 2350 5050
Wire Wire Line
	2150 5100 2550 5100
Wire Notes Line
	2300 550  550  550 
Text GLabel 850  4000 0    39   Input ~ 0
5v
Wire Wire Line
	850  4000 950  4000
Wire Wire Line
	950  4000 950  4100
Wire Wire Line
	950  4100 1100 4100
Text GLabel 2300 5400 2    39   Input ~ 0
5va
Wire Wire Line
	2200 5400 2300 5400
Text GLabel 1000 5350 0    39   Input ~ 0
5va
Wire Wire Line
	1000 5350 1150 5350
Wire Wire Line
	2400 4350 2200 4350
Text GLabel 2000 800  2    39   Input ~ 0
12v
Wire Wire Line
	1850 800  2000 800 
$Comp
L shield-rescue:Conn_01x02-conn J8
U 1 1 5C62AF0E
P 1600 1500
F 0 "J8" H 1600 1600 50  0000 C CNN
F 1 "pow12V" H 1600 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 1600 1500 50  0001 C CNN
F 3 "" H 1600 1500 50  0001 C CNN
	1    1600 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 900  1950 900 
$Comp
L power:GND #PWR0104
U 1 1 5C62AF17
P 2050 1050
F 0 "#PWR0104" H 2050 800 50  0001 C CNN
F 1 "GND" H 2050 900 50  0000 C CNN
F 2 "" H 2050 1050 50  0001 C CNN
F 3 "" H 2050 1050 50  0001 C CNN
	1    2050 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	950  4800 1100 4800
$Comp
L shield-rescue:Conn_01x06-conn J9
U 1 1 5C63A0EA
P 10300 1150
F 0 "J9" H 10300 1350 50  0000 C CNN
F 1 "sd_conn" H 10300 750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 10300 1150 50  0001 C CNN
F 3 "" H 10300 1150 50  0001 C CNN
	1    10300 1150
	1    0    0    -1  
$EndComp
Text GLabel 1000 6100 0    39   Input ~ 0
MISO
Text GLabel 2300 6150 2    39   Input ~ 0
MOSI
Text GLabel 2600 6200 2    39   Input ~ 0
SS
Text GLabel 700  6150 0    39   Input ~ 0
SCK
Text GLabel 10000 950  0    39   Input ~ 0
uCgnd
Text GLabel 10000 1150 0    39   Input ~ 0
MISO
Text GLabel 10000 1250 0    39   Input ~ 0
MOSI
Text GLabel 10000 1450 0    39   Input ~ 0
SS
Text GLabel 10000 1350 0    39   Input ~ 0
SCK
Text GLabel 10000 1050 0    39   Input ~ 0
5va
Wire Wire Line
	10000 950  10100 950 
Wire Wire Line
	10000 1050 10100 1050
Wire Wire Line
	10100 1150 10000 1150
Wire Wire Line
	10000 1250 10100 1250
Wire Wire Line
	10100 1350 10000 1350
Wire Wire Line
	10000 1450 10100 1450
Wire Wire Line
	2150 6150 2300 6150
Wire Wire Line
	700  6150 1150 6150
Wire Wire Line
	1000 6100 1100 6100
Wire Wire Line
	2200 6200 2600 6200
Wire Wire Line
	750  5450 800  5450
Wire Wire Line
	800  5450 800  5400
Wire Wire Line
	800  5400 1100 5400
Wire Wire Line
	1000 5450 1150 5450
Text GLabel 2350 5250 2    39   Input ~ 0
sda2
Text GLabel 2550 5300 2    39   Input ~ 0
scl2
Wire Wire Line
	2200 5250 2350 5250
Wire Wire Line
	2150 5300 2550 5300
Text GLabel 10200 2600 0    39   Input ~ 0
sda2
Text GLabel 10200 2500 0    39   Input ~ 0
scl2
$Comp
L shield-rescue:Conn_01x02-conn I2C3
U 1 1 5C7C6622
P 10550 2500
F 0 "I2C3" H 10550 2600 50  0000 C CNN
F 1 "Conn_01x02" H 10550 2300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 10550 2500 50  0001 C CNN
F 3 "" H 10550 2500 50  0001 C CNN
	1    10550 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 2500 10350 2500
Wire Wire Line
	10350 2600 10200 2600
Wire Notes Line
	9550 700  10600 700 
Wire Notes Line
	10600 700  10600 1650
Wire Notes Line
	10600 1650 9550 1650
Wire Notes Line
	9550 1650 9550 700 
Wire Notes Line
	9850 1750 10900 1750
Wire Notes Line
	10900 1750 10900 2850
Wire Notes Line
	10900 2850 9850 2850
Wire Notes Line
	9850 2850 9850 1750
Wire Notes Line
	8400 850  9350 850 
Wire Notes Line
	9350 850  9350 1750
Wire Notes Line
	9350 1750 8400 1750
Wire Notes Line
	8400 850  8400 1750
Wire Wire Line
	7050 1000 7300 1000
Text GLabel 8800 1100 0    39   Input ~ 0
uCgnd
Wire Wire Line
	8800 1100 8950 1100
Text GLabel 7050 800  0    39   Input ~ 0
uCgnd
Text GLabel 7050 900  0    39   Input ~ 0
5vCap
NoConn ~ 1150 3950
NoConn ~ 1100 4000
NoConn ~ 1150 4050
NoConn ~ 1100 4350
NoConn ~ 1150 4400
NoConn ~ 1100 4450
NoConn ~ 1150 4500
NoConn ~ 1100 4550
NoConn ~ 1100 4650
NoConn ~ 1150 4850
NoConn ~ 1150 4950
NoConn ~ 1100 5000
NoConn ~ 1150 5050
NoConn ~ 1100 5100
NoConn ~ 1150 5150
NoConn ~ 1100 5500
NoConn ~ 1150 5550
NoConn ~ 1100 5600
NoConn ~ 1100 5700
NoConn ~ 1150 5650
NoConn ~ 1100 5800
NoConn ~ 1100 5900
NoConn ~ 1100 6000
NoConn ~ 1150 6050
NoConn ~ 1150 5950
NoConn ~ 1150 5850
NoConn ~ 1150 5750
NoConn ~ 2150 6050
NoConn ~ 2150 5950
NoConn ~ 2150 5850
NoConn ~ 2200 5900
NoConn ~ 2200 6000
NoConn ~ 2200 6100
NoConn ~ 2150 5450
NoConn ~ 2150 5550
NoConn ~ 2150 5650
NoConn ~ 2150 5750
NoConn ~ 2200 5800
NoConn ~ 2200 5700
NoConn ~ 2200 5600
NoConn ~ 2200 5500
NoConn ~ 2200 4500
NoConn ~ 2200 4600
NoConn ~ 2150 4550
NoConn ~ 2150 4650
NoConn ~ 2150 4850
NoConn ~ 2150 4000
NoConn ~ 2200 3950
NoConn ~ 2200 4050
NoConn ~ 2200 4150
NoConn ~ 2150 4300
Wire Wire Line
	1150 4150 1000 4150
Wire Wire Line
	1000 4150 1000 4200
Connection ~ 1000 4200
Wire Wire Line
	1000 4200 1100 4200
Wire Wire Line
	1800 2050 1900 2050
Text GLabel 1750 1900 0    39   Input ~ 0
5vCap
Wire Wire Line
	1750 1900 1900 1900
Wire Wire Line
	1900 1900 1900 2050
Connection ~ 1900 2050
Wire Wire Line
	1900 2050 2000 2050
NoConn ~ 2150 4100
Text GLabel 6000 700  0    39   Input ~ 0
uCgnd
Text GLabel 7300 700  0    39   Input ~ 0
uCgnd
$Comp
L shield-rescue:IRF540N-transistors Q4
U 1 1 5D73930A
P 4800 5500
F 0 "Q4" H 5050 5575 50  0000 L CNN
F 1 "IRF540N" H 5050 5500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 5050 5425 50  0001 L CIN
F 3 "" H 4800 5500 50  0001 L CNN
	1    4800 5500
	1    0    0    -1  
$EndComp
Text GLabel 4700 5000 0    60   Input ~ 0
12v
$Comp
L Device:R R12
U 1 1 5D739311
P 4250 5500
F 0 "R12" V 4330 5500 50  0000 C CNN
F 1 "10" V 4250 5500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4180 5500 50  0001 C CNN
F 3 "" H 4250 5500 50  0001 C CNN
	1    4250 5500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5D739318
P 4900 5900
F 0 "#PWR04" H 4900 5650 50  0001 C CNN
F 1 "GND" H 4900 5750 50  0000 C CNN
F 2 "" H 4900 5900 50  0001 C CNN
F 3 "" H 4900 5900 50  0001 C CNN
	1    4900 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 5500 4500 5500
Wire Wire Line
	3950 5500 4100 5500
Wire Wire Line
	4900 5700 4900 5750
Wire Wire Line
	4900 5100 4950 5100
Wire Wire Line
	4900 5100 4900 5300
$Comp
L Device:R R13
U 1 1 5D739325
P 4650 5750
F 0 "R13" V 4730 5750 50  0000 C CNN
F 1 "10k" V 4650 5750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4580 5750 50  0001 C CNN
F 3 "" H 4650 5750 50  0001 C CNN
	1    4650 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 5750 4900 5750
Connection ~ 4900 5750
Wire Wire Line
	4900 5750 4900 5900
Wire Wire Line
	4500 5750 4500 5500
Connection ~ 4500 5500
Wire Wire Line
	4500 5500 4600 5500
Wire Wire Line
	4700 5000 4950 5000
Text GLabel 6750 5000 2    39   Input ~ 0
UvLed1a
Text GLabel 5750 5500 0    39   Input ~ 0
PinLed1
Wire Notes Line
	5400 6150 5400 3900
Wire Notes Line
	5400 3900 7200 3900
Wire Notes Line
	7200 3900 7200 6150
Wire Notes Line
	7200 6150 5400 6150
$Comp
L shield-rescue:IRF540N-transistors Q3
U 1 1 5D751B5F
P 6600 5500
F 0 "Q3" H 6850 5575 50  0000 L CNN
F 1 "IRF540N" H 6850 5500 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 6850 5425 50  0001 L CIN
F 3 "" H 6600 5500 50  0001 L CNN
	1    6600 5500
	1    0    0    -1  
$EndComp
Text GLabel 6500 5000 0    60   Input ~ 0
12v
$Comp
L Device:R R2
U 1 1 5D751B66
P 6050 5500
F 0 "R2" V 6130 5500 50  0000 C CNN
F 1 "10" V 6050 5500 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5980 5500 50  0001 C CNN
F 3 "" H 6050 5500 50  0001 C CNN
	1    6050 5500
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5D751B6D
P 6700 5900
F 0 "#PWR02" H 6700 5650 50  0001 C CNN
F 1 "GND" H 6700 5750 50  0000 C CNN
F 2 "" H 6700 5900 50  0001 C CNN
F 3 "" H 6700 5900 50  0001 C CNN
	1    6700 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5500 6300 5500
Wire Wire Line
	5750 5500 5900 5500
Wire Wire Line
	6700 5700 6700 5750
Wire Wire Line
	6700 5100 6750 5100
Wire Wire Line
	6700 5100 6700 5300
$Comp
L Device:R R10
U 1 1 5D751B7A
P 6450 5750
F 0 "R10" V 6530 5750 50  0000 C CNN
F 1 "10k" V 6450 5750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6380 5750 50  0001 C CNN
F 3 "" H 6450 5750 50  0001 C CNN
	1    6450 5750
	0    1    1    0   
$EndComp
Wire Wire Line
	6600 5750 6700 5750
Connection ~ 6700 5750
Wire Wire Line
	6700 5750 6700 5900
Wire Wire Line
	6300 5750 6300 5500
Connection ~ 6300 5500
Wire Wire Line
	6300 5500 6400 5500
Wire Wire Line
	6500 5000 6750 5000
Wire Notes Line
	3500 6150 3500 3900
Wire Notes Line
	3500 3900 5300 3900
Wire Notes Line
	5300 3900 5300 6150
Wire Notes Line
	5300 6150 3500 6150
Wire Notes Line
	11150 3950 11150 6200
Text GLabel 3950 5500 0    39   Input ~ 0
PinLed0
Text GLabel 4950 5000 2    39   Input ~ 0
UvLed0a
Wire Wire Line
	2200 4700 2350 4700
NoConn ~ 1150 4600
NoConn ~ 1150 4700
Wire Wire Line
	2150 5000 2550 5000
$Comp
L shield-rescue:Jack-DC-conn J5
U 1 1 5D8483B5
P 1550 900
F 0 "J5" H 1628 1225 50  0000 C CNN
F 1 "Jack-DC" H 1628 1134 50  0000 C CNN
F 2 "Connectors:BARREL_JACK" H 1600 860 50  0001 C CNN
F 3 "~" H 1600 860 50  0001 C CNN
	1    1550 900 
	1    0    0    -1  
$EndComp
Text GLabel 1900 1400 2    39   Input ~ 0
12v
Wire Wire Line
	1800 1400 1900 1400
Wire Wire Line
	1800 1500 1900 1500
Wire Wire Line
	1900 1500 1900 1600
$Comp
L power:GND #PWR06
U 1 1 5D8AE86C
P 1900 1600
F 0 "#PWR06" H 1900 1350 50  0001 C CNN
F 1 "GND" H 1900 1450 50  0000 C CNN
F 2 "" H 1900 1600 50  0001 C CNN
F 3 "" H 1900 1600 50  0001 C CNN
	1    1900 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4250 2400 4250
NoConn ~ 2150 4200
NoConn ~ 2150 4400
Text GLabel 1900 2600 2    39   Input ~ 0
uCgnd
Wire Wire Line
	1800 2600 1900 2600
Wire Wire Line
	1400 2500 1400 2650
Wire Wire Line
	1800 2500 1800 2600
Wire Wire Line
	8950 1100 8950 1650
Wire Wire Line
	2150 4750 2300 4750
Wire Wire Line
	2300 4750 2300 4800
Wire Wire Line
	2300 4800 2350 4800
NoConn ~ 2200 4800
Text GLabel 1000 6200 0    39   Input ~ 0
uCgnd
Text GLabel 2250 6250 2    39   Input ~ 0
uCgnd
Wire Wire Line
	2150 6250 2250 6250
Wire Wire Line
	1000 6200 1100 6200
Wire Wire Line
	1850 1000 1950 1000
Wire Wire Line
	2050 1000 2050 1050
Wire Wire Line
	1950 900  1950 1000
Connection ~ 1950 1000
Wire Wire Line
	1950 1000 2050 1000
$EndSCHEMATC
