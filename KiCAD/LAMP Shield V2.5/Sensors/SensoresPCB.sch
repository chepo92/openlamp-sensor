EESchema Schematic File Version 4
LIBS:SensoresPCB-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L conn:Conn_01x07 J3
U 1 1 5BE49349
P 5250 1000
F 0 "J3" H 5250 1200 50  0000 C CNN
F 1 "Conn_01x07" H 5250 700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 5250 1000 50  0001 C CNN
F 3 "" H 5250 1000 50  0001 C CNN
	1    5250 1000
	1    0    0    1   
$EndComp
$Comp
L conn:Conn_01x02 Therm1
U 1 1 5BE4967B
P 5250 1750
F 0 "Therm1" H 5250 1850 50  0000 C CNN
F 1 "Conn_01x02" H 5250 1550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5250 1750 50  0001 C CNN
F 3 "" H 5250 1750 50  0001 C CNN
	1    5250 1750
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Heater1
U 1 1 5BE49721
P 5250 2200
F 0 "Heater1" H 5250 2300 50  0000 C CNN
F 1 "Conn_01x02" H 5250 2000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5250 2200 50  0001 C CNN
F 3 "" H 5250 2200 50  0001 C CNN
	1    5250 2200
	1    0    0    -1  
$EndComp
$Comp
L conn:Conn_01x02 Led1
U 1 1 5BE498DC
P 5250 2750
F 0 "Led1" H 5250 2850 50  0000 C CNN
F 1 "Conn_01x02" H 5250 2550 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5250 2750 50  0001 C CNN
F 3 "" H 5250 2750 50  0001 C CNN
	1    5250 2750
	1    0    0    -1  
$EndComp
$Comp
L transistors:IRF540N Q1
U 1 1 5BE4A98D
P 5450 3950
F 0 "Q1" H 5700 4025 50  0000 L CNN
F 1 "IRF540N" H 5700 3950 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220-3_Vertical" H 5700 3875 50  0001 L CIN
F 3 "" H 5450 3950 50  0001 L CNN
	1    5450 3950
	1    0    0    -1  
$EndComp
Text GLabel 5350 3400 0    60   Input ~ 0
12v
$Comp
L Device:R R1
U 1 1 5BE4AB8C
P 4900 3950
F 0 "R1" V 4980 3950 50  0000 C CNN
F 1 "10" V 4900 3950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4830 3950 50  0001 C CNN
F 3 "" H 4900 3950 50  0001 C CNN
	1    4900 3950
	0    1    1    0   
$EndComp
Text GLabel 8950 2450 2    60   Input ~ 0
5v
Text GLabel 4600 3950 0    60   Input ~ 0
pwm0
$Comp
L power:GND #PWR03
U 1 1 5BE4B11D
P 5550 4350
F 0 "#PWR03" H 5550 4100 50  0001 C CNN
F 1 "GND" H 5550 4200 50  0000 C CNN
F 2 "" H 5550 4350 50  0001 C CNN
F 3 "" H 5550 4350 50  0001 C CNN
	1    5550 4350
	1    0    0    -1  
$EndComp
Text GLabel 8650 3200 2    60   Input ~ 0
12v
Text GLabel 4800 900  0    39   Input ~ 0
sda
Text GLabel 4800 1000 0    39   Input ~ 0
scl
Text GLabel 4800 1200 0    39   Input ~ 0
3v3
Text GLabel 6000 1000 0    39   Input ~ 0
Tx
Text GLabel 6000 900  0    39   Input ~ 0
Rx
Text GLabel 6000 700  0    39   Input ~ 0
Led
Text GLabel 4850 1750 0    39   Input ~ 0
T0a
Text GLabel 4850 2200 0    39   Input ~ 0
Ha
Text GLabel 4850 2300 0    39   Input ~ 0
Hb
Text GLabel 4900 2750 0    39   Input ~ 0
UvLedA
Text GLabel 4900 2850 0    39   Input ~ 0
UvLedB
Text GLabel 4850 1850 0    39   Input ~ 0
T0b
Text GLabel 9150 900  2    39   Input ~ 0
scl
Text GLabel 9150 1000 2    39   Input ~ 0
sda
Text GLabel 10250 1400 2    39   Input ~ 0
T0a
Text GLabel 10250 1500 2    39   Input ~ 0
T0b
Text GLabel 6000 3450 2    39   Input ~ 0
Ha
Text GLabel 6000 3550 2    39   Input ~ 0
Hb
Text GLabel 10000 1950 2    39   Input ~ 0
UvLedA
Text GLabel 9500 2050 2    60   Input ~ 0
pwm0
Text GLabel 9800 850  0    60   Input ~ 0
5v
$Comp
L Device:R R3
U 1 1 5BE4D45D
P 10050 1100
F 0 "R3" V 10130 1100 50  0000 C CNN
F 1 "100k" V 10050 1100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9980 1100 50  0001 C CNN
F 3 "" H 10050 1100 50  0001 C CNN
	1    10050 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BE4D7B2
P 9150 1950
F 0 "R2" V 9230 1950 50  0000 C CNN
F 1 "560" V 9150 1950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9080 1950 50  0001 C CNN
F 3 "" H 9150 1950 50  0001 C CNN
	1    9150 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 3400 5800 3400
Wire Wire Line
	5050 3950 5150 3950
Wire Wire Line
	4600 3950 4750 3950
Wire Wire Line
	5550 4150 5550 4200
Wire Wire Line
	8500 3200 8650 3200
Wire Wire Line
	8500 2450 8950 2450
Wire Wire Line
	4850 1750 5050 1750
Wire Wire Line
	4850 1850 5050 1850
Wire Wire Line
	4850 2200 5050 2200
Wire Wire Line
	4850 2300 5050 2300
Wire Wire Line
	4900 2750 5050 2750
Wire Wire Line
	4900 2850 5050 2850
Wire Wire Line
	8500 900  9150 900 
Wire Wire Line
	8500 1000 9150 1000
Wire Wire Line
	6000 3450 5800 3450
Wire Wire Line
	5550 3550 6000 3550
Wire Wire Line
	5800 3450 5800 3400
Wire Wire Line
	5550 3550 5550 3750
Wire Wire Line
	9800 850  10050 850 
Wire Wire Line
	10050 850  10050 950 
Wire Wire Line
	10050 1400 10050 1250
Wire Wire Line
	10050 1400 10250 1400
$Comp
L Device:R R4
U 1 1 5C30FE8A
P 5300 4200
F 0 "R4" V 5380 4200 50  0000 C CNN
F 1 "10k" V 5300 4200 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5230 4200 50  0001 C CNN
F 3 "" H 5300 4200 50  0001 C CNN
	1    5300 4200
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 4200 5550 4200
Connection ~ 5550 4200
Wire Wire Line
	5550 4200 5550 4350
Wire Wire Line
	5150 4200 5150 3950
Connection ~ 5150 3950
Wire Wire Line
	5150 3950 5250 3950
$Comp
L conn:Conn_01x02 J4
U 1 1 5C36E2C1
P 8300 1500
F 0 "J4" H 8300 1600 50  0000 C CNN
F 1 "Conn_01x02" H 8300 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8300 1500 50  0001 C CNN
F 3 "" H 8300 1500 50  0001 C CNN
	1    8300 1500
	-1   0    0    1   
$EndComp
Wire Wire Line
	8500 1400 10050 1400
Connection ~ 10050 1400
Wire Wire Line
	8500 1500 10250 1500
$Comp
L Device:CP C1
U 1 1 5C3740C4
P 2550 2200
F 0 "C1" H 2668 2246 50  0000 L CNN
F 1 "CP" H 2668 2155 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P3.80mm" H 2588 2050 50  0001 C CNN
F 3 "~" H 2550 2200 50  0001 C CNN
	1    2550 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5C37439A
P 2950 2200
F 0 "C2" H 3068 2246 50  0000 L CNN
F 1 "CP" H 3068 2155 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D8.0mm_P3.80mm" H 2988 2050 50  0001 C CNN
F 3 "~" H 2950 2200 50  0001 C CNN
	1    2950 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C37446B
P 2550 2500
F 0 "#PWR05" H 2550 2250 50  0001 C CNN
F 1 "GND" H 2550 2350 50  0000 C CNN
F 2 "" H 2550 2500 50  0001 C CNN
F 3 "" H 2550 2500 50  0001 C CNN
	1    2550 2500
	1    0    0    -1  
$EndComp
Text GLabel 2250 1950 0    60   Input ~ 0
12v
Text GLabel 3150 1900 2    60   Input ~ 0
5v
Wire Wire Line
	2250 1950 2550 1950
Wire Wire Line
	2550 1950 2550 2050
Wire Wire Line
	3150 1900 2950 1900
Wire Wire Line
	2950 1900 2950 2050
Wire Wire Line
	2950 2350 2950 2450
Wire Wire Line
	2550 2350 2550 2500
$Comp
L conn:Conn_01x02 J6
U 1 1 5C37D39D
P 8300 2550
F 0 "J6" H 8300 2650 50  0000 C CNN
F 1 "Conn_01x02" H 8300 2350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8300 2550 50  0001 C CNN
F 3 "" H 8300 2550 50  0001 C CNN
	1    8300 2550
	-1   0    0    1   
$EndComp
$Comp
L conn:Conn_01x02 J2
U 1 1 5C383C5C
P 8300 3300
F 0 "J2" H 8300 3400 50  0000 C CNN
F 1 "Conn_01x02" H 8300 3100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8300 3300 50  0001 C CNN
F 3 "" H 8300 3300 50  0001 C CNN
	1    8300 3300
	-1   0    0    1   
$EndComp
Text GLabel 10300 2700 2    39   Input ~ 0
T0b
Wire Wire Line
	8500 1950 9000 1950
Wire Wire Line
	9300 1950 10000 1950
Text GLabel 6000 1300 0    39   Input ~ 0
uCgnd
Text GLabel 9150 2550 2    39   Input ~ 0
uCgnd
Text GLabel 10300 2550 2    39   Input ~ 0
UvLedB
Text GLabel 10300 2400 2    39   Input ~ 0
uCgnd
Wire Wire Line
	8500 2550 9150 2550
Wire Wire Line
	10300 2400 10300 2700
Text GLabel 3050 2450 2    39   Input ~ 0
uCgnd
Wire Wire Line
	2950 2450 3050 2450
$Comp
L power:GND #PWR0101
U 1 1 5C3AA143
P 6100 4300
F 0 "#PWR0101" H 6100 4050 50  0001 C CNN
F 1 "GND" H 6100 4150 50  0000 C CNN
F 2 "" H 6100 4300 50  0001 C CNN
F 3 "" H 6100 4300 50  0001 C CNN
	1    6100 4300
	1    0    0    -1  
$EndComp
Text GLabel 6250 4200 2    39   Input ~ 0
uCgnd
Wire Wire Line
	6100 4300 6100 4200
Wire Wire Line
	6100 4200 6250 4200
$Comp
L power:GND #PWR01
U 1 1 5C4C0CC6
P 8700 3450
F 0 "#PWR01" H 8700 3200 50  0001 C CNN
F 1 "GND" H 8700 3300 50  0000 C CNN
F 2 "" H 8700 3450 50  0001 C CNN
F 3 "" H 8700 3450 50  0001 C CNN
	1    8700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 3300 8700 3300
Wire Wire Line
	8700 3300 8700 3450
$Comp
L conn:Conn_01x02 J8
U 1 1 5C4C3C94
P 8300 2050
F 0 "J8" H 8300 2150 50  0000 C CNN
F 1 "Conn_01x02" H 8300 1850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8300 2050 50  0001 C CNN
F 3 "" H 8300 2050 50  0001 C CNN
	1    8300 2050
	-1   0    0    1   
$EndComp
$Comp
L conn:Conn_01x02 J7
U 1 1 5C4C6976
P 8300 1000
F 0 "J7" H 8300 1100 50  0000 C CNN
F 1 "Conn_01x02" H 8300 800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 8300 1000 50  0001 C CNN
F 3 "" H 8300 1000 50  0001 C CNN
	1    8300 1000
	-1   0    0    1   
$EndComp
$Comp
L conn:Conn_01x07 J5
U 1 1 5C4C78DA
P 6350 1000
F 0 "J5" H 6350 1200 50  0000 C CNN
F 1 "Conn_01x07" H 6350 700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x07_Pitch2.54mm" H 6350 1000 50  0001 C CNN
F 3 "" H 6350 1000 50  0001 C CNN
	1    6350 1000
	1    0    0    1   
$EndComp
Text GLabel 4800 1300 0    39   Input ~ 0
5v
Text GLabel 4800 1100 0    39   Input ~ 0
uCgnd
Text GLabel 4800 800  0    39   Input ~ 0
rst
Text GLabel 4800 700  0    39   Input ~ 0
int
Text GLabel 6000 1100 0    39   Input ~ 0
5vnc
Text GLabel 6000 800  0    39   Input ~ 0
Nc
Text GLabel 6000 1200 0    39   Input ~ 0
Nc
Wire Wire Line
	6000 700  6150 700 
Wire Wire Line
	6000 800  6150 800 
Wire Wire Line
	6000 900  6150 900 
Wire Wire Line
	6000 1000 6150 1000
Wire Wire Line
	6000 1100 6150 1100
Wire Wire Line
	6000 1200 6150 1200
Wire Wire Line
	6000 1300 6150 1300
Wire Wire Line
	4800 700  5050 700 
Wire Wire Line
	4800 800  5050 800 
Wire Wire Line
	5050 900  4800 900 
Wire Wire Line
	4800 1000 5050 1000
Wire Wire Line
	4800 1100 5050 1100
Wire Wire Line
	4800 1200 5050 1200
Wire Wire Line
	4800 1300 5050 1300
Wire Wire Line
	8500 2050 9500 2050
$EndSCHEMATC
