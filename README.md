# OpenLAMP Sensor

System to perform LAMP isothermal amplification reactions in 0.2 ml PCR tubes, with digital control, measurements and data logging.

See more info, build instructions and usage in the [Project Page](https://chepo92.gitlab.io/openlamp-sensor/)

<img src="Photos/V4/View Window.jpg" alt="drawing" width="200"/>
