// Temperature conversion constants       
// resistance at 25 degrees C
#define THERMISTORNOMINAL 100000      
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25   
// how many samples to take and average, more takes longer
// but is more 'smooth'
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 3950
// the value of the 'other' resistor
#define SERIESRESISTOR 100000    

//Low pass filter for temperature reaings 
double filterVal = 0.2;       // this determines smoothness  0001 is off   1 is max

//temperature sampling period in ms
unsigned long tempSamplePeriod = 10;  // 
unsigned long rgbSamplePeriod = 5000;  // 60  sec

//debounce time for buton
unsigned long debounceDelay = 100;    // the debounce time; increase if the output flickers

unsigned long calirationDuration=60000;  // 60  sec
unsigned long heatingMaxDuration=300000; // 5 min 5*60*1000 sec
unsigned long experimentDuration=600000; // 10 min 10*60*1000 sec
unsigned long thermalRunawayMaxTime = 60000 ;   // 60 seconds out of temp

// PID tuning parameters
double Kp=8.00, Ki=0.8, Kd=0.01; // 2 .02 .00  // 10 0.5 0.01 
double Kp1=8.00, Ki1=0.8, Kd1=0.01; // 2 .02 .00
