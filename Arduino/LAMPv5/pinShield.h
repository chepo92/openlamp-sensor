
// pin Thermistors
#define THERMISTORPIN0 62    // A8 
#define THERMISTORPIN1 64    // A10

// pin softwire color sensor0
#define softWireSDA0  15   
#define softWireSCL0  14

// pin softwire color sensor1
#define softWireSDA1  17  
#define softWireSCL1  16  

// pin softwire LCD
#define SDA2  SDA  
#define SCL2  SCL

#define chipSelect  53

// Leds Pins
#define led0 3      // the number of the LED pin
#define led1 2      // the number of the LED pin

// Button and sate led Pins
#define buttonPin   24    // the number of the pushbutton pin
#define ledWPin     22     // the number of the LED pin

// Heater pwm outputs
#define heater0Pin 11  //
#define heater1Pin 9  // 
