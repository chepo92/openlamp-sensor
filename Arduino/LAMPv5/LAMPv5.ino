 /*
  * for serial commands use serial monitor without newline or carriage return
  * 
  * Sources:
  * 
  * 
  * 
  * 
  * 
  * Programmer: Axel Sepulveda
 */
 


/*
 * Include software I2C libraries
 */

#include <Wire.h>
#include <SoftWire.h>     // https://github.com/stevemarple/SoftWire
#include <AsyncDelay.h>             // https://github.com/stevemarple/AsyncDelay
#include "pinShield.h"
#include "parameters.h"

// SD Card libraries
#include <SPI.h>
#include <SD.h>

// PID temp controller
#include <PID_v1.h>         //https://github.com/br3ttb/Arduino-PID-Library/

#include <LiquidCrystal_I2CSoft.h>

//Choose the pins, order: (SDA,SCL)
SoftWire sw0(softWireSDA0,softWireSCL0);  
SoftWire sw1(softWireSDA1,softWireSCL1);  
SoftWire sw2(SDA2,SCL2);

// SoftWire requires that the programmer declares the buffers used. This allows the amount of memory used to be set
// according to need.
uint8_t i2cRxBuffer0[32];
uint8_t i2cTxBuffer0[32];

uint8_t i2cRxBuffer1[32];
uint8_t i2cTxBuffer1[32];
  
uint8_t i2cRxBuffer2[32];
uint8_t i2cTxBuffer2[32];


LiquidCrystal_I2CSoft lcd(0x3F,16,2);  // set the LCD address to 0x3F for a 16 chars and 2 line display

/*
 * Include light sensor libraries and Create sensor objects
 */
#include "Adafruit_TCS34725Soft.h"    // Included in folder 
Adafruit_TCS34725 ams0 = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_60X);
Adafruit_TCS34725 ams1 = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_60X);

//buffer to hold raw values
const uint8_t NUM_CHANNELS = 4;
uint16_t sensorValues0[NUM_CHANNELS];
uint16_t sensorValues1[NUM_CHANNELS];

//Temp variable reading of  AS726x
uint8_t temp ;

//AsyncDelays library to manage timings
AsyncDelay delay_2s;

// ADC ready byte for AS726x
bool rdy = false;

// Temperature reading variables
uint16_t samples0;
uint16_t samples1; 

//Low pass filter for temperature reaings 
double temp0, temp1;
double smoothed0, smoothed1;

// serial commands Variables
char command; 
int number; 


//  state Variables for led and Button
boolean greenState = HIGH;         // the current state of the output pin
boolean redState = LOW;         // the current state of the output pin

int defaultLedPWM=10;

boolean buttonState= HIGH;        // the current reading from the input pin (default pulled up
boolean lastButtonState = HIGH;   // the previous reading from the input pin


// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lasTempSampleTime ;    // 

unsigned long lastRGBSampleTime ;    // 
boolean measureFlag ; 
boolean measureLedFlag ; 
boolean measureRegister ; 
// Button debouncer variable
unsigned long lastDebounceTime ;  // the last time the button was pushed

// Led blinking variables
unsigned long lastBlink;  // the last time the output led pin was toggled

// Timers
//unsigned long heatingTimer=0;
unsigned long procesStartTime;

//unsigned long heatingTimer=0;
unsigned long thermalRunawayStartTime;

int blinkDelay= -1 ; 

// States Machine
byte mainState=0; //
byte prevState=-1;
//byte numStates = 7; // error state is not counted
#define IDLEE   0
#define CALIBRATE 1
#define HEATING 2
#define HEATED  3
#define CALIBRATE2 4
// agregar segunda calibracion con tubo de calib
#define MEASURE 5
#define ERRORR 6

String states[] = {"IDLE","CALIBRATE","HEATING","HEATED","CALIB 2","MEASURE","ERROR"};
byte errorIndex = 0; 
#define SENSOR      0
#define UNDERTEMP   1 
#define OVERTEMP    2
#define HTIMEOUT    3
#define SDCARD      4

String errors[] = {"SENSOR", "UNDERTEMP", "OVERTEMP" ,"HTIMEOUT","SDCARD"};


//Define PID Variables
double Setpoint0, Input0, Output0;
double Setpoint1, Input1, Output1;



// Threshold for bang bang operation (when temp is this close to setpoint it will start PID)
int threshold = 5; 

// PID object declaration
PID myPID0(&Input0, &Output0, &Setpoint0, Kp, Ki, Kd, DIRECT);
PID myPID1(&Input1, &Output1, &Setpoint1, Kp1, Ki1, Kd1, DIRECT);

// Control Variables
boolean manual  = false; 
boolean enableFD=false;



// Control Variables
boolean SerialSendData = true;
boolean SerialDebug = true; 
boolean OutputEnable = true; 

// String to be stored and sent
String dataString = "millis, temp0, temp1, pwm0, pwm1, fotodiode0, fotodiode1, mainState";


void setup(void) {
  // Set pwm freq, heater pins as outputs and default to off state
    setPwmFrequency(heater0Pin, 1);  
    setPwmFrequency(heater1Pin, 1);  
    pinMode(heater0Pin, OUTPUT);
    pinMode(heater1Pin, OUTPUT);
    digitalWrite(heater0Pin,LOW);
    digitalWrite(heater1Pin,LOW);
    
  //Start Serial comm    
    Serial.begin(250000);
  // Declare input thermistor pins
    pinMode(THERMISTORPIN0, INPUT);
    pinMode(THERMISTORPIN1, INPUT);

  // Define temp setpont for PID      
    Setpoint0 = 65;
    Setpoint1 = 65;
    
    // Set mode and max output limit of PID (under 40 won't melt plastic)
    myPID0.SetMode(MANUAL);
    myPID0.SetOutputLimits(0,40);
  
    myPID0.SetMode(MANUAL);
    myPID1.SetOutputLimits(0,40);

    // Sketch version
    Serial.println("ARDUINO LAMP v5");  
    Serial.print(".");  
    
    delay_2s.start(2000, AsyncDelay::MILLIS);
    Serial.print(".");  
    // Softwire variable assign and start
    sw0.setRxBuffer(i2cRxBuffer0, sizeof(i2cRxBuffer0));
    sw0.setTxBuffer(i2cTxBuffer0, sizeof(i2cTxBuffer0));
    sw0.setTimeout_ms(200);
    Serial.print(".");  
    sw1.setRxBuffer(i2cRxBuffer1, sizeof(i2cRxBuffer1));
    sw1.setTxBuffer(i2cTxBuffer1, sizeof(i2cTxBuffer1));
    sw1.setTimeout_ms(200);
    Serial.print(".");  
    
    sw2.setRxBuffer(i2cRxBuffer2, sizeof(i2cRxBuffer2));
    sw2.setTxBuffer(i2cTxBuffer2, sizeof(i2cTxBuffer2));
    sw2.setTimeout_ms(200);
    sw2.begin();      
    Serial.println(".");            
    Serial.println("STARTING");

    lcd.init(&sw2);                      // initialize the lcd 
    Serial.println("."); 
    lcd.backlight();    
    lcd.setCursor(0,0);      
    lcd.print("Arduino LAMP    ");
    lcd.setCursor(0,1);
    lcd.print("Version 5.0     ");

    Serial.println("LCD OK");
    //begin and make sure we can talk to the light sensors
  if (ams0.begin(&sw0)) {
     Serial.println("Sensor 0 OK");
  } else {
      mainState=ERRORR;
      errorIndex=SENSOR;    
    Serial.println("No TCS34725 (0) found ... check your connections");
  }
  if (ams1.begin(&sw1)) {
     Serial.println("Sensor 1 OK");
  } else {
      mainState=ERRORR;
      errorIndex=SENSOR;    
    Serial.println("No TCS34725 (1) found ... check your connections");
  }  
  
  // Start SD card
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    mainState=ERRORR;
    errorIndex=SDCARD;
  } else {
    Serial.println("SD Card OK");  
  }
  lcd.setCursor(0,0);      
  lcd.print("Arduino LAMP: OK");
  delay(2000);
  //declare input pins 
  pinMode(buttonPin, INPUT_PULLUP);

  //declare output pins 
  pinMode(led0, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(ledWPin, OUTPUT);
  digitalWrite(led0, LOW);
  digitalWrite(led1, LOW);
  digitalWrite(ledWPin, LOW);
  
  lcd.clear();
}

//-------------------------------------------------------------------------------------------------------------------------

void loop(void) {


//--------- read button with debounce
  int reading = digitalRead(buttonPin);
  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }    
  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:      
    // if the button state has changed:    
    if (reading != buttonState) {
      buttonState = reading;           
//     if (SerialDebug) {Serial.println("button change"); }          
      if (buttonState == HIGH) { // only change State if the new button state is HIGH (when button is released) 
        nextState();
        if (SerialDebug) {Serial.println("button Released"); }                      
      }           
    }
  }
  lastButtonState = reading;  // save the reading. Next time through the loop, it'll be the lastButtonState:
  


      
//-------- Read analog temps     
   if ((millis() - lasTempSampleTime ) > tempSamplePeriod) {

   ////-------- Disable outputs for reading
//  digitalWrite(heater0Pin,0);       
//  digitalWrite(heater1Pin,0);  
       lasTempSampleTime = millis();
       samples0 = analogRead(THERMISTORPIN0);
       // delay(50);
       samples1 = analogRead(THERMISTORPIN1);
       //delay(20);
   }
   
//--- Temp Low pass filter

   smoothed0 =  smooth(samples0, filterVal, smoothed0);   // second parameter determines smoothness  - 0 is off,  .9999 is max smooth 
  temp0 = smoothed0;

   smoothed1 =  smooth(samples1, filterVal, smoothed1);   // second parameter determines smoothness  - 0 is off,  .9999 is max smooth   
  temp1 =smoothed1 ;
  
//----  convert the value to resistance
  temp0 = 1023 / temp0 - 1;
  temp0 = SERIESRESISTOR / temp0;
//    Serial.print("Thermistor0 resistance "); 
//    Serial.println(average0);
  
  temp1 = 1023 / temp1 - 1;
  temp1 = SERIESRESISTOR / temp1;
//     Serial.print("Thermistor1 resistance "); 
//     Serial.println(average1);
  
//---- convert to temp   
  temp0=steinhartConvert(temp0)  ;
  temp1=steinhartConvert(temp1)  ;

//---- check if it is time to measure
if (mainState==MEASURE && (millis() - lastRGBSampleTime) > rgbSamplePeriod) {
   measureFlag = true ; 
   lastRGBSampleTime  = millis() ; 
}

//---- turn leds on 1000ms before measurement
if (mainState==MEASURE && (millis()+1000 - lastRGBSampleTime) > rgbSamplePeriod) {
   measureLedFlag = true ; 
}

//-------- Turn LEDs on during calibrate and measurement with flag states
 if ((mainState==MEASURE && measureLedFlag) || mainState==CALIBRATE || mainState==CALIBRATE2) {
      analogWrite(led0,defaultLedPWM);
      analogWrite(led1,defaultLedPWM);
 } else {
      digitalWrite(led0,LOW);
      digitalWrite(led1,LOW);      
 }

//--------   read photodiodes                   
  if ((mainState==MEASURE && measureFlag) || mainState==CALIBRATE || mainState==CALIBRATE2 || enableFD ){
      if (SerialDebug) {Serial.println("measuring... "); }  
      for (int i= 0 ; i< NUM_CHANNELS ; i++ ) {
               sensorValues0[i]=0; 
               sensorValues1[i]=0;
      }
      if (ams0.dataReady() && ams1.dataReady()){
            if (SerialDebug) {Serial.println("Ready 0!"); }  
            ams0.getRawData(&sensorValues0[0], &sensorValues0[1], &sensorValues0[2],&sensorValues0[3]);
      } else {
            if (SerialDebug) {Serial.println("Waiting ADC"); }  
      }
      if (ams1.dataReady()){
            if (SerialDebug) {Serial.println("Ready 1! "); }  
//            ams0.getRGB (&red, &gre, &blu);
            ams1.getRawData(&sensorValues1[0], &sensorValues1[1], &sensorValues1[2],&sensorValues1[3]);
      } else {
            if (SerialDebug) {Serial.println("Waiting ADC 1! "); }  
      }       
                     
  } else {
    
    for (int i= 0 ; i< NUM_CHANNELS ; i++ ) {
             sensorValues0[i]=0; 
             sensorValues1[i]=0;
    }
  }

//-------- Turn LEDs off after measuring and set read sensor flag low, flag to turn off leds, and register measure state to SD variable
 if ((mainState==MEASURE && measureFlag)) {
      digitalWrite(led0,LOW);
      digitalWrite(led1,LOW);    
      measureFlag = false;
      measureLedFlag = false; 
      measureRegister = true ; 
 }  else {
      measureRegister = false; 
 }
 
  
//--------  PID Input 
  Input0 = temp0; 
  Input1 = temp1;  
  
//--------  If in heating, heated or measure state, choose between bang bang or pid depeding on temp. difference threshold
  if ((mainState==HEATING || mainState==HEATED || mainState==CALIBRATE2 || mainState==MEASURE)&& !manual ){
    
    if (SerialDebug) {Serial.println("heating..."); }
    //Activate PID if in range
    if((Setpoint0-Input0 > threshold)){
        if (SerialDebug) {Serial.println("bang 0..."); }
        myPID0.SetMode(MANUAL);
        Output0=40; 
    } else if(Setpoint0-Input0 <= threshold && myPID0.GetMode()==MANUAL){
        if (SerialDebug) {Serial.println("pid..."); }
        Output0=0; 
        myPID0.SetMode(AUTOMATIC);
              
    }
  
    if((Setpoint1-Input1 > threshold)){
        if (SerialDebug) {Serial.println("bang 1..."); }
        Output1=40; 
        myPID1.SetMode(MANUAL);
    } else if(Setpoint1-Input1 <= threshold && myPID1.GetMode()==MANUAL){
        if (SerialDebug) {Serial.println("pid..."); }
        Output1=00;  
        myPID1.SetMode(AUTOMATIC);
             
    }              
  }
   
//------- Compute PIDs
  myPID0.Compute();
  myPID1.Compute();

//--------  Apply Output heater pwm
  if (OutputEnable) {
    analogWrite(heater0Pin,Output0);       
    analogWrite(heater1Pin,Output1);      
  } else {
    digitalWrite(heater0Pin,0);       
    digitalWrite(heater1Pin,0);      
  }       

//-------- thermal runaway protection 
   thermalRunaway ();

//--------turn heaters off when the experiment ends after the experimentDuration time
  if (mainState==MEASURE && ((millis()- procesStartTime) > experimentDuration) ) {
     shutDown (0);
     shutDown (1);
     mainState=IDLEE;
     stopProcessTimer() ; 
     if (SerialDebug) {Serial.println("Ended!");}    
  }

//-------- change of state when heating is complete to heated STATE
  if (mainState==HEATING && doneHeating(0) && doneHeating(1)) {
     nextState();
     if (SerialDebug) {Serial.println("Done Heating!"); }  
  }

//--------     If in Idle or Calibrate state and PID are on and manual control is off, then turn PID off
  if ((mainState==IDLEE || mainState==CALIBRATE ) && !manual && (statusPIDs() || Output0!=0 || Output1!=0)) {  
      if (SerialDebug) {Serial.println("apagando");}     
          shutDown (0);
          shutDown (1);
  }   
          
//--------   Serial commands Input 
  // only when you receive data:
  if (Serial.available()) {
    // read the incoming byte:
    command = Serial.read();
    number = Serial.parseInt();
                     
    // say what you got:               
    Serial.print("Recibido: ");  
    Serial.print(command);
    Serial.print(" ");  
    Serial.println(number);

    if (command=='M')   {     // Both PID in manual mode
          manual=true;
          myPID0.SetMode(MANUAL);
          Output0=0;
          myPID1.SetMode(MANUAL);
          Output1=0;                
          Serial.println("Apagado");
    } else
    if (command=='A')   {  //both PID in auto mode
          manual=false;
          myPID0.SetMode(AUTOMATIC);
          Output0=0;
          myPID1.SetMode(AUTOMATIC);
          Output1=0;                
          Serial.println("Auto");
    } else
    if (command=='a')   {   //Set heater0Pin pwm
          number = constrain(number,0,255);  
          Output0=number;
    }
    if (command=='b')   {   //Set heater1Pin pwm
          number = constrain(number,0,255);  
          Output1=number;                
    } else      
    //P,I, D: Set gains, params should be passed multiplied by x100 (easier to parse the message) 
    if (command=='P')   {  
          Kp=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    }else
    if (command=='I')   {
          Ki=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    }else
    if (command=='D')   {
          Kd=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    } else           
    if (command=='?')   {  // show acgtual gains 
        Serial.print(Kp);
        Serial.print(",");     
        Serial.print(Ki);
        Serial.print(",");     
        Serial.print(Kd);
        Serial.println("");     
    } else           
    if (command=='E')   {   //enable photodiode readings
        enableFD=!enableFD;
    } else           
    if (command=='L')   {   //Change pid output limit, use with care as too much output can melt the plastic
        number = constrain(number,0,255);              
        myPID0.SetOutputLimits(0,number);
        myPID1.SetOutputLimits(0,number);
    } else           
    if (command=='S')   {  //change setpoint temperature
        number = constrain(number,0,100);   
        Setpoint0 = number;
        Setpoint1 = number;
    } else           
    if (command=='J')   {  // SerialDebug messages
        SerialDebug=!SerialDebug;
        
    } else           
    if (command=='V')   {  // SerialSendData messages
        SerialSendData=!SerialSendData;
    } else           
    if (command=='O')   {  // Enable outputs pwm. 
        OutputEnable=!OutputEnable;
    }          
                          
  }
      
          
//--------   Assemble the data string to be saved
// Formato: millis, temp0, temp1, pwm0, pwm1, rgbc0, rgbc1, mainState 
    
  dataString = String(millis());
  dataString += ",";
  
  dataString += String(temp0);
  dataString += ",";
  dataString += String(temp1);
  dataString += ",";            

  dataString += String(Output0);
  dataString += ",";
  dataString += String(Output1);
  dataString += ",";            
  for (int i= 0 ; i< NUM_CHANNELS ; i++ ) {
      dataString += String(sensorValues0[i]);
      dataString += ",";
  }
  for (int i= 0 ; i< NUM_CHANNELS ; i++ ) {
      dataString += String(sensorValues1[i]);
      dataString += ",";
  }
           
  dataString += String(mainState);
  dataString += ",";            
  dataString += String(measureRegister);


//--------   Send the data string over serial if it's enabled 
  if (SerialSendData) {Serial.println(dataString); }

//--------  if not in idle mode, writes datastring in SD, 
  if (mainState!=IDLEE || mainState!=ERRORR){
      File dataFile = SD.open("datalog.txt", FILE_WRITE);
      // if the file is available, write to it:
      if (dataFile) {
        dataFile.println(dataString);
        dataFile.close();
      }
      // if the file isn't open, pop up an error:
      else {
        if (SerialDebug) {Serial.println("error opening datalog.txt");}
      } 
  }

//-------- Displays in the LCD
      lcdControl(); 

//-------- State LED blinking
  blinkLED(mainState);
      

//--------- records the actual state for the new loop
      prevState=mainState;
  
}


//-------- Function to convert thermistor reading into temps 
double steinhartConvert(double resistance)   {
  float steinhart;
  steinhart = resistance / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C
  return steinhart; 
}

//-------- Thermal runaway protection
void thermalRunaway (){
  //Check over- under temps
  if (temp0>100 || temp1>100){
      shutDown (0);
      shutDown (1);      
      mainState=ERRORR;
      errorIndex=OVERTEMP;
      if (SerialDebug) {Serial.println("OverTemp");}
  }
  //Check under temps
  if (temp0< -10 || temp0< -10){
      shutDown (0);
      shutDown (1);
      mainState=ERRORR;
      errorIndex=UNDERTEMP;
      if (SerialDebug) {Serial.println("UnderTemp");}
  }
  //heating has taken too long
  if ((mainState==HEATING) && (!doneHeating(1) || !doneHeating(0) ) &&  ((millis()- procesStartTime) > heatingMaxDuration) ){
      shutDown (0);
      shutDown (1);      
      mainState=ERRORR;
      errorIndex=HTIMEOUT;
      if (SerialDebug) {Serial.println("Heating Timeout");}
  }

  if ((mainState==HEATED || mainState==CALIBRATE2 || mainState==MEASURE)  ){
    if (!doneHeating(1) || !doneHeating(0)){
      if (thermalRunawayStartTime == 0 ){
          thermalRunawayStartTime = millis(); 
      } else if (millis() - thermalRunawayStartTime > thermalRunawayMaxTime ) {
          shutDown (0);
          shutDown (1);      
          mainState=ERRORR;
          errorIndex=HTIMEOUT;
          if (SerialDebug) {Serial.println("Heating Runaway");}        
      }      
    } else {
      thermalRunawayStartTime = 0; 
    }

      

  }
}

// shut all down PID's in manual, outputs in 0 and Stop timers
void shutDown (int index){
  stopProcessTimer();
  if (index==0){
      myPID0.SetMode(MANUAL);
      Output0=0;
      if (SerialDebug) { Serial.println("Apagado: 0");}
  } else if (index==1){
      myPID1.SetMode(MANUAL);
      Output1=0;
      if (SerialDebug) {Serial.println("Apagado: 1");      }
  }           
}

//-------- PIDS in Auto 
void startPIDs(){
      myPID0.SetMode(AUTOMATIC);
      myPID1.SetMode(AUTOMATIC);
}

////-------- Start heating timers
//void startHeatingTimer(){
//   heatingTimer=millis();
//}

////-------- Checks timers started
//boolean areTimersStarted(){
//  return heatingTimer!=0;
//}

//-------- Starts duration timer
void reStartProcessTimer(){
  procesStartTime=millis();
}

void stopProcessTimer(){
  procesStartTime=0;
}

//-------- Checks duration timer is started
boolean isProcessTimerStarted(){
  return procesStartTime!=0 ;
}


//-------- Checks pid status 
boolean statusPIDs(){
  return (myPID0.GetMode()==AUTOMATIC || myPID1.GetMode()==AUTOMATIC);
}

//-------- Checks if heating is complete
boolean doneHeating(int heater){
  if (heater==0){
      return abs(Input0-Setpoint0)<1;
  } else if (heater==1){
      return abs(Input1-Setpoint1)<1;
  } 
  return false;
  
}

//-------- Digital Low pass filter
int smooth(int data, float filterVal, float smoothedVal){
  if (filterVal > 1){      // check to make sure param's are within range
    filterVal = .99;
  }
  else if (filterVal <= 0){
    filterVal = 0;
  }
  smoothedVal = (data * (1 - filterVal)) + (smoothedVal  *  filterVal);
  return (int)smoothedVal;
}

//-------- Led blinking freq depending on main loop state 
void blinkLED(int mainState) {
  switch (mainState) {
      case IDLEE:
        blinkDelay=-1;
        // statements
        break;
      case HEATING:
        blinkDelay=100;
        // statements
        break;
      case HEATED:
        blinkDelay=0;
        // statements
        break;
      case MEASURE:
        blinkDelay=1000;
        // statements
        break;
      case CALIBRATE:
        blinkDelay=1000;
        // statements
        break;   
      case ERRORR:
        blinkDelay=-1;// statements
        break;             
      default:
        blinkDelay=-1;
        // statements
        break;
  }

  if(millis()-lastBlink>blinkDelay && blinkDelay>0){
      redState=!redState;
      digitalWrite(ledWPin,redState);
      lastBlink=millis();
  } else if(blinkDelay==0){
     redState=true;
     digitalWrite(ledWPin,redState);
  } else if(blinkDelay==-1){
     redState=false;
     digitalWrite(ledWPin,redState);
  } 
  
}


void nextState(){
    mainState++;
    reStartProcessTimer();
    if (mainState>MEASURE){
       mainState=0;
    }  
}

//-------------------- Controls the LCD Display
void lcdControl(){
  static boolean displayTime=false;
  unsigned long elapsedProcessTime= millis()- procesStartTime;
  unsigned int minutes = elapsedProcessTime / 60000;
  unsigned int seconds = (elapsedProcessTime / (1000)) % 60;
//  Serial.println(elapsedProcessTime);
//  Serial.println(minutes);

  
  if (delay_2s.isExpired()) {
      lcd.clear();
      if(!displayTime ){   
        lcd.setCursor(0,0);      
        lcd.print("State: "+ states[mainState]);
        if (mainState!=IDLEE && mainState!=IDLEE){ displayTime = true; }        
      } else if (displayTime) {
        lcd.setCursor(0,0);
        if (mainState!=IDLEE){            
            lcd.print("Elapsed: "+ String(minutes) + ":" + String(seconds) );      
        }
        displayTime=false;
      }
      lcd.setCursor(0,1);
      lcd.print("0:"+ String(temp0) + " 1: " + String(temp1));    
      //lcd.scrollDisplayLeft();     
      delay_2s.repeat(); // Count from when the delay expired, not now    
  }
}


//-------- Sets pwm freq to increase efficiency 
void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }

}
  /*

Pins 5 and 6:  controlled by timer0 8bit
Setting  Divisor Frequency
0x01  1 62500
0x02  8 7812.5
0x03  64  976.5625
0x04  256 244.140625
0x05  1024  61.03515625

TCCR0B = TCCR0B & 0b11111000 | <setting>;


Default: delay(1000) or 1000 millis() ~ 1 second

0x01: delay(64000) or 64000 millis() ~ 1 second
0x02: delay(8000) or 8000 millis() ~ 1 second
0x03: is the default
0x04: delay(250) or 250 millis() ~ 1 second
0x05: delay(62) or 62 millis() ~ 1 second
(Or 63 if you need to round up.  The number is actually 62.5)

Also, the default settings for the other timers are:
TCCR1B: 0x03
TCCR2B: 0x04


Pins 9 and 10: controlled by timer1 16Bit
Setting Divisor Frequency
0x01  1 31250
0x02  8 3906.25
0x03  64  488.28125
0x04  256 122.0703125
0x05  1024  30.517578125

TCCR1B = TCCR1B & 0b11111000 | <setting>;


Pins 11 and 3:  controlled by timer2 8 Bit
Setting Divisor Frequency
0x01  1 31250
0x02  8 3906.25
0x03  32  976.5625
0x04  64  488.28125
0x05  128 244.140625
0x06  256 122.0703125
0x07  1024  30.517578125


http://greenmeetsblue.blogspot.cl/2011/10/arduino-development-challenges.html
*/
