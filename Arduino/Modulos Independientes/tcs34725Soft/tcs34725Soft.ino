#include <Wire.h>

#include <SoftWire.h>     // https://github.com/stevemarple/SoftWire
#include <AsyncDelay.h>             // https://github.com/stevemarple/AsyncDelay
//Choose the pins, order: (SDA,SCL)
SoftWire sw0(15,14);  
uint8_t i2cRxBuffer0[32];
uint8_t i2cTxBuffer0[32];

#include "Adafruit_TCS34725Soft.h"

/* Example code for the Adafruit TCS34725 breakout library */

/* Connect SCL    to analog 5
   Connect SDA    to analog 4
   Connect VDD    to 3.3V DC
   Connect GROUND to common ground */

/* Initialise with default values (int time = 2.4ms, gain = 1x) */
// Adafruit_TCS34725 tcs = Adafruit_TCS34725();

/* Initialise with specific int time and gain values */
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_60X);

void setup(void) {
  Serial.begin(250000);
  sw0.setRxBuffer(i2cRxBuffer0, sizeof(i2cRxBuffer0));
  sw0.setTxBuffer(i2cTxBuffer0, sizeof(i2cTxBuffer0));
  sw0.setTimeout_ms(200);  
  if (tcs.begin(&sw0)) {
    Serial.println("Found sensor");
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1);
  }
  tcs.setIntegrationTime(TCS34725_INTEGRATIONTIME_700MS);
  // Now we're ready to get readings!
}

void loop(void) {
  uint16_t r, g, b, c, colorTemp, lux;
  Serial.print(tcs.dataReady());
  if (tcs.dataReady()){ 
    Serial.print(" ADC Comp ");
    tcs.getRawData(&r, &g, &b, &c);
    // colorTemp = tcs.calculateColorTemperature(r, g, b);
    colorTemp = tcs.calculateColorTemperature_dn40(r, g, b, c);
    lux = tcs.calculateLux(r, g, b);
    //delay(700);
  } else {
    Serial.print("ADC Busy ");
  }
  Serial.print(millis());
  Serial.print(" Color Temp: "); Serial.print(colorTemp, DEC); Serial.print(" K - ");
  Serial.print("Lux: "); Serial.print(lux, DEC); Serial.print(" - ");
  Serial.print("R: "); Serial.print(r, DEC); Serial.print(" ");
  Serial.print("G: "); Serial.print(g, DEC); Serial.print(" ");
  Serial.print("B: "); Serial.print(b, DEC); Serial.print(" ");
  Serial.print("C: "); Serial.print(c, DEC); Serial.print(" ");
  Serial.println(" ");
}
