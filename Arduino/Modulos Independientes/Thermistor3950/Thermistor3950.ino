    // which analog pin to connect
    #define THERMISTORPIN0 A0     
    #define THERMISTORPIN1 A1        
    // resistance at 25 degrees C
    #define THERMISTORNOMINAL 100000      
    // temp. for nominal resistance (almost always 25 C)
    #define TEMPERATURENOMINAL 25   
    // how many samples to take and average, more takes longer
    // but is more 'smooth'
    #define NUMSAMPLES 1
    // The beta coefficient of the thermistor (usually 3000-4000)
    #define BCOEFFICIENT 3950
    // the value of the 'other' resistor
    #define SERIESRESISTOR 100000    

/*
 * Connection Vcc - therm - (Analogpin) - SeriesResistor - GND
 * 
 */
     
    uint16_t samples0[NUMSAMPLES];
        uint16_t samples1[NUMSAMPLES]; 

              uint8_t i;
      float average0,average1;


         float temp0, temp1;
    void setup(void) {
      Serial.begin(250000);
      //analogReference(EXTERNAL);
      pinMode(THERMISTORPIN0, INPUT);
            pinMode(THERMISTORPIN1, INPUT);
    }
     
    void loop(void) {

     
      // take N samples in a row, with a slight delay
      for (i=0; i< NUMSAMPLES; i++) {
       samples0[i] = analogRead(THERMISTORPIN0);
       delay(10);
       samples1[i] = analogRead(THERMISTORPIN1);
       delay(10);
      }
     
      // average all the samples out
      average0 = 0;
      average1 = 0;
      for (i=0; i< NUMSAMPLES; i++) {
         average0 += samples0[i];
         average1 += samples1[i];
      }
      average0 /= NUMSAMPLES;
      average1 /= NUMSAMPLES;
     
  //    Serial.print("Average analog reading0 "); 
  //    Serial.println(average0);
      
  //    Serial.print("Average analog reading1 "); 
  //    Serial.println(average1);     
      // convert the value to resistance
      average0 = 1023 / average0 - 1;
      average0 = SERIESRESISTOR / average0;
  //    Serial.print("Thermistor0 resistance "); 
  //    Serial.println(average0);
      
      average1 = 1023 / average1 - 1;
      average1 = SERIESRESISTOR / average1;
 //     Serial.print("Thermistor1 resistance "); 
 //     Serial.println(average1);
      
     
   temp0=steinhartConvert(average0)  ;
   temp1=steinhartConvert(average1)  ;

     
//      Serial.print("Temperature0 "); 
//      Serial.print(temp0);
//      Serial.println(" *C");
//
//     Serial.print("Temperature1 "); 
//      Serial.print(temp1);
//      Serial.println(" *C");

      
      Serial.print(temp0);
      Serial.print(",");     
      Serial.print(temp1);
      Serial.println("");
     
      //delay(50);
    }

double steinhartConvert(double resistance)   {
      float steinhart;
      steinhart = resistance / THERMISTORNOMINAL;     // (R/Ro)
      steinhart = log(steinhart);                  // ln(R/Ro)
      steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
      steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
      steinhart = 1.0 / steinhart;                 // Invert
      steinhart -= 273.15;                         // convert to C
      return steinhart; 
}


