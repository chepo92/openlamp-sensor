/***************************************************************************
  This is a library for the Adafruit AS7262 6-Channel Visible Light Sensor

  This sketch reads the sensor

  Designed specifically to work with the Adafruit AS7262 breakout
  ----> http://www.adafruit.com/products/3779
  
  These sensors use I2C to communicate. The device's I2C address is 0x49
  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!
  
  Written by Dean Miller for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Wire.h>
#include <SoftWire.h>
//Choose the pins, order: (SDA,SCL)
SoftWire sw0(A4,A5);  
//SoftWire sw1(20,21);  
#include "Adafruit_AS726xSoft.h"
#include <AsyncDelay.h>

Adafruit_AS726xSoft ams0;
//Adafruit_AS726xSoft ams1;

//buffer to hold raw values
uint16_t sensorValues[AS726x_NUM_CHANNELS];

//buffer to hold calibrated values (not used by default in this example)
//float calibratedValues[AS726x_NUM_CHANNELS];

// SoftWire requires that the programmer declares the buffers used. This allows the amount of memory used to be set
// according to need. For the HIH61xx only a very small RX buffer is needed.
uint8_t i2cRxBuffer0[32];
uint8_t i2cTxBuffer0[32];

uint8_t i2cRxBuffer1[32];
uint8_t i2cTxBuffer1[32];

uint8_t temp ;

void setup() {
  Serial.begin(250000);
  while(!Serial);
   
  sw0.setRxBuffer(i2cRxBuffer0, sizeof(i2cRxBuffer0));
  sw0.setTxBuffer(i2cTxBuffer0, sizeof(i2cTxBuffer0));
  sw0.setTimeout_ms(200);
  sw0.begin();

//  sw1.setRxBuffer(i2cRxBuffer1, sizeof(i2cRxBuffer1));
//  sw1.setTxBuffer(i2cTxBuffer1, sizeof(i2cTxBuffer1));
//  sw1.setTimeout_ms(200);
//  sw1.begin();
    
  Serial.println("STARTING");
  //begin and make sure we can talk to the sensor
  if(!ams0.begin(&sw0)){
    Serial.println("could not connect to sensor! Please check your wiring.");
  }
//  if(!ams1.begin(&sw1)){
//    Serial.println("could not connect to sensor! Please check your wiring.");
//  }
  ams0.setConversionType(MODE_2);
  ams0.setGain(GAIN_64X);
  ams0.setIntegrationTime(255);

  Serial.println("INIT");
}


void loop() {

  if (ams0.dataReady()) {
      temp = ams0.readTemperature();  
      ams0.readRawValues(sensorValues);
      Serial.print("One: ");
      Serial.print("Temp: "); Serial.print(temp);
      Serial.print(" Violet: "); Serial.print(sensorValues[AS726x_VIOLET]);
      Serial.print(" Blue: "); Serial.print(sensorValues[AS726x_BLUE]);
      Serial.print(" Green: "); Serial.print(sensorValues[AS726x_GREEN]);
      Serial.print(" Yellow: "); Serial.print(sensorValues[AS726x_YELLOW]);
      Serial.print(" Orange: "); Serial.print(sensorValues[AS726x_ORANGE]);
      Serial.print(" Red: "); Serial.print(sensorValues[AS726x_RED]);
      Serial.println();
  }
  
//  if (ams1.dataReady()) {
//      temp = ams1.readTemperature();  
//      ams1.readRawValues(sensorValues);
//      Serial.print("Two: ");
//      Serial.print("Temp: "); Serial.print(temp);
//      Serial.print(" Violet: "); Serial.print(sensorValues[AS726x_VIOLET]);
//      Serial.print(" Blue: "); Serial.print(sensorValues[AS726x_BLUE]);
//      Serial.print(" Green: "); Serial.print(sensorValues[AS726x_GREEN]);
//      Serial.print(" Yellow: "); Serial.print(sensorValues[AS726x_YELLOW]);
//      Serial.print(" Orange: "); Serial.print(sensorValues[AS726x_ORANGE]);
//      Serial.print(" Red: "); Serial.print(sensorValues[AS726x_RED]);
//      Serial.println();
//  }


}

