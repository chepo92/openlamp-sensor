 /*
  * Para el modo PID manual  USAR el serial monitor SIN AJUSTE de LINEA para enviar comandos
  * 
  * 
  * 
 */
 //source:
//https://www.luisllamas.es/medir-color-arduino-colorimetro-tcs3200/
//Mod: Axel Sepulveda

//Fotodiodo 0
//VCC——5V   
//GND——GND
//S0——  -> gnd
//S1——  -> 5V
//OE --  -> gnd
//OUT——pin2
//S2—— pin3
//S3—— pin4
#include "fotodiode.h"
const int fd0 = 2;
const int s20 = 3;
const int s30 = 4;
fotodiode FD0(s20,s30,fd0); 

//Fotodiodo 1
//VCC——5V   
//GND——GND
//S0——  -> gnd
//S1——  -> 5V
//OE --  -> gnd
//OUT——pin5
//S2—— pin6
//S3—— pin7 

const int fd1 =5;
const int s21 = 6;
const int s31 = 7;
fotodiode FD1(s21,s31,fd1); 
 

//int pinLed = 8 ; 
  
    // which analog pin to connect
    #define THERMISTORPIN0 A0     
    #define THERMISTORPIN1 A1        
    // resistance at 25 degrees C
    #define THERMISTORNOMINAL 100000      
    // temp. for nominal resistance (almost always 25 C)
    #define TEMPERATURENOMINAL 25   
    // how many samples to take and average, more takes longer
    // but is more 'smooth'
    #define NUMSAMPLES 1
    // The beta coefficient of the thermistor (usually 3000-4000)
    #define BCOEFFICIENT 3950
    // the value of the 'other' resistor
    #define SERIESRESISTOR 100000    

/*
 * Connection Vcc - therm - (Analogpin) - SeriesResistor - GND
 * 
 */
     
uint16_t samples0[NUMSAMPLES];
uint16_t samples1[NUMSAMPLES]; 
uint8_t i;
float average0,average1;
float temp0, temp1;

char command; 
int number; 

//ahora seran 9 y 10
uint16_t heater0=11; //
uint16_t heater1=10; //

#include <PID_v1.h>
//#define PIN_OUTPUT0 10
//#define PIN_OUTPUT1 11

//Define Variables we'll be connecting to
double Setpoint0, Input0, Output0;
double Setpoint1, Input1, Output1;

//Specify the links and initial tuning parameters
double Kp=2.00, Ki=0.02, Kd=0.00; // 2 .02 .00

  // Para Aluminio: 10 0.1

PID myPID0(&Input0, &Output0, &Setpoint0, Kp, Ki, Kd, DIRECT);
PID myPID1(&Input1, &Output1, &Setpoint1, Kp, Ki, Kd, DIRECT);

boolean manual =true; 
boolean enableFD=false;

unsigned long timer0=0;
unsigned long timer1=0;
unsigned long timerprotec=5*60*1000;

void setup(void) {
     pinMode(heater0, OUTPUT);
     pinMode(heater1, OUTPUT);
     digitalWrite(heater0,LOW);
     digitalWrite(heater1,LOW);
     
     Serial.begin(250000);
     //analogReference(EXTERNAL);
     pinMode(THERMISTORPIN0, INPUT);
     pinMode(THERMISTORPIN1, INPUT);

     Setpoint0 = 65;
     Setpoint1 = 65;

      //turn the PID on
      //myPID0.SetMode(AUTOMATIC);
      myPID0.SetMode(MANUAL);
      myPID0.SetOutputLimits(0,40);
      
      //myPID1.SetMode(AUTOMATIC);
      myPID0.SetMode(MANUAL);
      myPID1.SetOutputLimits(0,40);

//   pinMode(pinLed, OUTPUT);
//   digitalWrite(pinLed, LOW);
   
   pinMode(s20, OUTPUT);
   pinMode(s30, OUTPUT);
   pinMode(fd0, INPUT);
   
   pinMode(s21, OUTPUT);
   pinMode(s31, OUTPUT);
   pinMode(fd1, INPUT);
   
}
    
void loop(void) {
      thermalRunaway ();
      //Read analog A0 A1 temps     
      // take N samples in a row, with a slight delay
            for (i=0; i< NUMSAMPLES; i++) {
             samples0[i] = analogRead(THERMISTORPIN0);
             delay(10);
             samples1[i] = analogRead(THERMISTORPIN1);
             delay(10);
            }
           
            // average all the samples out
            average0 = 0;
            average1 = 0;
            for (i=0; i< NUMSAMPLES; i++) {
               average0 += samples0[i];
               average1 += samples1[i];
            }
            average0 /= NUMSAMPLES;
            average1 /= NUMSAMPLES;
              
            // convert the value to resistance
            average0 = 1023 / average0 - 1;
            average0 = SERIESRESISTOR / average0;
        //    Serial.print("Thermistor0 resistance "); 
        //    Serial.println(average0);
            
            average1 = 1023 / average1 - 1;
            average1 = SERIESRESISTOR / average1;
       //     Serial.print("Thermistor1 resistance "); 
       //     Serial.println(average1);
            
        //convert to temp   
         temp0=steinhartConvert(average0)  ;
         temp1=steinhartConvert(average1)  ;
         
      //PID Input 
      Input0 = temp0; 
      Input1 = temp1;  
      //Activate PID if in range
      /*
        if(!manual && (Setpoint0-Input0 > 10)){
            Output0=40; 
            myPID0.SetMode(MANUAL);
        } else if(!manual && Setpoint0-Input0 <= 10 && myPID0.GetMode()==MANUAL){
            myPID0.SetMode(AUTOMATIC);
            Output0=0;       
        }
      
        if(!manual && (Setpoint1-Input1 > 10)){
            Output1=40; 
            myPID1.SetMode(MANUAL);
        } else if(!manual && Setpoint1-Input1 <= 10 && myPID1.GetMode()==MANUAL){
            myPID1.SetMode(AUTOMATIC);
            Output1=00;       
        }
      */
      // Compute PIDs
        myPID0.Compute();
        myPID1.Compute();
          
      //Serial commands Input 
              // only when you receive data:
              if (Serial.available()) {
                      // read the incoming byte:
                      command = Serial.read();
                      number = Serial.parseInt();
                                 
              // say what you got:               
                Serial.print("Recibido: ");  
                Serial.print(command);
                Serial.print(" ");  
                Serial.println(number);
      
                if (command=='M')   {
                      manual=true;
                      myPID0.SetMode(MANUAL);
                      Output0=0;
                      myPID1.SetMode(MANUAL);
                      Output1=0;                
                      Serial.println("Apagado");
                } else
                if (command=='A')   {
                      manual=false;
                      myPID0.SetMode(AUTOMATIC);
                      Output0=0;
                      myPID1.SetMode(AUTOMATIC);
                      Output1=0;                
                      Serial.println("Auto");
                } else
                if (command=='a')   {
                      number = constrain(number,0,255);  
                      Output0=number;
                }
                if (command=='b')   {
                      number = constrain(number,0,255);  
                      Output1=number;                
                } else      
                if (command=='P')   {  //los parametros se deben pasar x100 (facilidad por el parseInt)
                      Kp=number/100.0;
                      myPID0.SetTunings(Kp, Ki, Kd) ;                        
                      Serial.println("ok");
                }else
                if (command=='I')   {
                      Ki=number/100.0;
                      myPID0.SetTunings(Kp, Ki, Kd) ;                        
                      Serial.println("ok");
                }else
                if (command=='D')   {
                      Kd=number/100.0;
                      myPID0.SetTunings(Kp, Ki, Kd) ;                        
                      Serial.println("ok");
                } else           
                if (command=='?')   {
                    Serial.print(Kp);
                    Serial.print(",");     
                    Serial.print(Ki);
                    Serial.print(",");     
                    Serial.print(Kd);
                    Serial.println("");     
                } else           
                if (command=='E')   {
                    enableFD=!enableFD;
                } else           
                if (command=='L')   {
                    number = constrain(number,0,255);              
                    myPID0.SetOutputLimits(0,number);
                    myPID1.SetOutputLimits(0,number);
                } else           
                if (command=='S')   {
                    number = constrain(number,0,100);   
                    Setpoint0 = number;
                    Setpoint1 = number;
                }         
                                      
              }
            analogWrite(heater0,Output0);       
            analogWrite(heater1,Output1);  
      
      //Leer Valores de los fotodiodos        
      //digitalWrite(led, HIGH);
      if (enableFD){
          FD0.getColor();
          FD1.getColor();
      }
      
          
      //Manda todo por serial   
            Serial.print(millis());
            Serial.print(",");    
            Serial.print(temp0);
            Serial.print(",");     
            Serial.print(temp1);
            Serial.print(",");     
            Serial.print(Output0);
            Serial.print(",");     
            Serial.print(Output1);
            Serial.print(",");
            //Serial.println(""); 
            
            FD0.printCounts();
            Serial.print(",");      
            FD1.printCounts();
            Serial.println(); 
 
}

double steinhartConvert(double resistance)   {
      float steinhart;
      steinhart = resistance / THERMISTORNOMINAL;     // (R/Ro)
      steinhart = log(steinhart);                  // ln(R/Ro)
      steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
      steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
      steinhart = 1.0 / steinhart;                 // Invert
      steinhart -= 273.15;                         // convert to C
      return steinhart; 
}

void thermalRunaway (){
  //Check over- under temps
  if (temp0>100 || temp0< -10){
      shutDown (0);
  }
  if (temp1>100 || temp0< -10){
      shutDown (1);
  }

  // Start timers
  if (timer0==0 && myPID0.GetMode()==AUTOMATIC){
      timer0=millis();
  }
  if (timer1==0 && myPID1.GetMode()==AUTOMATIC){
      timer1=millis();     
  }
// Stop timers
  if (myPID0.GetMode()==MANUAL){
      timer0=0;
  }
  if (myPID1.GetMode()==MANUAL){
      timer1=0;
  }
//
  this_time=millis();
  if (myPID0.GetMode()==AUTOMATIC && abs(Setpoint0-Input0)>10 && this_time-timer0 > timerprotec){
      shutDown (0);
  }
  if (myPID1.GetMode()==AUTOMATIC && abs(Setpoint1-Input1)>10 && this_time-timer1 > timerprotec){
     shutDown (1);
  }

 
}

void shutDown (int index){
  if (index==0){
      myPID0.SetMode(MANUAL);
      Output0=0;
      Serial.println("Apagado: 0");
  } else if (index==1){
      myPID1.SetMode(MANUAL);
      Output1=0;
      Serial.println("Apagado: 1");      
  }           
}
