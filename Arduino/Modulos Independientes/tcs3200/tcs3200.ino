//source:
//https://www.luisllamas.es/medir-color-arduino-colorimetro-tcs3200/
//Mod: Axel Sepulveda
//VCC——5V   
//GND——GND
//S0—— pin2
//S1—— pin
//OE-- pin4
//OUT——pin5
//S2—— pin6
//S3—— pin7

const int out2 =2; 
const int s22 = 3;
const int s32 = 4;


const int out =5;
const int s2 = 6;
const int s3 = 7;
 
double countRed = 0;
double countGreen = 0;
double countBlue = 0;
double countClear = 0;

double countRed2 = 0;
double countGreen2 = 0;
double countBlue2 = 0;
double countClear2 = 0;

int led=A5;
void setup() {
   Serial.begin(9600);
   pinMode(s22, OUTPUT);
   pinMode(s32, OUTPUT);
   pinMode(s2, OUTPUT);
   pinMode(s3, OUTPUT);
   
   pinMode(out, INPUT);
      pinMode(out2, INPUT);
   pinMode(led, OUTPUT);
   //escalar la frecuencia 
   
   
}
 
void loop() {
   digitalWrite(led, HIGH);
   getColor();
   getColor2();
//   Serial.print(" Blue: ");
   Serial.print(countBlue); 
      Serial.print(",");
//   Serial.print("Red: ");
   Serial.print(countRed);
      Serial.print(",");
//  Serial.print(" Green: ");
   Serial.print(countGreen);
   Serial.print(",");
//  Serial.print(" Clear: ");
   Serial.print(countClear);
   //Serial.println(); 

   
   //digitalWrite(led, LOW);
//   Serial.print(" Blue: ");
  Serial.print("---");
   Serial.print(countBlue2); 
      Serial.print(",");
//   Serial.print("Red: ");
   Serial.print(countRed2);
      Serial.print(",");
//  Serial.print(" Green: ");
   Serial.print(countGreen2);
   Serial.print(",");
//  Serial.print(" Clear: ");
   Serial.print(countClear2);
   Serial.println(); 
   //digitalWrite(led, LOW);


}
 
void getColor()
{
   digitalWrite(s2, LOW);
   digitalWrite(s3, LOW);
   
    int n =1;
    countRed=0;
    for (int i = 0; i < n; i++){
        countRed += pulseIn(out, LOW );
    }
    //countRed = countRed/n/1000;
    //countRed = 1.0/countRed; 
    
   digitalWrite(s3, HIGH);
   countBlue=0;
    for (int i = 0; i < n; i++){
         countBlue += pulseIn(out, LOW );
    }
    //countBlue = countBlue/n/1000;
    
   //countBlue= 1.0/countBlue; 
     
   digitalWrite(s2, HIGH);
   countGreen=0;
    for (int i = 0; i < n; i++){
        countGreen += pulseIn(out, LOW );
    }
    //countGreen = countGreen/n/1000;
   //countGreen= 1.0/countGreen; 
   
   digitalWrite(s3, LOW);   
    countClear=0;
    for (int i = 0; i < n; i++){
        countClear += pulseIn(out, LOW );
    }
    //countClear = countClear/n/1000;
    //countClear= 1.0/countClear; 
}

void getColor2()
{
   digitalWrite(s22, LOW);
   digitalWrite(s32, LOW);
   
    int n =1;
    countRed2=0;
    for (int i = 0; i < n; i++){
        countRed2 += pulseIn(out2, LOW );
    }
    //countRed = countRed/n/1000;
    //countRed = 1.0/countRed; 
    
   digitalWrite(s32, HIGH);
   countBlue2=0;
    for (int i = 0; i < n; i++){
         countBlue2 += pulseIn(out2, LOW );
    }
    //countBlue = countBlue/n/1000;
    
   //countBlue= 1.0/countBlue; 
     
   digitalWrite(s22, HIGH);
   countGreen2=0;
    for (int i = 0; i < n; i++){
        countGreen2 += pulseIn(out2, LOW );
    }
    //countGreen = countGreen/n/1000;
   //countGreen= 1.0/countGreen; 
   
   digitalWrite(s32, LOW);   
    countClear2=0;
    for (int i = 0; i < n; i++){
        countClear2 += pulseIn(out2, LOW );
    }
    //countClear = countClear/n/1000;
    //countClear= 1.0/countClear; 
}
