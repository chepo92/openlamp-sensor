/***************************************************************************
  This is a library for the Adafruit AS7262 6-Channel Visible Light Sensor

  This sketch reads the sensor

  Designed specifically to work with the Adafruit AS7262 breakout
  ----> http://www.adafruit.com/products/3779
  
  These sensors use I2C to communicate. The device's I2C address is 0x49
  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!
  
  Written by Dean Miller for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Wire.h>
#include <SoftWire.h>

SoftWire sw(18,19);  
#include "Adafruit_AS726xSoft.h"

#include <AsyncDelay.h>


//create the object
Adafruit_AS726xSoft ams;

//buffer to hold raw values
uint16_t sensorValues[AS726x_NUM_CHANNELS];

//buffer to hold calibrated values (not used by default in this example)
//float calibratedValues[AS726x_NUM_CHANNELS];
int pinrst= 18;
int pinrst1= 19;

#include <AsyncDelay.h>

AsyncDelay delay_1s;

boolean enabled=false;
bool rdy = false;

void setup() {
  Serial.begin(250000);
  while(!Serial);
  
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pinrst, OUTPUT);
  pinMode(pinrst1, OUTPUT);
  
  digitalWrite(pinrst,enabled);
  digitalWrite(pinrst1,!enabled);
  delay_1s.start(1000, AsyncDelay::MILLIS);
  delay (1000);
  Serial.println("STARTING");
  //begin and make sure we can talk to the sensor
  if(!ams.begin(&sw)){
    Serial.println("could not connect to sensor! Please check your wiring.");
  }
  ams.setConversionType(MODE_2);

  Serial.println("INIT");
}


void loop() {

  if (ams.dataReady()) {
      uint8_t temp = ams.readTemperature();  
      ams.readRawValues(sensorValues);
      Serial.print("Temp: "); Serial.print(temp);
      Serial.print(" Violet: "); Serial.print(sensorValues[AS726x_VIOLET]);
      Serial.print(" Blue: "); Serial.print(sensorValues[AS726x_BLUE]);
      Serial.print(" Green: "); Serial.print(sensorValues[AS726x_GREEN]);
      Serial.print(" Yellow: "); Serial.print(sensorValues[AS726x_YELLOW]);
      Serial.print(" Orange: "); Serial.print(sensorValues[AS726x_ORANGE]);
      Serial.print(" Red: "); Serial.print(sensorValues[AS726x_RED]);
      Serial.println();
  }


}
/*
void loop() {

  digitalWrite(pinrst,enabled);
  digitalWrite(pinrst1,!enabled);

  if (delay_1s.isExpired()) {
      if(ams.begin()){
  //read the device temperature
  uint8_t temp = ams.readTemperature();
  ams.startMeasurement(); //begin a measurement
  
  //wait till data is available
  rdy = false;
  while(!rdy){
    delay(5);
    rdy = ams.dataReady();
  }  
  ams.readRawValues(sensorValues);
  Serial.print("Temp: "); Serial.print(temp);
  Serial.print(" Violet: "); Serial.print(sensorValues[AS726x_VIOLET]);
  Serial.print(" Blue: "); Serial.print(sensorValues[AS726x_BLUE]);
  Serial.print(" Green: "); Serial.print(sensorValues[AS726x_GREEN]);
  Serial.print(" Yellow: "); Serial.print(sensorValues[AS726x_YELLOW]);
  Serial.print(" Orange: "); Serial.print(sensorValues[AS726x_ORANGE]);
  Serial.print(" Red: "); Serial.print(sensorValues[AS726x_RED]);
  Serial.println();
  } else Serial.println("could not connect to sensor! Please check your wiring.");   
    delay_1s.repeat(); // Count from when the delay expired, not now
  }
  //enabled=!enabled;
}

*/
