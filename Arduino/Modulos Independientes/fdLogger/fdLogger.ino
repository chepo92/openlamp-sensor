/*
  SD card datalogger
 ** SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 8

 //source:
//https://www.luisllamas.es/medir-color-arduino-colorimetro-tcs3200/
//Mod: Axel Sepulveda
//VCC——5V   
//GND——GND
//S0—— pin2
//S1—— pin
//OE-- pin4
//OUT——pin5
//S2—— pin6
//S3—— pin7
 */
 
const int s0 = 2;
const int s1 = 3;
const int oe = 4;
const int out =5;
const int s2 = 6;
const int s3 = 7;
 
double countRed = 0;
double countGreen = 0;
double countBlue = 0;
double countClear = 0;
 
 
#include <SPI.h>
#include <SD.h>
const int chipSelect = 8;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.print("Initializing SD card...");
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    // don't do anything more:
    while (1);
  }
  Serial.println("card initialized.");
  
  
   pinMode(s0, OUTPUT);
   pinMode(s1, OUTPUT);
   pinMode(s2, OUTPUT);
   pinMode(s3, OUTPUT);
   pinMode(oe, OUTPUT);
   pinMode(out, INPUT);
   //escalar la frecuencia 
   digitalWrite(s0, LOW);
   digitalWrite(s1, HIGH);
}
  
}

void loop() {
   getColor();
//   Serial.print(" Blue: ");
   Serial.print(countBlue); 
      Serial.print(",");
//   Serial.print("Red: ");
   Serial.print(countRed);
      Serial.print(",");
//  Serial.print(" Green: ");
   Serial.print(countGreen);
   Serial.print(",");
//  Serial.print(" Clear: ");
   Serial.print(countClear);
   Serial.println();    
  // make a string for assembling the data to log:
  String dataString = "";
  dataString += String(countBlue);
  dataString += ",";
  dataString += String(countRed);
  dataString += ","; 
  dataString += String(countGreen);
  dataString += ",";  
  dataString += String(countClear);

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    //Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    Serial.println("error opening datalog.txt");
  }
}


void getColor()
{
   digitalWrite(s2, LOW);
   digitalWrite(s3, LOW);
   
    int n =1;
    countRed=0;
    for (int i = 0; i < n; i++){
        countRed += pulseIn(out, LOW );
    }
    countRed = countRed/n/1000;
    countRed = 1.0/countRed; 
    
   digitalWrite(s3, HIGH);
   countBlue=0;
    for (int i = 0; i < n; i++){
         countBlue += pulseIn(out, LOW );
    }
    countBlue = countBlue/n/1000;
    
   countBlue= 1.0/countBlue; 
     
   digitalWrite(s2, HIGH);
   countGreen=0;
    for (int i = 0; i < n; i++){
        countGreen += pulseIn(out, LOW );
    }
    countGreen = countGreen/n/1000;
   countGreen= 1.0/countGreen; 
   
   digitalWrite(s3, LOW);   
    countClear=0;
    for (int i = 0; i < n; i++){
        countClear += pulseIn(out, LOW );
    }
    countClear = countClear/n/1000;
       countClear= 1.0/countClear; 
}
