class fotodiode {
private:
 int s2;
 int s3;
 int out;
 
 double countRed;
 double countBlue;
 double countGreen;
 double countClear;
 
public:
 fotodiode();
 void getColor();
 bool isDown();
 void turnOn();
 void turnOff();
};

fotodiode::fotodiode(int ps2, int ps3, int pOut)
{
 this->s2 = ps2;
 this->s3 = ps3;
 this->out = pOut;
}


 
void fotodiode::printCounts()
{
//   Serial.print(" Blue: ");
   Serial.print(countBlue); 
      Serial.print(",");
//   Serial.print("Red: ");
   Serial.print(countRed);
      Serial.print(",");
//  Serial.print(" Green: ");
   Serial.print(countGreen);
   Serial.print(",");
//  Serial.print(" Clear: ");
   Serial.print(countClear);
}

void fotodiode::getColor()
{
   digitalWrite(s2, LOW);
   digitalWrite(s3, LOW);
   
    int n =1;
    countRed=0;
    for (int i = 0; i < n; i++){
        countRed += pulseIn(out, LOW );
    }
    //countRed = countRed/n/1000;
    //countRed = 1.0/countRed; 
    
   digitalWrite(s3, HIGH);
   countBlue=0;
    for (int i = 0; i < n; i++){
         countBlue += pulseIn(out, LOW );
    }
    //countBlue = countBlue/n/1000;
    
   //countBlue= 1.0/countBlue; 
     
   digitalWrite(s2, HIGH);
   countGreen=0;
    for (int i = 0; i < n; i++){
        countGreen += pulseIn(out, LOW );
    }
    //countGreen = countGreen/n/1000;
   //countGreen= 1.0/countGreen; 
   
   digitalWrite(s3, LOW);   
    countClear=0;
    for (int i = 0; i < n; i++){
        countClear += pulseIn(out, LOW );
    }
    //countClear = countClear/n/1000;
    //countClear= 1.0/countClear; 
}