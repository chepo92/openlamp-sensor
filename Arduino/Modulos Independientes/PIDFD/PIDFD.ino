 /*
  * Para el modo PID manual  USAR el serial monitor SIN AJUSTE de LINEA para enviar comandos
  * 
  * 
  * 
 */
 //source:
//https://www.luisllamas.es/medir-color-arduino-colorimetro-tcs3200/
//Mod: Axel Sepulveda
//VCC——5V   
//GND——GND
//
//S0—— pin2
//S1—— pin3
//OE-- pin4
//OUT——pin5
//S2—— pin6
//S3—— pin7

 
const int s0 = 2;
const int s1 = 3;
const int oe = 4;
const int out =5;
const int s2 = 6;
const int s3 = 7;
 
double countRed = 0;
double countGreen = 0;
double countBlue = 0;
double countClear = 0;

int pinLed = 8 ; 
  
    // which analog pin to connect
    #define THERMISTORPIN0 A0     
    #define THERMISTORPIN1 A1        
    // resistance at 25 degrees C
    #define THERMISTORNOMINAL 100000      
    // temp. for nominal resistance (almost always 25 C)
    #define TEMPERATURENOMINAL 25   
    // how many samples to take and average, more takes longer
    // but is more 'smooth'
    #define NUMSAMPLES 1
    // The beta coefficient of the thermistor (usually 3000-4000)
    #define BCOEFFICIENT 3950
    // the value of the 'other' resistor
    #define SERIESRESISTOR 100000    

/*
 * Connection Vcc - therm - (Analogpin) - SeriesResistor - GND
 * 
 */
     
uint16_t samples0[NUMSAMPLES];
uint16_t samples1[NUMSAMPLES]; 
uint8_t i;
float average0,average1;
float temp0, temp1;

char command; 
int number; 

//ahora seran 9 y 10
uint16_t heater0=11; //
uint16_t heater1=10; //

#include <PID_v1.h>
//#define PIN_OUTPUT0 10
//#define PIN_OUTPUT1 11

//Define Variables we'll be connecting to
double Setpoint0, Input0, Output0;
double Setpoint1, Input1, Output1;

//Specify the links and initial tuning parameters
double Kp=2.500, Ki=0.020, Kd=0.001; // 2 .02 .02

PID myPID0(&Input0, &Output0, &Setpoint0, Kp, Ki, Kd, DIRECT);
PID myPID1(&Input1, &Output1, &Setpoint1, Kp, Ki, Kd, DIRECT);


void setup(void) {
     Serial.begin(250000);
     //analogReference(EXTERNAL);
     pinMode(THERMISTORPIN0, INPUT);
     pinMode(THERMISTORPIN1, INPUT);
     pinMode(heater0, OUTPUT);
     pinMode(heater1, OUTPUT);
     digitalWrite(heater0,LOW);
     digitalWrite(heater1,LOW);
     Setpoint0 = 65;
     Setpoint1 = 65;

      //turn the PID on
      myPID0.SetMode(AUTOMATIC);
      myPID0.SetOutputLimits(0,40);
      
      myPID1.SetMode(AUTOMATIC);
      myPID1.SetOutputLimits(0,40);

   pinMode(pinLed, OUTPUT);
   digitalWrite(pinLed, LOW);
   
   pinMode(s0, OUTPUT);
   pinMode(s1, OUTPUT);
   pinMode(s2, OUTPUT);
   pinMode(s3, OUTPUT);
   pinMode(oe, OUTPUT);
   
   pinMode(out, INPUT);
   //escalar la frecuencia 
   digitalWrite(oe, LOW);
   digitalWrite(s0, LOW);
   digitalWrite(s1, HIGH);
    }
    
void loop(void) {

     
      // take N samples in a row, with a slight delay
      for (i=0; i< NUMSAMPLES; i++) {
       samples0[i] = analogRead(THERMISTORPIN0);
       delay(10);
       samples1[i] = analogRead(THERMISTORPIN1);
       delay(10);
      }
     
      // average all the samples out
      average0 = 0;
      average1 = 0;
      for (i=0; i< NUMSAMPLES; i++) {
         average0 += samples0[i];
         average1 += samples1[i];
      }
      average0 /= NUMSAMPLES;
      average1 /= NUMSAMPLES;
        
      // convert the value to resistance
      average0 = 1023 / average0 - 1;
      average0 = SERIESRESISTOR / average0;
  //    Serial.print("Thermistor0 resistance "); 
  //    Serial.println(average0);
      
      average1 = 1023 / average1 - 1;
      average1 = SERIESRESISTOR / average1;
 //     Serial.print("Thermistor1 resistance "); 
 //     Serial.println(average1);
      
  //convert to temp   
   temp0=steinhartConvert(average0)  ;
   temp1=steinhartConvert(average1)  ;
   
//PID Input 
      Input0 = temp0;    
      myPID0.Compute();
      Input1 = temp1;    
      myPID1.Compute();
//Serial commands Input 
        // only when you receive data:
        if (Serial.available()) {
                // read the incoming byte:
                command = Serial.read();
                number = Serial.parseInt();
                           
        // say what you got:               
          Serial.print("Recibido: ");  
          Serial.print(command);
          Serial.print(" ");  
          Serial.println(number);

          if (command=='M')   {
                myPID0.SetMode(MANUAL);
                Output0=0;
                myPID1.SetMode(MANUAL);
                Output1=0;                
                Serial.println("Apagado");
          } else
          if (command=='A')   {
                myPID0.SetMode(AUTOMATIC);
                Output0=0;
                myPID1.SetMode(AUTOMATIC);
                Output1=0;                
                Serial.println("Auto");
          } else
          if (command=='a')   {
                number = constrain(number,0,255);  
                Output0=number;
          }
          if (command=='b')   {
                number = constrain(number,0,255);  
                Output1=number;                
          } else      
          if (command=='P')   {  //los parametros se deben pasar x100 (facilidad por el parseInt)
                Kp=number/1000.0;
                myPID0.SetTunings(Kp, Ki, Kd) ;                        
                Serial.println("ok");
          }else
          if (command=='I')   {
                Ki=number/1000.0;
                myPID0.SetTunings(Kp, Ki, Kd) ;                        
                Serial.println("ok");
          }else
          if (command=='D')   {
                Kd=number/1000.0;
                myPID0.SetTunings(Kp, Ki, Kd) ;                        
                Serial.println("ok");
          } else           
          if (command=='?')   {
              Serial.print(Kp*1000);
              Serial.print(",");     
              Serial.print(Ki*1000);
              Serial.print(",");     
              Serial.print(Kd*1000);
              Serial.println("");
          }        
                                
        }
      analogWrite(heater0,Output0);       
      analogWrite(heater1,Output1);  
        
   //digitalWrite(led, HIGH);
   getColor();
      Serial.print(millis());
      Serial.print(",");    
      Serial.print(temp0);
      Serial.print(",");     
      Serial.print(temp1);
      Serial.print(",");     
      Serial.print(Output0);
      Serial.print(",");     
      Serial.print(Output1);
       Serial.print(",");
      //Serial.println(""); 
//   Serial.print(" Blue: ");
   Serial.print(countBlue); 
      Serial.print(",");
//   Serial.print("Red: ");
   Serial.print(countRed);
      Serial.print(",");
//  Serial.print(" Green: ");
   Serial.print(countGreen);
   Serial.print(",");
//  Serial.print(" Clear: ");
   Serial.print(countClear);
   Serial.println(); 
   //digitalWrite(led, LOW);
   
        

            


      
}

double steinhartConvert(double resistance)   {
      float steinhart;
      steinhart = resistance / THERMISTORNOMINAL;     // (R/Ro)
      steinhart = log(steinhart);                  // ln(R/Ro)
      steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
      steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
      steinhart = 1.0 / steinhart;                 // Invert
      steinhart -= 273.15;                         // convert to C
      return steinhart; 
}

void getColor()
{
   digitalWrite(s2, LOW);
   digitalWrite(s3, LOW);
   
    int n =1;
    countRed=0;
    for (int i = 0; i < n; i++){
        countRed += pulseIn(out, LOW );
    }
    //countRed = countRed/n/1000;
    //countRed = 1.0/countRed; 
    
   digitalWrite(s3, HIGH);
   countBlue=0;
    for (int i = 0; i < n; i++){
         countBlue += pulseIn(out, LOW );
    }
    //countBlue = countBlue/n/1000;
    
   //countBlue= 1.0/countBlue; 
     
   digitalWrite(s2, HIGH);
   countGreen=0;
    for (int i = 0; i < n; i++){
        countGreen += pulseIn(out, LOW );
    }
    //countGreen = countGreen/n/1000;
   //countGreen= 1.0/countGreen; 
   
   digitalWrite(s3, LOW);   
    countClear=0;
    for (int i = 0; i < n; i++){
        countClear += pulseIn(out, LOW );
    }
    //countClear = countClear/n/1000;
    //countClear= 1.0/countClear; 
}

