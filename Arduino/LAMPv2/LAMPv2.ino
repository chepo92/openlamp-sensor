 /*
  * Para el modo PID manual  USAR el serial monitor SIN AJUSTE de LINEA para enviar comandos
 */
 //source:
//https://www.luisllamas.es/medir-color-arduino-colorimetro-tcs3200/
//Mod: Axel Sepulveda


//Fotodiodo 0
//VCC——5V   
//GND——GND
//S0——  -> gnd
//S1——  -> 5V
//OE --  -> gnd
//OUT——pin2
//S2—— pin3
//S3—— pin4
#include "fotodiode.h"
const int fd0 = 5;
const int s20 = 6;
const int s30 = 7;
fotodiode FD0(s20,s30,fd0); 

//Fotodiodo 1
//VCC——5V   
//GND——GND
//S0——  -> gnd
//S1——  -> 5V
//OE --  -> gnd
//OUT——pin5
//S2—— pin6
//S3—— pin7 

const int fd1 = 2;
const int s21 = 3;
const int s31 = 4;
fotodiode FD1(s21,s31,fd1); 


 
// pin Termistores y constantes de conversión de temperatura
    #define THERMISTORPIN0 A0     
    #define THERMISTORPIN1 A2        
    // resistance at 25 degrees C
    #define THERMISTORNOMINAL 100000      
    // temp. for nominal resistance (almost always 25 C)
    #define TEMPERATURENOMINAL 25   
    // how many samples to take and average, more takes longer
    // but is more 'smooth'
    #define NUMSAMPLES 1
    // The beta coefficient of the thermistor (usually 3000-4000)
    #define BCOEFFICIENT 3950
    // the value of the 'other' resistor
    #define SERIESRESISTOR 100000    

/*
 * Connection Vcc - therm - (Analogpin) - SeriesResistor - GND
 * 
 */
 // variables de temperatura 
uint16_t samples0[NUMSAMPLES];
uint16_t samples1[NUMSAMPLES]; 
uint8_t i;
float average0,average1;
float temp0, temp1;

float filterVal = 0;       // this determines smoothness  0001 is off   1 is max
float smoothed0, smoothed1;

// Variables comandos serial
char command; 
int number; 

//Tarjeta SD
#include <SPI.h>
#include <SD.h>
const int chipSelect = 8;

// Boton y leds
const int led0 = A1;      // the number of the LED pin
const int led1 = A3;      // the number of the LED pin


const int buttonPin = A4;    // the number of the pushbutton pin
const int ledW = A5;      // the number of the LED pin

// Variables estado led y boton
int greenState = HIGH;         // the current state of the output pin
int redState = LOW;         // the current state of the output pin

int buttonState= HIGH;        // the current reading from the input pin (default pulled up
int lastButtonState = HIGH;   // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the button was pushed
unsigned long debounceDelay = 100;    // the debounce time; increase if the output flickers

unsigned long lastBlink = 0;  // the last time the output pin was toggled
long blinkDelay= -1 ; 

int mainState=0; //{0: Idle, 1: Heating 2: Heated  3: Measure 4: Cold Measure 5: Error}
#define IDLEE   0
#define CALIBRATE 1
#define HEATING 2
#define HEATED  3
#define MEASURE 4

#define ERRORR 5
// Controlador de Temperatura PID

//Salidas pwm pin 9 y 10
uint16_t heater0=10; //
uint16_t heater1=9; //



#include <PID_v1.h>

//Define Variables we'll be connecting to
double Setpoint0, Input0, Output0;
double Setpoint1, Input1, Output1;

//Specify the links and initial tuning parameters
double Kp=3.00, Ki=0.04, Kd=0.01; // 2 .02 .00
double Kp1=2.00, Ki1=0.03, Kd1=0.01; // 2 .02 .00


int threshold = 10; 
// Para Aluminio: 10 0.1 

PID myPID0(&Input0, &Output0, &Setpoint0, Kp, Ki, Kd, DIRECT);

PID myPID1(&Input1, &Output1, &Setpoint1, Kp1, Ki1, Kd1, DIRECT);

boolean manual  = false; 
boolean enableFD=false;


//Cronometros 
unsigned long timer0=0;
unsigned long timer1=0;
unsigned long durationTimer=0;
unsigned long timerprotec=300000;
unsigned long duration = 600000;

boolean SerialSendData = true;
boolean SerialDebug = true; 
boolean OutputEnable = true; 


void setup(void) {
    setPwmFrequency(heater0, 1);  
    setPwmFrequency(heater1, 1);  
    pinMode(heater0, OUTPUT);
    pinMode(heater1, OUTPUT);
    digitalWrite(heater0,LOW);
    digitalWrite(heater1,LOW);
    
    Serial.begin(250000);
    //analogReference(EXTERNAL);
    pinMode(THERMISTORPIN0, INPUT);
    pinMode(THERMISTORPIN1, INPUT);
    
    Setpoint0 = 65;
    Setpoint1 = 65;
    
    //myPID0.SetMode(AUTOMATIC);
    myPID0.SetMode(MANUAL);
    myPID0.SetOutputLimits(0,40);
    
    //myPID1.SetMode(AUTOMATIC);
    myPID0.SetMode(MANUAL);
    myPID1.SetOutputLimits(0,40);
    
    
    pinMode(s20, OUTPUT);
    pinMode(s30, OUTPUT);
    pinMode(fd0, INPUT);
    
    pinMode(s21, OUTPUT);
    pinMode(s31, OUTPUT);
    pinMode(fd1, INPUT);
    
    Serial.print("Initializing SD card...");
    // see if the card is present and can be initialized:
    if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    } else {
    Serial.println("card initialized.");  
    }

    pinMode(buttonPin, INPUT_PULLUP);
    pinMode(led0, OUTPUT);
    pinMode(led1, OUTPUT);
    pinMode(ledW, OUTPUT);
    digitalWrite(led0, LOW);
    digitalWrite(led1, LOW);
    digitalWrite(ledW, LOW);
}
    
void loop(void) {
//--------Protección de temperatura (Probar)
//      thermalRunaway ();


//--------Apagar cuando acaba el experimento
  if (mainState==MEASURE && ((millis()- durationTimer) > duration) ) {
     shutDown (0);
     shutDown (1);
     mainState=IDLEE;
     durationTimer=0 ; 
     if (SerialDebug) {Serial.println("Ended!");}    
  }

//-------- Cambiar el estado cuando se llega a la temp
  if (mainState==HEATING && doneHeating(0) && doneHeating(1)) {
     mainState=HEATED;
     if (SerialDebug) {Serial.println("Done Heating!"); }  
  }

//--------- Leer boton con debounce
  int reading = digitalRead(buttonPin);
  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }    
  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:      
    // if the button state has changed:    
    if (reading != buttonState) {
      buttonState = reading;           
      if (SerialDebug) {Serial.println("button change"); }          
      if (buttonState == HIGH) { // only change State if the new button state is HIGH (when button is pressed) 
        mainState++;
        if (SerialDebug) {Serial.println("button Released"); }                 
        if (mainState>4){
            mainState=0;
        }       
      }           
    }
  }
  lastButtonState = reading;  // save the reading. Next time through the loop, it'll be the lastButtonState:

//--------   Leer temps
  //Disable outputs 
  digitalWrite(heater0,0);       
  digitalWrite(heater1,0);  
      
//Read analog A0 A1 temps     
// take N samples in a row, with a slight delay

  for (i=0; i< NUMSAMPLES; i++) {
   
   samples0[i] = analogRead(THERMISTORPIN0);
   //delay(20);
   samples1[i] = analogRead(THERMISTORPIN1);
   //delay(20);
   
  }

//--------  Output pwm
  if (OutputEnable) {
    analogWrite(heater0,Output0);       
    analogWrite(heater1,Output1);      
  } else {
    analogWrite(heater0,0);       
    analogWrite(heater1,0);      
  }

//--- Convertir temps  
  // average all the samples out
  //average0 = 0;
  //average1 = 0;
  //for (i=0; i< NUMSAMPLES; i++) {
  //   average0 += samples0[i];
  //   average1 += samples1[i];
  //}
  
  //average0 /= NUMSAMPLES;
   smoothed0 =  smooth(samples0[0], filterVal, smoothed0);   // second parameter determines smoothness  - 0 is off,  .9999 is max smooth 
  average0 =smoothed0;
  //average1 /= NUMSAMPLES;
   smoothed1 =  smooth(samples1[0], filterVal, smoothed1);   // second parameter determines smoothness  - 0 is off,  .9999 is max smooth   
  average1 =smoothed1 ;
  
  // convert the value to resistance
  average0 = 1023 / average0 - 1;
  average0 = SERIESRESISTOR / average0;
//    Serial.print("Thermistor0 resistance "); 
//    Serial.println(average0);
  
  average1 = 1023 / average1 - 1;
  average1 = SERIESRESISTOR / average1;
//     Serial.print("Thermistor1 resistance "); 
//     Serial.println(average1);
  
//convert to temp   
  temp0=steinhartConvert(average0)  ;
  temp1=steinhartConvert(average1)  ;



//-------- Encender LED's UV en estados de medición 
 if (mainState==MEASURE || mainState==CALIBRATE) {
      digitalWrite(led0,HIGH);
      digitalWrite(led1,HIGH);
 } else {
      digitalWrite(led0,LOW);
      digitalWrite(led1,LOW);      
 }

//-------- Control LED indicador de Estado
  blinkLED(mainState);

//--------    Estados de calentar y timers apagados -> encender timers

  if ((mainState==HEATING || mainState==HEATED || mainState==MEASURE)&& !areTimersStarted() ){
        startTimers();
        if (SerialDebug) {Serial.println("timer start"); }     
  } 

//--------    Estados de calentar y timers apagados -> encender timers

  if ((mainState==MEASURE)&& !durationTimerStarted() ){
        startDurationTimers();
        if (SerialDebug) {Serial.println("Duration timer start"); }     
  }   

//--------     Estado Idle y encendidos los PID y no en control Manual -> Apagar
  if ((mainState==IDLEE || mainState==CALIBRATE ) && !manual && (statusPIDs() || Output0!=0 || Output1!=0)) {  
      if (SerialDebug) {Serial.println("apagando");}     
          shutDown (0);
          shutDown (1);
  } 

//--------   Calculos Controlador PID    
  //PID Input 
  Input0 = temp0; 
  Input1 = temp1;  
  
  // If state is heating and not in Serial-Manual Control 
  //Serial.print("Boolean"); 
  //Serial.println((mainState==HEATING || mainState==HEATED || mainState==MEASURE)&& !manual); 
  if ((mainState==HEATING || mainState==HEATED || mainState==MEASURE)&& !manual ){
    
    if (SerialDebug) {Serial.println("heating..."); }
    //Activate PID if in range
    if((Setpoint0-Input0 > threshold)){
        if (SerialDebug) {Serial.println("bang 0..."); }
        myPID0.SetMode(MANUAL);
        Output0=40; 
    } else if(Setpoint0-Input0 <= threshold && myPID0.GetMode()==MANUAL){
        if (SerialDebug) {Serial.println("pid..."); }
        Output0=0; 
        myPID0.SetMode(AUTOMATIC);
              
    }
  
    if((Setpoint1-Input1 > threshold)){
        if (SerialDebug) {Serial.println("bang 1..."); }
        Output1=40; 
        myPID1.SetMode(MANUAL);
    } else if(Setpoint1-Input1 <= threshold && myPID1.GetMode()==MANUAL){
        if (SerialDebug) {Serial.println("pid..."); }
        Output1=00;  
        myPID1.SetMode(AUTOMATIC);
             
    }              
  }
   
  // Compute PIDs
  myPID0.Compute();
  myPID1.Compute();
          
//--------   Serial commands Input 
  // only when you receive data:
  if (Serial.available()) {
          // read the incoming byte:
          command = Serial.read();
          number = Serial.parseInt();
                     
  // say what you got:               
    Serial.print("Recibido: ");  
    Serial.print(command);
    Serial.print(" ");  
    Serial.println(number);

    if (command=='M')   {     //Poner en modo Manual ambos PID
          manual=true;
          myPID0.SetMode(MANUAL);
          Output0=0;
          myPID1.SetMode(MANUAL);
          Output1=0;                
          Serial.println("Apagado");
    } else
    if (command=='A')   {  //Poner en modo Automatico ambos PID
          manual=false;
          myPID0.SetMode(AUTOMATIC);
          Output0=0;
          myPID1.SetMode(AUTOMATIC);
          Output1=0;                
          Serial.println("Auto");
    } else
    if (command=='a')   {   //Control manual del heater0
          number = constrain(number,0,255);  
          Output0=number;
    }
    if (command=='b')   {   //Control manual del heater1
          number = constrain(number,0,255);  
          Output1=number;                
    } else      
    //P,I, D: Configurar ganancias, los parametros se deben pasar x100 (facilidad por el parseInt)
    if (command=='P')   {  
          Kp=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    }else
    if (command=='I')   {
          Ki=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    }else
    if (command=='D')   {
          Kd=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    } else           
    if (command=='?')   {  // Muestra las ganancias actuales
        Serial.print(Kp);
        Serial.print(",");     
        Serial.print(Ki);
        Serial.print(",");     
        Serial.print(Kd);
        Serial.println("");     
    } else           
    if (command=='E')   {   //Habilitar lectura de Fotodiodos
        enableFD=!enableFD;
    } else           
    if (command=='L')   {   //Cambiar el limite pwm superior USAR CON CUIDADO de no quemar el plastico!
        number = constrain(number,0,255);              
        myPID0.SetOutputLimits(0,number);
        myPID1.SetOutputLimits(0,number);
    } else           
    if (command=='S')   {  //Cambiar los Setpoint 
        number = constrain(number,0,100);   
        Setpoint0 = number;
        Setpoint1 = number;
    } else           
    if (command=='J')   {  //Cambiar los Setpoint 
        SerialDebug=!SerialDebug;
        
    } else           
    if (command=='V')   {  //Cambiar los Setpoint 
        SerialSendData=!SerialSendData;
    } else           
    if (command=='O')   {  //Cambiar los Setpoint 
        OutputEnable=!OutputEnable;
    }          
                          
  }


      
//--------   Leer Valores de los fotodiodos                  
  if (mainState==MEASURE || mainState==CALIBRATE || enableFD ){
      if (SerialDebug) {Serial.println("measuring... "); }  
      FD0.getColor();
      FD1.getColor();
  }      
          
//--------   Manda todo string por serial 
// Formato: millis, temp0, temp1, pwm0, pwm1, BRGC0, BRGC1, mainState 
  
  String dataString = "";
  dataString += String(millis());
  dataString += ",";
  dataString += String(temp0);
  dataString += ",";
  dataString += String(temp1);
  dataString += ",";            

  dataString += String(Output0);
  dataString += ",";
  dataString += String(Output1);
  dataString += ",";            

  dataString += String(FD0.printCounts());
  dataString += ",";   
  dataString += String(FD1.printCounts());
  dataString += ",";            
  dataString += String(mainState);
  if (SerialSendData) {Serial.println(dataString); }

//--------   Escribe en la SD
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    //Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    if (SerialDebug) {Serial.println("error opening datalog.txt");}
  }   


  
}

double steinhartConvert(double resistance)   {
  float steinhart;
  steinhart = resistance / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C
  return steinhart; 
}

//Protección de que nada se queme o se queden pegadas las lecturas. 
void thermalRunaway (){
  //Check over- under temps
  if (temp0>100 || temp0< -10){
      shutDown (0);
      shutDown (1);      
      mainState=ERRORR;
  }
  if (temp1>100 || temp0< -10){
      shutDown (0);
      shutDown (1);
      mainState=ERRORR;
  }
  if (!doneHeating(0) && ((millis()-timer0) > timerprotec)){
      shutDown (0);
      shutDown (1);      
      mainState=ERRORR;
  }
  if (!doneHeating(0) && ((millis()-timer1) > timerprotec)){
     shutDown (0);     
     shutDown (1);
     mainState=ERRORR;
  }

 
}

// shut all down PID's in manual, outputs in 0 and Stop timers
void shutDown (int index){
  if (index==0){
      myPID0.SetMode(MANUAL);
      Output0=0;
      timer0=0;
      if (SerialDebug) { Serial.println("Apagado: 0");}
  } else if (index==1){
      myPID1.SetMode(MANUAL);
      Output1=0;
      timer1=0;
      if (SerialDebug) {Serial.println("Apagado: 1");      }
  }           
}

// PIDS in Auto and start timers
void startPIDs(){
      myPID0.SetMode(AUTOMATIC);
      myPID1.SetMode(AUTOMATIC);
}

void startTimers(){
  if (timer0==0){
      timer0=millis();
  }
  if (timer1==0){
      timer1=millis();     
  }
}

boolean areTimersStarted(){
  return timer0!=0 && timer1!=0;
}



void startDurationTimers(){
  durationTimer=millis();
}

boolean durationTimerStarted(){
  return durationTimer!=0 ;
}



boolean statusPIDs(){
  return (myPID0.GetMode()==AUTOMATIC || myPID1.GetMode()==AUTOMATIC);
}

boolean doneHeating(int heater){
  if (heater==0){
      return abs(Input0-Setpoint0)<1;
  } else if (heater==1){
      return abs(Input1-Setpoint1)<1;
  } 
  return false;
  
}


int smooth(int data, float filterVal, float smoothedVal){
  if (filterVal > 1){      // check to make sure param's are within range
    filterVal = .99;
  }
  else if (filterVal <= 0){
    filterVal = 0;
  }
  smoothedVal = (data * (1 - filterVal)) + (smoothedVal  *  filterVal);
  return (int)smoothedVal;
}


void blinkLED(int mainState) {
  switch (mainState) {
      case IDLEE:
        blinkDelay=-1;
        // statements
        break;
      case HEATING:
        blinkDelay=100;
        // statements
        break;
      case HEATED:
        blinkDelay=0;
        // statements
        break;
      case MEASURE:
        blinkDelay=1000;
        // statements
        break;
      case CALIBRATE:
        blinkDelay=1000;
        // statements
        break;   
      case ERRORR:
        blinkDelay=-1;// statements
        break;             
      default:
        blinkDelay=-1;
        // statements
        break;
  }
  long timeB=millis(); 
  if(timeB-lastBlink>blinkDelay && blinkDelay>0){
       redState=!redState;
       digitalWrite(ledW,redState);
      lastBlink=timeB;
  } else if(blinkDelay==0){
     redState=true;
     digitalWrite(ledW,redState);
  } else if(blinkDelay==-1){
     redState=false;
     digitalWrite(ledW,redState);
  } 
  
}


void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }

}
  /*

Pins 5 and 6:  controlled by timer0 8bit
Setting  Divisor Frequency
0x01  1 62500
0x02  8 7812.5
0x03  64  976.5625
0x04  256 244.140625
0x05  1024  61.03515625

TCCR0B = TCCR0B & 0b11111000 | <setting>;


Default: delay(1000) or 1000 millis() ~ 1 second

0x01: delay(64000) or 64000 millis() ~ 1 second
0x02: delay(8000) or 8000 millis() ~ 1 second
0x03: is the default
0x04: delay(250) or 250 millis() ~ 1 second
0x05: delay(62) or 62 millis() ~ 1 second
(Or 63 if you need to round up.  The number is actually 62.5)

Also, the default settings for the other timers are:
TCCR1B: 0x03
TCCR2B: 0x04


Pins 9 and 10: controlled by timer1 16Bit
Setting Divisor Frequency
0x01  1 31250
0x02  8 3906.25
0x03  64  488.28125
0x04  256 122.0703125
0x05  1024  30.517578125

TCCR1B = TCCR1B & 0b11111000 | <setting>;


Pins 11 and 3:  controlled by timer2 8 Bit
Setting Divisor Frequency
0x01  1 31250
0x02  8 3906.25
0x03  32  976.5625
0x04  64  488.28125
0x05  128 244.140625
0x06  256 122.0703125
0x07  1024  30.517578125


http://greenmeetsblue.blogspot.cl/2011/10/arduino-development-challenges.html
*/



