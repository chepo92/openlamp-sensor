 /*
  * Para el modo PID manual  USAR el serial monitor SIN AJUSTE de LINEA para enviar comandos
 */
 //source:
//https://www.luisllamas.es/medir-color-arduino-colorimetro-tcs3200/
//Mod: Axel Sepulveda

#include <Wire.h>
#include <SoftWire.h>
//Choose the pins, order: (SDA,SCL)
SoftWire sw0(18,19);  
SoftWire sw1(20,21);  
#include "Adafruit_AS726xSoft.h"
#include <AsyncDelay.h>

Adafruit_AS726xSoft ams0;
Adafruit_AS726xSoft ams1;

//buffer to hold raw values
uint16_t sensorValues0[AS726x_NUM_CHANNELS];
uint16_t sensorValues1[AS726x_NUM_CHANNELS];

//buffer to hold calibrated values (not used by default in this example)
//float calibratedValues[AS726x_NUM_CHANNELS];

// SoftWire requires that the programmer declares the buffers used. This allows the amount of memory used to be set
// according to need. For the HIH61xx only a very small RX buffer is needed.
uint8_t i2cRxBuffer0[32];
uint8_t i2cTxBuffer0[32];

uint8_t i2cRxBuffer1[32];
uint8_t i2cTxBuffer1[32];

uint8_t temp ;

AsyncDelay delay_1s;

bool rdy = false;

 
// pin Termistores y constantes de conversión de temperatura
    #define THERMISTORPIN0 A0     
    #define THERMISTORPIN1 A2        
    // resistance at 25 degrees C
    #define THERMISTORNOMINAL 100000      
    // temp. for nominal resistance (almost always 25 C)
    #define TEMPERATURENOMINAL 25   
    // how many samples to take and average, more takes longer
    // but is more 'smooth'
    // The beta coefficient of the thermistor (usually 3000-4000)
    #define BCOEFFICIENT 3950
    // the value of the 'other' resistor
    #define SERIESRESISTOR 100000    

/*
 * Connection Vcc - therm - (Analogpin) - SeriesResistor - GND
 * 
 */
 // variables de temperatura 
uint16_t samples0;
uint16_t samples1; 

double temp0, temp1;
double filterVal = 0;       // this determines smoothness  0001 is off   1 is max
double smoothed0, smoothed1;

// Variables comandos serial
char command; 
int number; 

//Tarjeta SD
#include <SPI.h>
#include <SD.h>
const byte chipSelect = 8;

// Leds UV
const byte led0 = A1;      // the number of the LED pin
const byte led1 = A3;      // the number of the LED pin

// Boton y led de estado
const byte buttonPin = 7;    // the number of the pushbutton pin
const byte ledW = 6;      // the number of the LED pin

// Variables estado led y boton
boolean greenState = HIGH;         // the current state of the output pin
boolean redState = LOW;         // the current state of the output pin

boolean buttonState= HIGH;        // the current reading from the input pin (default pulled up
boolean lastButtonState = HIGH;   // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the button was pushed
unsigned long debounceDelay = 100;    // the debounce time; increase if the output flickers

unsigned long lastBlink = 0;  // the last time the output pin was toggled
int blinkDelay= -1 ; 

byte mainState=0; //{0: Idle, 1: Heating 2: Heated  3: Measure 4: Cold Measure 5: Error}
#define IDLEE   0
#define CALIBRATE 1
#define HEATING 2
#define HEATED  3
#define MEASURE 4

#define ERRORR 5
// Controlador de Temperatura PID

//Salidas pwm pin 9 y 10
byte heater0=10; //
byte heater1=9; //

#include <PID_v1.h>

//Define Variables we'll be connecting to
double Setpoint0, Input0, Output0;
double Setpoint1, Input1, Output1;

//Specify the links and initial tuning parameters
double Kp=3.00, Ki=0.04, Kd=0.01; // 2 .02 .00
double Kp1=2.00, Ki1=0.03, Kd1=0.01; // 2 .02 .00

int threshold = 10; 

// Para Aluminio: 10 0.1 

PID myPID0(&Input0, &Output0, &Setpoint0, Kp, Ki, Kd, DIRECT);

PID myPID1(&Input1, &Output1, &Setpoint1, Kp1, Ki1, Kd1, DIRECT);

boolean manual  = false; 
boolean enableFD=false;

//Cronometros 
unsigned long timer0=0;
unsigned long timer1=0;
unsigned long durationTimer=0;
unsigned long timerprotec=300000;
unsigned long duration = 600000;

boolean SerialSendData = true;
boolean SerialDebug = true; 
boolean OutputEnable = true; 

String dataString = "millis, temp0, temp1, pwm0, pwm1, fotodiode0, fotodiode1, mainState";


void setup(void) {
    setPwmFrequency(heater0, 1);  
    setPwmFrequency(heater1, 1);  
    pinMode(heater0, OUTPUT);
    pinMode(heater1, OUTPUT);
    digitalWrite(heater0,LOW);
    digitalWrite(heater1,LOW);
    
    Serial.begin(250000);
    //analogReference(EXTERNAL);
    pinMode(THERMISTORPIN0, INPUT);
    pinMode(THERMISTORPIN1, INPUT);
    
    Setpoint0 = 65;
    Setpoint1 = 65;
    
    //myPID0.SetMode(AUTOMATIC);
    myPID0.SetMode(MANUAL);
    myPID0.SetOutputLimits(0,40);
    
    //myPID1.SetMode(AUTOMATIC);
    myPID0.SetMode(MANUAL);
    myPID1.SetOutputLimits(0,40);
    
    Serial.println("ARDUINO LAMPv2.5");  
    Serial.println("..........");  
    
    delay_1s.start(1000, AsyncDelay::MILLIS);

    sw0.setRxBuffer(i2cRxBuffer0, sizeof(i2cRxBuffer0));
    sw0.setTxBuffer(i2cTxBuffer0, sizeof(i2cTxBuffer0));
    sw0.setTimeout_ms(200);
    sw0.begin();
  
    sw1.setRxBuffer(i2cRxBuffer1, sizeof(i2cRxBuffer1));
    sw1.setTxBuffer(i2cTxBuffer1, sizeof(i2cTxBuffer1));
    sw1.setTimeout_ms(200);
    sw1.begin();
      
    Serial.println("STARTING");
    //begin and make sure we can talk to the sensor
    if(!ams0.begin(&sw0)){
      Serial.println("could not connect to sensor! Please check your wiring.");
    }
    if(!ams1.begin(&sw1)){
      Serial.println("could not connect to sensor! Please check your wiring.");
    }
    ams0.setConversionType(MODE_2);
    ams1.setConversionType(MODE_2);
  
    Serial.println("INIT");
    
    Serial.print("Initializing SD card...");
    // see if the card is present and can be initialized:
    if (!SD.begin(chipSelect)) {
    Serial.println("Card failed, or not present");
    } else {
    Serial.println("card initialized.");  
    }

    pinMode(buttonPin, INPUT_PULLUP);
    pinMode(led0, OUTPUT);
    pinMode(led1, OUTPUT);
    pinMode(ledW, OUTPUT);
    digitalWrite(led0, LOW);
    digitalWrite(led1, LOW);
    digitalWrite(ledW, LOW);
}
    
void loop(void) {
//--------Protección de temperatura (Probar)
//      thermalRunaway ();


//--------Apagar cuando acaba el experimento
  if (mainState==MEASURE && ((millis()- durationTimer) > duration) ) {
     shutDown (0);
     shutDown (1);
     mainState=IDLEE;
     durationTimer=0 ; 
     if (SerialDebug) {Serial.println("Ended!");}    
  }

//-------- Cambiar el estado cuando se llega a la temp
  if (mainState==HEATING && doneHeating(0) && doneHeating(1)) {
     mainState=HEATED;
     if (SerialDebug) {Serial.println("Done Heating!"); }  
  }

//--------- Leer boton con debounce
  int reading = digitalRead(buttonPin);
  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }    
  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:      
    // if the button state has changed:    
    if (reading != buttonState) {
      buttonState = reading;           
      if (SerialDebug) {Serial.println("button change"); }          
      if (buttonState == HIGH) { // only change State if the new button state is HIGH (when button is pressed) 
        mainState++;
        if (SerialDebug) {Serial.println("button Released"); }                 
        if (mainState>4){
            mainState=0;
        }       
      }           
    }
  }
  lastButtonState = reading;  // save the reading. Next time through the loop, it'll be the lastButtonState:

//--------   Leer temps
  //Disable outputs 
  digitalWrite(heater0,0);       
  digitalWrite(heater1,0);  
      
//Read analog A0 A1 temps     
   samples0 = analogRead(THERMISTORPIN0);
   //delay(20);
   samples1 = analogRead(THERMISTORPIN1);
   //delay(20);
   


//--------  Output pwm
  if (OutputEnable) {
    analogWrite(heater0,Output0);       
    analogWrite(heater1,Output1);      
  } else {
    analogWrite(heater0,0);       
    analogWrite(heater1,0);      
  }

//--- Convertir temps  

   smoothed0 =  smooth(samples0, filterVal, smoothed0);   // second parameter determines smoothness  - 0 is off,  .9999 is max smooth 
  temp0 = smoothed0;

   smoothed1 =  smooth(samples1, filterVal, smoothed1);   // second parameter determines smoothness  - 0 is off,  .9999 is max smooth   
  temp1 =smoothed1 ;
  
  // convert the value to resistance
  temp0 = 1023 / temp0 - 1;
  temp0 = SERIESRESISTOR / temp0;
//    Serial.print("Thermistor0 resistance "); 
//    Serial.println(average0);
  
  temp1 = 1023 / temp1 - 1;
  temp1 = SERIESRESISTOR / temp1;
//     Serial.print("Thermistor1 resistance "); 
//     Serial.println(average1);
  
//convert to temp   
  temp0=steinhartConvert(temp0)  ;
  temp1=steinhartConvert(temp1)  ;



//-------- Encender LED's UV en estados de medición 
 if (mainState==MEASURE || mainState==CALIBRATE) {
      digitalWrite(led0,HIGH);
      digitalWrite(led1,HIGH);
 } else {
      digitalWrite(led0,LOW);
      digitalWrite(led1,LOW);      
 }

//-------- Control LED indicador de Estado
  blinkLED(mainState);

//--------    Estados de calentar y timers apagados -> encender timers

  if ((mainState==HEATING || mainState==HEATED || mainState==MEASURE)&& !areTimersStarted() ){
        startTimers();
        if (SerialDebug) {Serial.println("timer start"); }     
  } 

//--------    Estados de calentar y timers apagados -> encender timers

  if ((mainState==MEASURE)&& !durationTimerStarted() ){
        startDurationTimers();
        if (SerialDebug) {Serial.println("Duration timer start"); }     
  }   

//--------     Estado Idle y encendidos los PID y no en control Manual -> Apagar
  if ((mainState==IDLEE || mainState==CALIBRATE ) && !manual && (statusPIDs() || Output0!=0 || Output1!=0)) {  
      if (SerialDebug) {Serial.println("apagando");}     
          shutDown (0);
          shutDown (1);
  } 

//--------   Calculos Controlador PID    
  //PID Input 
  Input0 = temp0; 
  Input1 = temp1;  
  
  // If state is heating and not in Serial-Manual Control 
  //Serial.print("Boolean"); 
  //Serial.println((mainState==HEATING || mainState==HEATED || mainState==MEASURE)&& !manual); 
  if ((mainState==HEATING || mainState==HEATED || mainState==MEASURE)&& !manual ){
    
    if (SerialDebug) {Serial.println("heating..."); }
    //Activate PID if in range
    if((Setpoint0-Input0 > threshold)){
        if (SerialDebug) {Serial.println("bang 0..."); }
        myPID0.SetMode(MANUAL);
        Output0=40; 
    } else if(Setpoint0-Input0 <= threshold && myPID0.GetMode()==MANUAL){
        if (SerialDebug) {Serial.println("pid..."); }
        Output0=0; 
        myPID0.SetMode(AUTOMATIC);
              
    }
  
    if((Setpoint1-Input1 > threshold)){
        if (SerialDebug) {Serial.println("bang 1..."); }
        Output1=40; 
        myPID1.SetMode(MANUAL);
    } else if(Setpoint1-Input1 <= threshold && myPID1.GetMode()==MANUAL){
        if (SerialDebug) {Serial.println("pid..."); }
        Output1=00;  
        myPID1.SetMode(AUTOMATIC);
             
    }              
  }
   
  // Compute PIDs
  myPID0.Compute();
  myPID1.Compute();
          
//--------   Serial commands Input 
  // only when you receive data:
  if (Serial.available()) {
          // read the incoming byte:
          command = Serial.read();
          number = Serial.parseInt();
                     
  // say what you got:               
    Serial.print("Recibido: ");  
    Serial.print(command);
    Serial.print(" ");  
    Serial.println(number);

    if (command=='M')   {     //Poner en modo Manual ambos PID
          manual=true;
          myPID0.SetMode(MANUAL);
          Output0=0;
          myPID1.SetMode(MANUAL);
          Output1=0;                
          Serial.println("Apagado");
    } else
    if (command=='A')   {  //Poner en modo Automatico ambos PID
          manual=false;
          myPID0.SetMode(AUTOMATIC);
          Output0=0;
          myPID1.SetMode(AUTOMATIC);
          Output1=0;                
          Serial.println("Auto");
    } else
    if (command=='a')   {   //Control manual del heater0
          number = constrain(number,0,255);  
          Output0=number;
    }
    if (command=='b')   {   //Control manual del heater1
          number = constrain(number,0,255);  
          Output1=number;                
    } else      
    //P,I, D: Configurar ganancias, los parametros se deben pasar x100 (facilidad por el parseInt)
    if (command=='P')   {  
          Kp=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    }else
    if (command=='I')   {
          Ki=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    }else
    if (command=='D')   {
          Kd=number/100.0;
          myPID0.SetTunings(Kp, Ki, Kd) ;                        
          Serial.println("ok");
    } else           
    if (command=='?')   {  // Muestra las ganancias actuales
        Serial.print(Kp);
        Serial.print(",");     
        Serial.print(Ki);
        Serial.print(",");     
        Serial.print(Kd);
        Serial.println("");     
    } else           
    if (command=='E')   {   //Habilitar lectura de Fotodiodos
        enableFD=!enableFD;
    } else           
    if (command=='L')   {   //Cambiar el limite pwm superior USAR CON CUIDADO de no quemar el plastico!
        number = constrain(number,0,255);              
        myPID0.SetOutputLimits(0,number);
        myPID1.SetOutputLimits(0,number);
    } else           
    if (command=='S')   {  //Cambiar los Setpoint 
        number = constrain(number,0,100);   
        Setpoint0 = number;
        Setpoint1 = number;
    } else           
    if (command=='J')   {  //Cambiar los Setpoint 
        SerialDebug=!SerialDebug;
        
    } else           
    if (command=='V')   {  //Cambiar los Setpoint 
        SerialSendData=!SerialSendData;
    } else           
    if (command=='O')   {  //Cambiar los Setpoint 
        OutputEnable=!OutputEnable;
    }          
                          
  }


      
//--------   Leer Valores de los fotodiodos                  
  if (mainState==MEASURE || mainState==CALIBRATE || enableFD ){
      if (SerialDebug) {Serial.println("measuring... "); }  
      if (ams0.dataReady()) {
      temp = ams0.readTemperature();  
      ams0.readRawValues(sensorValues0);
      }
  
      if (ams1.dataReady()) {
      temp = ams1.readTemperature();  
      ams1.readRawValues(sensorValues1);
      }         
  }      
          
//--------   Manda todo string por serial 
// Formato: millis, temp0, temp1, pwm0, pwm1, fotodiodo0, fotodiodo1, mainState 
  
  
  dataString = String(millis());
  dataString += ",";
  dataString += String(temp0);
  dataString += ",";
  dataString += String(temp1);
  dataString += ",";            

  dataString += String(Output0);
  dataString += ",";
  dataString += String(Output1);
  dataString += ",";            

  dataString += String(sensorValues0[AS726x_VIOLET]);
  dataString += ",";
  dataString += String(sensorValues0[AS726x_BLUE]);
  dataString += ",";
  dataString += String(sensorValues0[AS726x_GREEN]);
  dataString += ",";  
  dataString += String(sensorValues0[AS726x_YELLOW]);
  dataString += ",";  
  dataString += String(sensorValues0[AS726x_ORANGE]);
  dataString += ",";  
  dataString += String(sensorValues0[AS726x_RED]);
  dataString += ",";  

  dataString += String(sensorValues1[AS726x_VIOLET]);
  dataString += ",";
  dataString += String(sensorValues1[AS726x_BLUE]);
  dataString += ",";
  dataString += String(sensorValues1[AS726x_GREEN]);
  dataString += ",";  
  dataString += String(sensorValues1[AS726x_YELLOW]);
  dataString += ",";  
  dataString += String(sensorValues1[AS726x_ORANGE]);
  dataString += ",";  
  dataString += String(sensorValues1[AS726x_RED]);
  dataString += ",";  
  
          
  dataString += String(mainState);
  if (SerialSendData) {Serial.println(dataString); }

//--------   Escribe en la SD
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  // if the file is available, write to it:
  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    // print to the serial port too:
    //Serial.println(dataString);
  }
  // if the file isn't open, pop up an error:
  else {
    if (SerialDebug) {Serial.println("error opening datalog.txt");}
  }   


  
}

double steinhartConvert(double resistance)   {
  float steinhart;
  steinhart = resistance / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 273.15;                         // convert to C
  return steinhart; 
}

//Protección de que nada se queme o se queden pegadas las lecturas. 
void thermalRunaway (){
  //Check over- under temps
  if (temp0>100 || temp0< -10){
      shutDown (0);
      shutDown (1);      
      mainState=ERRORR;
  }
  if (temp1>100 || temp0< -10){
      shutDown (0);
      shutDown (1);
      mainState=ERRORR;
  }
  if (!doneHeating(0) && ((millis()-timer0) > timerprotec)){
      shutDown (0);
      shutDown (1);      
      mainState=ERRORR;
  }
  if (!doneHeating(0) && ((millis()-timer1) > timerprotec)){
     shutDown (0);     
     shutDown (1);
     mainState=ERRORR;
  }

 
}

// shut all down PID's in manual, outputs in 0 and Stop timers
void shutDown (int index){
  if (index==0){
      myPID0.SetMode(MANUAL);
      Output0=0;
      timer0=0;
      if (SerialDebug) { Serial.println("Apagado: 0");}
  } else if (index==1){
      myPID1.SetMode(MANUAL);
      Output1=0;
      timer1=0;
      if (SerialDebug) {Serial.println("Apagado: 1");      }
  }           
}

// PIDS in Auto and start timers
void startPIDs(){
      myPID0.SetMode(AUTOMATIC);
      myPID1.SetMode(AUTOMATIC);
}

void startTimers(){
  if (timer0==0){
      timer0=millis();
  }
  if (timer1==0){
      timer1=millis();     
  }
}

boolean areTimersStarted(){
  return timer0!=0 && timer1!=0;
}



void startDurationTimers(){
  durationTimer=millis();
}

boolean durationTimerStarted(){
  return durationTimer!=0 ;
}



boolean statusPIDs(){
  return (myPID0.GetMode()==AUTOMATIC || myPID1.GetMode()==AUTOMATIC);
}

boolean doneHeating(int heater){
  if (heater==0){
      return abs(Input0-Setpoint0)<1;
  } else if (heater==1){
      return abs(Input1-Setpoint1)<1;
  } 
  return false;
  
}





int smooth(int data, float filterVal, float smoothedVal){
  if (filterVal > 1){      // check to make sure param's are within range
    filterVal = .99;
  }
  else if (filterVal <= 0){
    filterVal = 0;
  }
  smoothedVal = (data * (1 - filterVal)) + (smoothedVal  *  filterVal);
  return (int)smoothedVal;
}


void blinkLED(int mainState) {
  switch (mainState) {
      case IDLEE:
        blinkDelay=-1;
        // statements
        break;
      case HEATING:
        blinkDelay=100;
        // statements
        break;
      case HEATED:
        blinkDelay=0;
        // statements
        break;
      case MEASURE:
        blinkDelay=1000;
        // statements
        break;
      case CALIBRATE:
        blinkDelay=1000;
        // statements
        break;   
      case ERRORR:
        blinkDelay=-1;// statements
        break;             
      default:
        blinkDelay=-1;
        // statements
        break;
  }
  long timeB=millis(); 
  if(timeB-lastBlink>blinkDelay && blinkDelay>0){
       redState=!redState;
       digitalWrite(ledW,redState);
      lastBlink=timeB;
  } else if(blinkDelay==0){
     redState=true;
     digitalWrite(ledW,redState);
  } else if(blinkDelay==-1){
     redState=false;
     digitalWrite(ledW,redState);
  } 
  
}


void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }

}
  /*

Pins 5 and 6:  controlled by timer0 8bit
Setting  Divisor Frequency
0x01  1 62500
0x02  8 7812.5
0x03  64  976.5625
0x04  256 244.140625
0x05  1024  61.03515625

TCCR0B = TCCR0B & 0b11111000 | <setting>;


Default: delay(1000) or 1000 millis() ~ 1 second

0x01: delay(64000) or 64000 millis() ~ 1 second
0x02: delay(8000) or 8000 millis() ~ 1 second
0x03: is the default
0x04: delay(250) or 250 millis() ~ 1 second
0x05: delay(62) or 62 millis() ~ 1 second
(Or 63 if you need to round up.  The number is actually 62.5)

Also, the default settings for the other timers are:
TCCR1B: 0x03
TCCR2B: 0x04


Pins 9 and 10: controlled by timer1 16Bit
Setting Divisor Frequency
0x01  1 31250
0x02  8 3906.25
0x03  64  488.28125
0x04  256 122.0703125
0x05  1024  30.517578125

TCCR1B = TCCR1B & 0b11111000 | <setting>;


Pins 11 and 3:  controlled by timer2 8 Bit
Setting Divisor Frequency
0x01  1 31250
0x02  8 3906.25
0x03  32  976.5625
0x04  64  488.28125
0x05  128 244.140625
0x06  256 122.0703125
0x07  1024  30.517578125


http://greenmeetsblue.blogspot.cl/2011/10/arduino-development-challenges.html
*/



